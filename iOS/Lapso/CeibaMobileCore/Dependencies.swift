import RxSwift

import class Foundation.URLSession
import class Foundation.OperationQueue
import enum Foundation.QualityOfService

open class Dependencies {
    
    public static let sharedDependencies = Dependencies()
    public let backgroundWorkScheduler: ImmediateSchedulerType
    public let mainScheduler: SerialDispatchQueueScheduler
    
    private init() {
        
        let operationQueue = OperationQueue()
        //Max concurrent events
        operationQueue.maxConcurrentOperationCount = 20
        operationQueue.qualityOfService = QualityOfService.userInitiated
        backgroundWorkScheduler = OperationQueueScheduler(operationQueue: operationQueue)
        mainScheduler = MainScheduler.instance
    }
    
}
