import Foundation

open class BaseResponse{

    public var closure: (_ object: AnyObject) -> Void
    public var object: AnyObject
    
    public init(closure: @escaping (_ object: AnyObject) -> Void) {
        self.closure = closure
        self.object = NSObject()
    }
    
    public init(object: AnyObject, closure: @escaping (_ object: AnyObject) -> Void) {
        self.closure = closure
        self.object = object
    }

}
