import Foundation

public enum ApplicationException : Error {
    case Info(String)
    case Warning(String)
    case Error(String)
}
