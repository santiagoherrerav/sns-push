import Foundation
import RxSwift

open class BaseController : UIViewController{
    
    var errors : [Error] = []
    
    open func createEvent(closure: @escaping () throws -> AnyObject, onFinish : @escaping (_ object: AnyObject)  -> Void) -> Observable<BaseResponse> {
        return Observable<BaseResponse>.create { observer in
            do {
                let object = try closure()
                observer.on(.next(BaseResponse(object: object, closure: onFinish)))
                observer.on(.completed)
            } catch (let error) {
                observer.on(.error(error))
                observer.on(.completed)
            }
            return Disposables.create()
        }
    }
    
    open func createVoidEvent(closure: @escaping () throws -> Void, onFinish : @escaping (_ object: AnyObject)  -> Void) -> Observable<BaseResponse> {
        return Observable<BaseResponse>.create { observer in
            do {
                try closure()
                observer.on(.next(BaseResponse(closure: onFinish)))
                observer.on(.completed)
            } catch (let error) {
                observer.on(.error(error))
                observer.on(.completed)
            }
            return Disposables.create()
        }
    }
    
    open func createAsyncEvent(closure: @escaping () throws -> Void) -> Observable<BaseResponse> {
        return Observable<BaseResponse>.create { observer in
            do {
                _ = try closure()
                observer.on(.next(BaseResponse(closure: {_ in })))
                observer.on(.completed)
            } catch (let error) {
                observer.on(.error(error))
                observer.on(.completed)
            }
            return Disposables.create()
        }
    }
    
    open func executeEvent(event: Observable<BaseResponse>){
        _ = event.subscribeOn(Dependencies.sharedDependencies.backgroundWorkScheduler)
            .observeOn(Dependencies.sharedDependencies.mainScheduler)
            .subscribe(
                onNext: {
                    (element) in self.processResult(response: element);
            },
                onError:{
                    (error) in self.processError(error: error);
            })
    }
    
    open func processResult(response: BaseResponse){
        response.closure(response.object)
    }
    
    open func processError(error: Error){
        errors.append(error)
        showErrors()
    }
    
    func showErrors(){
        if let error = self.errors.first {
            let errorLevel = getErrorLevel(error: error);
            let errorMessage = getErrorMessage(error: error);
            let alert = UIAlertController(title: errorLevel, message: errorMessage, preferredStyle: .alert)
            let okayAction = UIAlertAction(title: "Aceptar", style: .default) { action in
                self.errors.remove(at: 0)
                self.showErrors()
            }
            alert.addAction(okayAction)
            self.present(alert, animated: true, completion: nil)
        }
    }
    
    open func showGenericDialog(title: String, message: String, closure: @escaping () -> Void){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let okayAction = UIAlertAction(title: "Aceptar", style: .default) { action in
            closure()
        }
        alert.addAction(okayAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    func throwError(error: Error) throws{
        throw error
    }
    
    func getErrorLevel(error: Error) -> String{
        do{
            try throwError(error: error)
        }catch ApplicationException.Error{
            return "Error"
        }catch ApplicationException.Info{
            return "Info"
        }catch ApplicationException.Warning{
            return "Warning"
        }catch{
            return "Error"
        }
        return "Error"
    }
    
    func getErrorMessage(error: Error) -> String{
        do{
            try throwError(error: error)
        }catch ApplicationException.Error(let error){
            return error
        }catch ApplicationException.Info(let error){
            return error
        }catch ApplicationException.Warning(let error){
            return error
        }catch{
            return "Error"
        }
        return "Error"
    }
}
