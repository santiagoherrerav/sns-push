import XCTest
import CeibaMobileCore
@testable
import Lapso

class AportesDominioTest: BaseTest {
    
    var aportesDominio: AportesDominio?
    
    override func setUp() {
        super.setUp()
        //clean data
        let repository = container.resolve(AportesRepositorioProtocolo.self)!
        do {
            aportesDominio = try AportesDominio(aportesRepositorio: repository)
        } catch {
            XCTFail("No se puede intanciar el dominio de registro")
        }
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    func testIniciarAporte() {
        do {
            try aportesDominio?.iniciarAporteMeta(uuidMeta: "e8dcac30-fa60-11e7-a084-6b4cba42674c", monto: 1234, destinoAporteMeta: "1", onFinish: { movimiento in
                XCTAssertNotNil(movimiento)
                XCTAssertEqual(1, movimiento.idMovimiento!)
            }, onError: { _ in XCTFail("Error al iniciar aporte") })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
}
