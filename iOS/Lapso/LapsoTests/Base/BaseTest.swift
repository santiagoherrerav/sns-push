import XCTest
import Swinject
@testable
import Lapso

open class BaseTest: XCTestCase{
    
    public let container: Container = {
        let container = Container()
        #if MOCKS
            container.register(RegistroRepositorioProtocolo.self) { _ in RegistroMockRepositorio() }
            container.register(MetasRepositorioProtocolo.self) { _ in MetasMockRepositorio() }
            container.register(SesionUsuarioRepositorioProtocolo.self) { _ in SesionUsuarioMockRepositorio() }
            container.register(AhorroRepositorioProtocolo.self) { _ in AhorroMockRepositorio() }
            container.register(AportesRepositorioProtocolo.self) { _ in AportesMockRepositorio() }
        #else
            container.register(RegistroRepositorioProtocolo.self) { _ in RegistroRepositorio() }
            container.register(MetasRepositorioProtocolo.self) { _ in MetasRepositorio() }
            container.register(SesionUsuarioRepositorioProtocolo.self) { _ in SesionUsuarioRepositorio() }
            container.register(AhorroRepositorioProtocolo.self) { _ in AhorroRepositorio() }
            container.register(AportesRepositorioProtocolo.self) { _ in AportesRepositorio() }
        #endif
        return container
    }()
    
    open override func setUp(){
        super.setUp()
    }
    
    open override func tearDown() {
        super.tearDown()
    }
}
