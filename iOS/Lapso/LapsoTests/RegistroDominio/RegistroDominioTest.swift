import XCTest
import CeibaMobileCore
@testable
import Lapso

class RegistroDominioTest: BaseTest {

    var registroDominio: RegistroDominio?

    override func setUp() {
        super.setUp()
        //clean data
        let repository = container.resolve(RegistroRepositorioProtocolo.self)!
        do {
            registroDominio = try RegistroDominio(registroRepositorio: repository)
        } catch {
            XCTFail("No se puede intanciar el dominio de registro")
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    func testValidarInvitacionUsuario() {
        do {
            try registroDominio?.validarInvitacionUsuario(codigoPais: "CO", numeroCelular: "3002134543", onFinish: {
                XCTAssert(true)
            }, onError: { _ in XCTFail("No se logró validar la invitación") })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    func testValidarInvitacionUsuarioSinNumero() {
        do {
            try registroDominio?.validarInvitacionUsuario(codigoPais: "CO", numeroCelular: "", onFinish: {
                XCTFail()
            }, onError: { _ in XCTFail("No se logró validar la invitación") })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "El codigo del Pais y numero de celular no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    func testValidarInvitacionUsuarioSinCodPais() {
        do {
            try registroDominio?.validarInvitacionUsuario(codigoPais: "", numeroCelular: "3215771100", onFinish: {
                XCTFail()
            }, onError: { _ in XCTFail("No se logró validar la invitación") })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "El codigo del Pais y numero de celular no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    func testValidarClave() {
        let clave : String = "5454"
        do {
            try registroDominio?.validarClave(clave: clave)
            XCTAssert(true)
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    func testValidarClaveVacia() {
        let clave : String = ""
        do {
            try registroDominio?.validarClave(clave: clave)
            XCTFail("Debio arrojar una excepcion")
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Debe de llenar todos los campos para continuar")
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    func testValidarClaveConsecutivos() {
        let clave : String = "1234"
        do {
            try registroDominio?.validarClave(clave: clave)
            XCTFail("Debio arrojar una excepcion")
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Ups, no es posible usar números consecutivos")
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    func testValidarClaveIguales() {
        let clave : String = "4444"
        do {
            try registroDominio?.validarClave(clave: clave)
            XCTFail("Debio arrojar una excepcion")
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Ups, no es posible usar un mismo número")
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    func testValidarConfirmacionClave() {
        let clave : String = "5454"
        let claveRepetida : String = "5454"

        let resp = registroDominio?.validarConfirmacionlave(clave: clave, claveConfirmacion: claveRepetida)
        XCTAssert(resp!)
    }
    
    func testValidarConfirmacionClaveDiferentes() {
        let clave : String = "4596"
        let claveRepetida : String = "4595"
        
        let resp = registroDominio?.validarConfirmacionlave(clave: clave, claveConfirmacion: claveRepetida)
        XCTAssertFalse(resp!)
    }
    
    func testConsultarUsuario() {
        do {
            try registroDominio?.consultarInformacionUsuario(codigoPais: "CO", numeroCelular: "3002134543", onFinish: self.datosEmpleado, onError: { _ in XCTFail("No se logró validar la invitación") })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    private func datosEmpleado(empleadoDTO: EmpleadoDTO) {
        let nombreExpected = "Alvaro"
        let primerApellidoExpected = "Perez"
        let segundoApellidoExpected = "Salcedo"
        let correoExpected = "alvaro.perez@ceiba.com.co"


        XCTAssertEqual(nombreExpected, empleadoDTO.primerNombre)
        XCTAssertEqual(primerApellidoExpected, empleadoDTO.primerApellido)
        XCTAssertEqual(segundoApellidoExpected, empleadoDTO.segundoApellido)
        XCTAssertEqual(correoExpected, empleadoDTO.correo)
    }

    
    
    func testConsultarUsuarioSinCelular() {
        do {
            try registroDominio?.consultarInformacionUsuario(codigoPais: "CO", numeroCelular: "", onFinish: {_ in
                XCTFail()
            }, onError: { _ in XCTFail("No se logró validar la invitación") })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "El codigo del Pais y numero de celular no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    func testConsultarUsuarioSinCodPais() {
        do {
            try registroDominio?.consultarInformacionUsuario(codigoPais: "", numeroCelular: "3215771100", onFinish: {_ in
                XCTFail()
            }, onError: { _ in XCTFail("No se logró validar la invitación") })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "El codigo del Pais y numero de celular no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    func testRegistrarUsuario(){
        do {
            try registroDominio?.registrarUsuario(codigoPais: "CO", numeroCelular: "3002134543", correo: "lapso.app@gmail.com", clave: "1234", onFinish: {
                XCTAssertTrue(true)
            }, onError: { _ in XCTFail("No se pudo registrar el usuario") })
        } catch ApplicationException.Info(let error) {
           XCTFail(error)
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    func testRegistrarUsuarioSinCodigoPais(){
        do {
            try registroDominio?.registrarUsuario(codigoPais: "", numeroCelular: "3002134543", correo: "lapso.app@gmail.com", clave: "1234", onFinish: {
                XCTFail()
            }, onError: {_ in XCTFail("No se pudo registrar el usuario")})
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    func testRegistrarUsuarioSinNumeroCelular(){
        do {
            try registroDominio?.registrarUsuario(codigoPais: "CO", numeroCelular: "", correo: "lapso.app@gmail.com", clave: "1234", onFinish: {
                XCTFail()
            }, onError: {_ in XCTFail("No se pudo registrar el usuario")})
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }

    func testRegistrarUsuarioSinCorreo(){
        do {
            try registroDominio?.registrarUsuario(codigoPais: "CO", numeroCelular: "3002134543", correo: "", clave: "1234", onFinish: {
                XCTFail()
            }, onError: {_ in XCTFail("No se pudo registrar el usuario")})
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    func testRegistrarUsuarioSinClave(){
        do {
            try registroDominio?.registrarUsuario(codigoPais: "CO", numeroCelular: "3002134543", correo: "lapso.app@gmail.com", clave: "", onFinish: {
                XCTFail()
            }, onError: {_ in XCTFail("No se pudo registrar el usuario")})
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }

}
