import XCTest
import CeibaMobileCore
@testable
import Lapso

class UCadenasTest: XCTestCase {
    
    let uCadenas : UCadenas = UCadenas()
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    

    func testCaracteresSonDigitos() {
        //given
        let cadena : String = "0123456789"
        //when
        let resp = uCadenas.caracteresSonDigitos(cadena: cadena)
        //then
        XCTAssert(resp)
    }
    
    
    func testCaracteresConLetraAlFinal() {
        //given
        let cadena : String = "1234B"
        //when
        let resp = uCadenas.caracteresSonDigitos(cadena: cadena)
        //then
        XCTAssertFalse(resp)
    }
    
    func testCaracteresConLetraAlPrincipio() {
        //given
        let cadena : String = "x1234"
        //when
        let resp = uCadenas.caracteresSonDigitos(cadena: cadena)
        //then
        XCTAssertFalse(resp)
    }
    
    func testCaracteresConLetraAlMedio() {
        //given
        let cadena : String = "012B34"
        //when
        let resp = uCadenas.caracteresSonDigitos(cadena: cadena)
        //then
        XCTAssertFalse(resp)
    }
    
    func testCaracteresTodoLetras() {
        //given
        let cadena : String = "ABCD"
        //when
        let resp = uCadenas.caracteresSonDigitos(cadena: cadena)
        //then
        XCTAssertFalse(resp)
    }
    
    func testCaracteresRaros() {
        //given
        let cadena : String = "123*5"
        //when
        let resp = uCadenas.caracteresSonDigitos(cadena: cadena)
        //then
        XCTAssertFalse(resp)
    }
    
    func testCaracteresConPunto() {
        //given
        let cadena : String = "12.35"
        //when
        let resp = uCadenas.caracteresSonDigitos(cadena: cadena)
        //then
        XCTAssertFalse(resp)
    }
    
    func testCaracteresConComa() {
        //given
        let cadena : String = "1,235"
        //when
        let resp = uCadenas.caracteresSonDigitos(cadena: cadena)
        //then
        XCTAssertFalse(resp)
    }
    
    func testSonDigitosConsecutivos() {
        //given
        let cadena : String = "1234"
        //when
        let resp = uCadenas.caracteresSonDigitosConsecutivos(cadena: cadena)
        //then
        XCTAssert(resp)
    }
    
    func testSonDigitosConsecutivosNueve() {
        //given
        let cadena : String = "7890"
        //when
        let resp = uCadenas.caracteresSonDigitosConsecutivos(cadena: cadena)
        //then
        XCTAssert(resp)
    }
    
    func testSonDigitosConsecutivosCero() {
        //given
        let cadena : String = "0123"
        //when
        let resp = uCadenas.caracteresSonDigitosConsecutivos(cadena: cadena)
        //then
        XCTAssert(resp)
    }
    
    func testNoSonDigitosConsecutivos() {
        //given
        let cadena : String = "4577"
        //when
        let resp = uCadenas.caracteresSonDigitosConsecutivos(cadena: cadena)
        //then
        XCTAssertFalse(resp)
    }
    
    func testNoSonDigitosConsecutivosSalto() {
        //given
        let cadena : String = "4578"
        //when
        let resp = uCadenas.caracteresSonDigitosConsecutivos(cadena: cadena)
        //then
        XCTAssertFalse(resp)
    }
    
    func testSonDigitosConsecutivosDesc() {
        //given
        let cadena : String = "4321"
        //when
        let resp = uCadenas.caracteresSonDigitosConsecutivosDesc(cadena: cadena)
        //then
        XCTAssert(resp)
    }
    
    func testSonDigitosConsecutivosDescNueve() {
        //given
        let cadena : String = "2109"
        //when
        let resp = uCadenas.caracteresSonDigitosConsecutivosDesc(cadena: cadena)
        //then
        XCTAssert(resp)
    }
    
    func testSonDigitosConsecutivosDescCero() {
        //given
        let cadena : String = "0987"
        //when
        let resp = uCadenas.caracteresSonDigitosConsecutivosDesc(cadena: cadena)
        //then
        XCTAssert(resp)
    }
    
    func testNoSonDigitosConsecutivosDesc() {
        //given
        let cadena : String = "6544"
        //when
        let resp = uCadenas.caracteresSonDigitosConsecutivosDesc(cadena: cadena)
        //then
        XCTAssertFalse(resp)
    }
    
    func testNoSonDigitosConsecutivosDescSalto() {
        //given
        let cadena : String = "6542"
        //when
        let resp = uCadenas.caracteresSonDigitosConsecutivosDesc(cadena: cadena)
        //then
        XCTAssertFalse(resp)
    }
    
    func testSonDigitosIguales() {
        //given
        let cadena : String = "0000"
        //when
        let resp = uCadenas.caracteresSonIguales(cadena: cadena)
        //then
        XCTAssert(resp)
    }
    
    func testNoSonDigitosIguales() {
        //given
        let cadena : String = "4595"
        //when
        let resp = uCadenas.caracteresSonIguales(cadena: cadena)
        //then
        XCTAssertFalse(resp)
    }
    
    func testSonDigitosIgualesLaMitad() {
        //given
        let cadena : String = "5445"
        //when
        let resp = uCadenas.caracteresSonIguales(cadena: cadena)
        //then
        XCTAssertFalse(resp)
    }
    
}

