import XCTest
import CeibaMobileCore
@testable
import Lapso


class AhorroDominioTest: BaseTest {
    
    var ahorroDominio: AhorroDominio?
    
    override func setUp() {
        super.setUp()
        
        let repositorio = container.resolve(AhorroRepositorioProtocolo.self)!
        do {
            ahorroDominio = try AhorroDominio(ahorroRepositorio: repositorio)
        } catch {
            XCTFail("No se pudo intanciar el dominio")
        }
    }
    
    override func tearDown() {
        super.tearDown()
    }
    
    
    func testCrearAhorrador() {
        do {
            try ahorroDominio?.crearAhorrador(codigoEmpleado: "1", idEmpresa: "1", onFinish: {
                XCTAssertTrue(true)
            }, onError: { _ in XCTFail("Ocurrió un problema creando al ahorrador") })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    func testCrearAhorradorSinCodigoEmpleado() {
        do {
            try ahorroDominio?.crearAhorrador(codigoEmpleado: "", idEmpresa: "1",  onFinish: {
                XCTFail()
            }, onError: { _ in XCTFail("Ocurrió un problema creando al ahorrador") })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    
    func testCrearAhorradorSinIdEmpresa() {
        do {
            try ahorroDominio?.crearAhorrador(codigoEmpleado: "1", idEmpresa: "", onFinish: {
                XCTFail()
            }, onError: { _ in XCTFail("Ocurrió un problema creando al ahorrador") })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
   
    func testCrearAhorradorConCodigoEmpleadoErroneo() {
        do {
            try ahorroDominio?.crearAhorrador(codigoEmpleado: "-1", idEmpresa: "1", onFinish: {
                XCTAssertTrue(true)
            }, onError: { error in XCTAssertNotNil(error) })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    func testCrearAhorradorConIdEmpresaErroneo() {
        do {
            try ahorroDominio?.crearAhorrador(codigoEmpleado: "1", idEmpresa: "-1", onFinish: {
                XCTAssertTrue(true)
            }, onError: { error in XCTAssertNotNil(error) })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }

    func testCrearAhorradorConDatosErroneos() {
        do {
            try ahorroDominio?.crearAhorrador(codigoEmpleado: "-1", idEmpresa: "-1", onFinish: {
                XCTAssertTrue(true)
            }, onError: { error in XCTAssertNotNil(error) })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    
    func testConsultarConfiguracionAhorro() {
        do {
            try ahorroDominio?.consultarConfiguracionAhorro(idEmpresa: "1", onFinish: { configuracionAhorroDTO in
                self.datosConfiguracionAhorroDTO(configuracionAhorroDTO: configuracionAhorroDTO)
            }, onError: { _ in XCTFail("Ocurrió un problema creando al ahorrador") })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    private func datosConfiguracionAhorroDTO(configuracionAhorroDTO: ConfiguracionAhorroDTO){
        let montoPago = 20.00
        let porcentajeMinimo = 1
        let porcentajeMaximo = 10
        let porcentajeAhorro = 1
    
        XCTAssertEqual(montoPago, configuracionAhorroDTO.montoPago)
        XCTAssertEqual(porcentajeMinimo, configuracionAhorroDTO.porcentajeMinimo)
        XCTAssertEqual(porcentajeMaximo, configuracionAhorroDTO.porcentajeMaximo)
        XCTAssertEqual(porcentajeAhorro, configuracionAhorroDTO.porcentajeAhorro)
    }
    
    func testConsultarConfiguracionAhorroSinIdEmpresa() {
        do {
            try ahorroDominio?.consultarConfiguracionAhorro(idEmpresa: "", onFinish: {_ in
                XCTFail()
            }, onError: { _ in XCTFail("Ocurrió un problema creando al ahorrador") })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    func testConsultarConfiguracionAhorroConCodigoEmpleadoErroneo() {
        do {
            try ahorroDominio?.consultarConfiguracionAhorro(idEmpresa: "1", onFinish: {_ in
                XCTAssertTrue(true)
            }, onError: { error in XCTAssertNotNil(error) })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    func testConsultarConfiguracionAhorroConIdEmpresaErroneo() {
        
        do {
            try ahorroDominio?.consultarConfiguracionAhorro(idEmpresa: "-1", onFinish: {_ in
                XCTAssertTrue(true)
            }, onError: { error in XCTAssertNotNil(error) })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    func testConsultarConfiguracionAhorroConDatosErroneos() {
        do {
            try ahorroDominio?.consultarConfiguracionAhorro(idEmpresa: "-1", onFinish: {_ in
                XCTAssertTrue(true)
            }, onError: { error in XCTAssertNotNil(error) })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    
    func testConfigurarAhorro() {
        do {
            try ahorroDominio?.configurarAhorro(idEmpresa: "1", porcentajeAhorro: 5, onFinish: {
                XCTAssertTrue(true)
            }, onError: { _ in XCTFail("Ocurrió un problema configurando el ahorro") })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    func testConfigurarAhorroSinIdEmpresa() {
        do {
            try ahorroDominio?.configurarAhorro(idEmpresa: "", porcentajeAhorro: 5, onFinish: {
                XCTFail()
            }, onError: { _ in XCTFail("Ocurrió un problema configurando el ahorro") })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    func testConfigurarAhorroConIdEmpresaErroneo() {
        do {
            try ahorroDominio?.configurarAhorro(idEmpresa: "-1", porcentajeAhorro: 5, onFinish: {
                XCTAssertTrue(true)
            }, onError: { error in XCTAssertNotNil(error) })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    func testConfigurarAhorroConPorcentajeAhorroErroneo() {
        do {
            try ahorroDominio?.configurarAhorro(idEmpresa: "1", porcentajeAhorro: -1, onFinish: {
                XCTAssertTrue(true)
            }, onError: { error in XCTAssertNotNil(error) })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    func testConfigurarAhorroConDatosErroneos() {
        do {
            try ahorroDominio?.configurarAhorro(idEmpresa: "-1", porcentajeAhorro: 2, onFinish: {
                XCTAssertTrue(true)
            }, onError: { error in XCTAssertNotNil(error) })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
}
