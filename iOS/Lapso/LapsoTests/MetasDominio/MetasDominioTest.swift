import XCTest
import CeibaMobileCore
@testable
import Lapso

class MetasDominioTest: BaseTest {

    var metasDominio: MetasDominio?

    override func setUp() {
        super.setUp()
        //clean data
        let repository = container.resolve(MetasRepositorioProtocolo.self)!
        do {
            metasDominio = try MetasDominio(metasRepositorio: repository)
        } catch {
            XCTFail("No se puede intanciar el dominio de registro")
        }
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }

    
    func testCalcularMeses() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 3))
        
        let fechaFinal = Calendar.current.date(from: DateComponents(year: 2018, month: 10, day: 1))
        
        let montoPago: Double = (metasDominio?.simularAportePeriodico(fechaActual: fechaActual!
            , fechaFinal: fechaFinal!, montoObjetivo: 10000, frecuenciaPago: .MENSUAL))!
        
        XCTAssertEqual(1111.0, montoPago)
//        XCTAssertEqual(1111.1, montoPago)
    }
    
    func testCalcularMesesFechaAntes() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 4))
        
        let fechaFinal = Calendar.current.date(from: DateComponents(year: 2019, month: 2, day: 28))
        
        let montoPago: Double = (metasDominio?.simularAportePeriodico(fechaActual: fechaActual!
            , fechaFinal: fechaFinal!, montoObjetivo: 10000, frecuenciaPago: .MENSUAL))!
        
        XCTAssertEqual(714.0, montoPago)
//        XCTAssertEqual(714.28, montoPago)
    }
    
    func testCalcularMesesFechaDespuesPago() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 31))
        
        let fechaFinal = Calendar.current.date(from: DateComponents(year: 2019, month: 2, day: 28))
        
        let montoPago: Double = (metasDominio?.simularAportePeriodico(fechaActual: fechaActual!
            , fechaFinal: fechaFinal!, montoObjetivo: 10000, frecuenciaPago: .MENSUAL))!
        
        XCTAssertEqual(714.0, montoPago)
        //        XCTAssertEqual(714.28, montoPago)
    }
    
    func testCalcularMesesFechaDurantePago() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 30))
        
        let fechaFinal = Calendar.current.date(from: DateComponents(year: 2019, month: 2, day: 28))
        
        let montoPago: Double = (metasDominio?.simularAportePeriodico(fechaActual: fechaActual!
            , fechaFinal: fechaFinal!, montoObjetivo: 10000, frecuenciaPago: .MENSUAL))!
        
        XCTAssertEqual(714.0, montoPago)
        //        XCTAssertEqual(714.28, montoPago)
    }
    
    func testCalcularSemanal() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 3))
        
        let fechaFinal = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 24))
        
        let montoPago: Double = (metasDominio?.simularAportePeriodico(fechaActual: fechaActual!
            , fechaFinal: fechaFinal!, montoObjetivo: 10000, frecuenciaPago: .SEMANAL))!
        
//        XCTAssertEqual(3333.3, montoPago)
        XCTAssertEqual(3333.0, montoPago)

    }
    
    func testCalcularSemanalDurantePagoInicial() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 5))
        
        let fechaFinal = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 24))
        
        let montoPago: Double = (metasDominio?.simularAportePeriodico(fechaActual: fechaActual!
            , fechaFinal: fechaFinal!, montoObjetivo: 10000, frecuenciaPago: .SEMANAL))!
        
        //        XCTAssertEqual(3333.3, montoPago)
        XCTAssertEqual(3333.0, montoPago)
    }
    
    func testCalcularSemanalDurantePagoFinal() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 6))
        
        let fechaFinal = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 24))
        
        let montoPago: Double = (metasDominio?.simularAportePeriodico(fechaActual: fechaActual!
            , fechaFinal: fechaFinal!, montoObjetivo: 10000, frecuenciaPago: .SEMANAL))!
        
        //        XCTAssertEqual(3333.3, montoPago)
        XCTAssertEqual(3333.0, montoPago)
    }
    
    func testCalcularQuincenal() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 3))
        
        let fechaFinal = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 17))
        
        let montoPago: Double = (metasDominio?.simularAportePeriodico(fechaActual: fechaActual!
            , fechaFinal: fechaFinal!, montoObjetivo: 10000, frecuenciaPago: .QUINCENAL))!
        
        XCTAssertEqual(10000.0, montoPago)
    }
    
    func testCalcularQuincenalDespuesCorte() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 16))
        
        let fechaFinal = Calendar.current.date(from: DateComponents(year: 2018, month: 2, day: 3))
        
        let montoPago: Double = (metasDominio?.simularAportePeriodico(fechaActual: fechaActual!
            , fechaFinal: fechaFinal!, montoObjetivo: 10000, frecuenciaPago: .QUINCENAL))!
        
        XCTAssertEqual(10000.0, montoPago)
    }
    
    /*
     
     @Test
     public void calcularMesesTest() throws Exception, ApplicationException {
     //given
     MetasDominio metasDominio = new MetasDominio(InstrumentationRegistry.getContext());
     
     //when
     double montoPago = metasDominio.simularAportePeriodico(new LocalDate(2018, 1, 3), new LocalDate(2018, 10, 1), 10000, FrecuenciaPago.MENSUAL);
     
     //then
     assertEquals(1111.1d, montoPago, 0.1d);
     }
 
     */

    func testCalcularFechaMinimaMetaMensualBajoCorte() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 31))

        let fechaEsperada = Calendar.current.date(from: DateComponents(year: 2018, month: 2, day: 1))

        let montoPago = metasDominio?.calcularFechaMininaMeta(fechaActual: fechaActual!, frecuenciaPago: .MENSUAL)
        
        XCTAssertEqual(montoPago, fechaEsperada)
    }
    
    func testCalcularFechaMinimaMetaMensualIgualCorte() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 30))
        
        let fechaEsperada = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 31))
        
        let montoPago = metasDominio?.calcularFechaMininaMeta(fechaActual: fechaActual!, frecuenciaPago: .MENSUAL)
        
        XCTAssertEqual(montoPago, fechaEsperada)
        
    }
    
    func testCalcularFechaMinimaMetaMensualAnioBisiesto() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2020, month: 2, day: 29))
        
        let fechaEsperada = Calendar.current.date(from: DateComponents(year: 2020, month: 3, day: 1))
        
        let montoPago = metasDominio?.calcularFechaMininaMeta(fechaActual: fechaActual!, frecuenciaPago: .MENSUAL)
        
        XCTAssertEqual(montoPago, fechaEsperada)
        
    }
    
    func testCalcularFechaMinimaMetaMensualAnioNoBisiesto() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2019, month: 2, day: 28))
        
        let fechaEsperada = Calendar.current.date(from: DateComponents(year: 2019, month: 3, day: 1))
        
        let montoPago = metasDominio?.calcularFechaMininaMeta(fechaActual: fechaActual!, frecuenciaPago: .MENSUAL)
        
        XCTAssertEqual(montoPago, fechaEsperada)
        
    }

    func testCalcularFechaMinimaMetaQuincenalBajoCorte() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 12))
        
        let fechaEsperada = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 15))
        
        let montoPago = metasDominio?.calcularFechaMininaMeta(fechaActual: fechaActual!, frecuenciaPago: .QUINCENAL)
        
        XCTAssertEqual(montoPago, fechaEsperada)
        
    }
    
    func testCalcularFechaMinimaMetaQuincenalIgualCorte() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 15))
        
        let fechaEsperada = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 16))
        
        let montoPago = metasDominio?.calcularFechaMininaMeta(fechaActual: fechaActual!, frecuenciaPago: .QUINCENAL)
        
        XCTAssertEqual(montoPago, fechaEsperada)
        
    }
    
    func testCalcularFechaMinimaMetaQuincenalDespuesCorte() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 31))
        
        let fechaEsperada = Calendar.current.date(from: DateComponents(year: 2018, month: 2, day: 1))
        
        let montoPago = metasDominio?.calcularFechaMininaMeta(fechaActual: fechaActual!, frecuenciaPago: .QUINCENAL)
        
        XCTAssertEqual(montoPago, fechaEsperada)
        
    }
    
    
    func testCalcularFechaMinimaMetaQuincenalAnioBisiesto() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2020, month: 2, day: 29))
        
        let fechaEsperada = Calendar.current.date(from: DateComponents(year: 2020, month: 3, day: 1))
        
        let montoPago = metasDominio?.calcularFechaMininaMeta(fechaActual: fechaActual!, frecuenciaPago: .QUINCENAL)
        
        XCTAssertEqual(montoPago, fechaEsperada)
        
    }
    
    func testCalcularFechaMinimaMetaQuincenalAnioNoBisiesto() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2019, month: 2, day: 28))
        
        let fechaEsperada = Calendar.current.date(from: DateComponents(year: 2019, month: 3, day: 1))
        
        let montoPago = metasDominio?.calcularFechaMininaMeta(fechaActual: fechaActual!, frecuenciaPago: .QUINCENAL)
        
        XCTAssertEqual(montoPago, fechaEsperada)

    }
    
    func testCalcularFechaMinimaMetaQuincenalMesTreintaDias() {
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 11, day: 30))
        
        let fechaEsperada = Calendar.current.date(from: DateComponents(year: 2018, month: 12, day: 1))
        
        let montoPago = metasDominio?.calcularFechaMininaMeta(fechaActual: fechaActual!, frecuenciaPago: .QUINCENAL)
        
        XCTAssertEqual(montoPago, fechaEsperada)
        
    }
    
    func testCalcularFechaMinimaMetaSemanalAntesCorte(){
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 4))
        
        let fechaEsperada = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 5))
        
        let montoPago = metasDominio?.calcularFechaMininaMeta(fechaActual: fechaActual!, frecuenciaPago: .SEMANAL)
        
        XCTAssertEqual(montoPago, fechaEsperada)
    }
    
    func testCalcularFechaMinimaMetaSemanalIgualCorte(){
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 5))
        
        let fechaEsperada = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 6))
        
        let montoPago = metasDominio?.calcularFechaMininaMeta(fechaActual: fechaActual!, frecuenciaPago: .SEMANAL)
        
        XCTAssertEqual(montoPago, fechaEsperada)
    }
    
    func testCalcularFechaMinimaMetaSemanalDespuesCorte(){
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 7))
        
        let fechaEsperada = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 8))
        
        let montoPago = metasDominio?.calcularFechaMininaMeta(fechaActual: fechaActual!, frecuenciaPago: .SEMANAL)
        
        XCTAssertEqual(montoPago, fechaEsperada)
    }
    
    func testCalcularFechaMinimaMetaSemanalViernesCorte(){
        let fechaActual = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 6))
        
        let fechaEsperada = Calendar.current.date(from: DateComponents(year: 2018, month: 1, day: 7))
        
        let montoPago = metasDominio?.calcularFechaMininaMeta(fechaActual: fechaActual!, frecuenciaPago: .SEMANAL)
        
        XCTAssertEqual(montoPago, fechaEsperada)
    }
    
    func testConsultarMetas() {do {
            try metasDominio?.consultarMetas( onFinish: { meta in
                XCTAssertNotNil(meta)
                XCTAssertEqual(2, meta.count)
            }, onError: { _ in XCTFail("Ocurrió un problema consultando metas por usuario") })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    func testCrearMeta() {
        let meta = MetaDTO()
        meta.nombre = "iphone"
        meta.ahorro = 10000
        meta.monto = 100000
        meta.fechaFinal = Calendar.current.date(from: DateComponents(year: 2019, month: 12, day: 1))
        
        do {
            try metasDominio?.crearMeta(meta: meta, onFinish: {
                XCTAssertTrue(true)
            }, onError: { _ in XCTFail("Ocurrió un problema creando una meta") })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
    
    func testConsultarFrecuenciaPago() {do {
            try metasDominio?.consultarFrecuenciaPago(idEmpresa: "1", onFinish: { frecuencia in
                XCTAssertNotNil(frecuencia)
                XCTAssertEqual(FrecuenciaPago.MENSUAL, frecuencia)
            }, onError: {_ in
                XCTFail("Ocurrió un problema al consultar frecuencia pago")
            })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
  
    func testConsultarFrecuenciaPagoSinIdEmpresa() {
        do {
            try metasDominio?.consultarFrecuenciaPago(idEmpresa: "", onFinish: { frecuencia in
                XCTFail()
            }, onError: {_ in
                XCTFail("Ocurrió un problema al consultar frecuencia pago")
            })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }
    }
}
