import XCTest
import CeibaMobileCore
@testable
import Lapso


class SesionUsuarioDominioTest: BaseTest {

    var sesionUsuarioDominio: SesionUsuarioDominio?

    override func setUp() {
        super.setUp()

        let repositorio = container.resolve(SesionUsuarioRepositorioProtocolo.self)!
        do {
            sesionUsuarioDominio = try SesionUsuarioDominio(sesionUsuarioRepositorio: repositorio)
        } catch {
            XCTFail("No se pudo intanciar el dominio")
        }
    }

    override func tearDown() {
        super.tearDown()
    }

    func testIniciarSesion() {
        let accessToken: String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"

        do {
            try sesionUsuarioDominio?.iniciarSesion(codigoPais: "CO", numeroCelular: "3002134543", clave: "1234", onFinish: {}, onError: { _ in XCTFail("Ocurrió un problema iniciando sesión") })
        } catch ApplicationException.Info(let error) {
            XCTFail(error)
        } catch {
            XCTFail("Ocurrio un error inesperado")
        }

    }


    func testIniciarSesionSinCodigoPais() {
        do {
            try sesionUsuarioDominio?.iniciarSesion(codigoPais: "", numeroCelular: "3002134543", clave: "1234", onFinish: {}, onError: { _ in XCTFail("Ocurrió un problema iniciando sesión") })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }

    func testIniciarSesionSinNumeroCelular() {
        do {
            try sesionUsuarioDominio?.iniciarSesion(codigoPais: "CO", numeroCelular: "", clave: "1234", onFinish: {}, onError: { _ in XCTFail("Ocurrió un problema iniciando sesión") })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }

    func testIniciarSesionSinClave() {
        do {
            try sesionUsuarioDominio?.iniciarSesion(codigoPais: "CO", numeroCelular: "3002134543", clave: "", onFinish: {}, onError: { _ in XCTFail("Ocurrió un problema iniciando sesión") })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }


    func testIniciarSesionConClaveErronea() {
        do {
            try sesionUsuarioDominio?.iniciarSesion(codigoPais: "CO", numeroCelular: "3002134543", clave: "3445", onFinish: {}, onError: { error in XCTAssertNotNil(error) })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }

    func testIniciarSesionConNumeroCelularErronea() {
        do {
            try sesionUsuarioDominio?.iniciarSesion(codigoPais: "CO", numeroCelular: "3002134234", clave: "1234", onFinish: {}, onError: { error in XCTAssertNotNil(error) })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    func testIniciarSesionConCodigoPaisErronea() {
        do {
            try sesionUsuarioDominio?.iniciarSesion(codigoPais: "MX", numeroCelular: "3002134543", clave: "1234", onFinish: {
            }, onError: { error in XCTAssertNotNil(error) })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    func testIniciarSesionConDatosErroneos() {
        do {
            try sesionUsuarioDominio?.iniciarSesion(codigoPais: "MX", numeroCelular: "30021345234", clave: "2345", onFinish: {
            }, onError: { error in XCTAssertNotNil(error) })
        } catch ApplicationException.Info(let error) {
            XCTAssertEqual(error, "Los datos no son válidos")
        } catch {
            XCTFail("El error no es el esperado")
        }
    }
    
    func testGuardarIdEmpresa() {
        //Given
        let idEmpresa: String = "1"
        //when
        sesionUsuarioDominio?.guardarIdEmpresa(idEmpresa: idEmpresa)
        //then
        let resultEmpresa = sesionUsuarioDominio?.consultarIdEmpresa()

        XCTAssertEqual(idEmpresa, resultEmpresa)
    }
    
    func testSobreEscribirIdEmpresa() {
        //Given
        let idEmpresa: String = "1"
        let idEmpresaNuevo : String = "2"

        //when
        sesionUsuarioDominio?.guardarIdEmpresa(idEmpresa: idEmpresa)
        sesionUsuarioDominio?.guardarIdEmpresa(idEmpresa: idEmpresaNuevo)

        //then
        let resultEmpresa = sesionUsuarioDominio?.consultarIdEmpresa()
        
        XCTAssertEqual(idEmpresaNuevo, resultEmpresa!)
    }
    
    func testEliminarIdEmpresa() {
        //Given
        let idEmpresa: String = "1"
        
        //when
        sesionUsuarioDominio?.guardarIdEmpresa(idEmpresa: idEmpresa)
        sesionUsuarioDominio?.eliminarIdEmpresa()
        
        //then
        let resultEmpresa = sesionUsuarioDominio?.consultarIdEmpresa()
        
        XCTAssertNil(resultEmpresa)
    }
    
    func testGuardarToken() {
        //Given
        let token: String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"
        //when
        sesionUsuarioDominio?.guardarTokenPagos(token: token)
        //then
        let resultToken = sesionUsuarioDominio?.consultarTokenPagos()
        
        XCTAssertEqual(resultToken, token)
    }
    
    func testSobreEscribirToken() {
        //Given
        let token: String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"
        let tokenNuevo: String = "eyJhbGciABACAI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"
        //when
        sesionUsuarioDominio?.guardarTokenPagos(token: token)
        sesionUsuarioDominio?.guardarTokenPagos(token: tokenNuevo)

        //then
        let resultToken = sesionUsuarioDominio?.consultarTokenPagos()
        
        XCTAssertEqual(tokenNuevo, resultToken!)
    }
    
    func testEliminarToken() {
        //Given
        let token: String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"
        //when
        sesionUsuarioDominio?.guardarTokenPagos(token: token)
        sesionUsuarioDominio?.eliminarTokenPagos()
        
        //then
        let resultToken = sesionUsuarioDominio?.consultarTokenPagos()
        
        
        XCTAssertNil(resultToken)
    }
    
}

