//
//  HeaderView.swift
//  Lapso
//
//  Created by Ceiba on 26/10/17.
//  Copyright © 2017 YUBER GARCIA. All rights reserved.
//

import UIKit

@IBDesignable
class HeaderViewProgress: UIView {

    @IBOutlet var view: UIView!
    @IBOutlet weak var progressView: UIProgressView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "HeaderViewProgress", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(view)
        view.frame = bounds
    }
    
    public func set(progress: Float) {
        self.progressView.setProgress(progress, animated: true)
    }
}
