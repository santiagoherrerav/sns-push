//
//  HeaderLogo.swift
//  Lapso
//
//  Created by Ceiba on 28/10/17.
//  Copyright © 2017 YUBER GARCIA. All rights reserved.
//

import UIKit

@IBDesignable
class HeaderLogo: UIView {
    
    @IBOutlet var view: UIView!
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        setupView()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        setupView()
    }
    
    func setupView() {
        let bundle = Bundle(for: type(of: self))
        UINib(nibName: "HeaderLogo", bundle: bundle).instantiate(withOwner: self, options: nil)
        
        addSubview(view)
        view.frame = bounds
    }
}

