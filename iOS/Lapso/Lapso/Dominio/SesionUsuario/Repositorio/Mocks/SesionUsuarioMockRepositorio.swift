import Foundation
import CeibaMobileCore
import Alamofire
import SwiftyJSON


#if MOCKS
    open class SesionUsuarioMockRepositorio: SesionUsuarioRepositorioProtocolo {
        public func obtenerUsuarioActual(onFinish: @escaping (UsuarioDTO) -> Void, onError: @escaping (Error) -> Void) throws {
            
        }
        
        
        public init() { }
        
        public func iniciarSesion(codigoPais: String, numeroCelular: String, clave: String, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {
           
            /*let accessToken: String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"*/
            
            if(codigoPais != "CO" || numeroCelular != "3002134543" || clave != "1234") {
                onError(ApplicationException.Info("Ocurrió un problema iniciando sesión"))
            }
            
            onFinish()
        }
    }
    
#endif


