import Foundation
import SwiftKeychainWrapper
import AWSCognitoIdentityProvider

open class SesionUsuarioRepositorioLocal:  SesionUsuarioRepositorioLocalProtocolo {
    
    var pool: AWSCognitoIdentityUserPool? = AWSCognitoIdentityUserPool.init(forKey: AWSCognitoUserPoolsSignInProviderKey)
    
    let keyIdEmpresa = "KEY_ID_EMPRESA"
    let keyTokenPagos = "KEY_TOKEN_PAGOS"
    
    open func guardarIdEmpresa(idEmpresa: String) {
        let _: Bool = KeychainWrapper.standard.set(idEmpresa, forKey: keyIdEmpresa)
    }
    
    open func consultarIdEmpresa() -> String? {
        let retrievedString: String? = KeychainWrapper.standard.string(forKey: keyIdEmpresa)
        return retrievedString
    }
    
    open func eliminarIdEmpresa() {
        let _: Bool = KeychainWrapper.standard.removeObject(forKey: keyIdEmpresa)
    }
    
    open func guardarTokenPagos(token: String) {
        
         let _: Bool = KeychainWrapper.standard.set(token, forKey: keyTokenPagos)
    }
    
    open func consultarTokenPagos() -> String? {
        let retrievedString: String? = KeychainWrapper.standard.string(forKey: keyTokenPagos)
        return retrievedString
    }
    
    open func eliminarTokenPagos() {
        let _: Bool = KeychainWrapper.standard.removeObject(forKey: keyTokenPagos)
    }
    
    open func cerrarSesion(onFinish: @escaping () -> Void) {
        if let user = self.pool?.currentUser() {
            if user.isSignedIn {
                self.eliminarIdEmpresa()
                self.eliminarTokenPagos()
                user.signOut()
                onFinish()
            }
        }
    }
}
