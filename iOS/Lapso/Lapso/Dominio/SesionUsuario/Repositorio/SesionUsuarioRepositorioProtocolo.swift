import Foundation


public protocol SesionUsuarioRepositorioProtocolo {

    func iniciarSesion(codigoPais: String, numeroCelular: String, clave: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws

    func obtenerUsuarioActual(onFinish: @escaping (_ object: UsuarioDTO) -> Void, onError: @escaping (_ error: Error) -> Void) throws
}
