import Foundation
import CeibaMobileCore
import SwiftyJSON
import AWSCognitoIdentityProvider


#if !MOCKS
    open class SesionUsuarioRepositorio: LapsoRepositorioBase, SesionUsuarioRepositorioProtocolo {
        
        var pool: AWSCognitoIdentityUserPool? = AWSCognitoIdentityUserPool.init(forKey: AWSCognitoUserPoolsSignInProviderKey)

        let uNumeroCelular: UNumeroCelular = UNumeroCelular()

        public override init() { }

        open func iniciarSesion(codigoPais: String, numeroCelular: String, clave: String, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {

            let celularFormatoE164 = uNumeroCelular.obtenerCelularEnFormatoE164(codigoPais: codigoPais, numeroCelular: numeroCelular)

            let phone = AWSCognitoIdentityUserAttributeType()
            phone?.name = "phone_number"
            phone?.value = celularFormatoE164

            self.pool?.getUser().getSession(celularFormatoE164, password: "\(clave)00", validationData: [phone!])
                .continueWith { [weak self] (task) -> Any? in
                    guard self != nil else { return nil }
                    DispatchQueue.main.async(execute: {
                        if let error = task.error as NSError? {
                            print(error)
                            onError(error)
                        } else if task.result != nil {
                            onFinish()
                        }
                    })
                    return nil
                }
        }
        
        public func obtenerUsuarioActual(onFinish: @escaping (_ object: UsuarioDTO) -> Void, onError: @escaping (Error) -> Void) throws {
            let poolCred = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
            if let usuario = poolCred.currentUser() {
                let idUsuario = usuario.username
                
                usuario.getSession().continueOnSuccessWith { (task) -> AnyObject? in
                    DispatchQueue.main.async(execute: {
                        if let error = task.error as NSError? {
                            onError(error)
                        } else if task.result != nil {
                            let result = task.result
                            let token = (result?.accessToken?.tokenString)!
                            let usuario = UsuarioDTO(id: idUsuario!, token: token)
                            onFinish(usuario)
                        }
                    })
                    return nil
                }
            }else{
                onError(ApplicationException.Error("No está autorizado"))
            }
        }
    }
#endif



