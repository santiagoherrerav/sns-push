import Foundation


public protocol SesionUsuarioRepositorioLocalProtocolo {
    
    func guardarIdEmpresa( idEmpresa : String)
    
    func consultarIdEmpresa() -> String?
    
    func eliminarIdEmpresa()
    
    func guardarTokenPagos( token: String )
    
    func consultarTokenPagos() -> String?
    
    func eliminarTokenPagos()

    func cerrarSesion(onFinish: @escaping () -> Void)
}
