import Foundation
import CeibaMobileCore


open class SesionUsuarioDominio {

    var sesionUsuarioRepositorio: SesionUsuarioRepositorioProtocolo
    var repositorioLocal: SesionUsuarioRepositorioLocalProtocolo


    public init(sesionUsuarioRepositorio: SesionUsuarioRepositorioProtocolo) throws {
        self.sesionUsuarioRepositorio = sesionUsuarioRepositorio
        self.repositorioLocal = SesionUsuarioRepositorioLocal()
    }

    open func iniciarSesion(codigoPais: String, numeroCelular: String, clave: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws {

        if(codigoPais.isEmpty || numeroCelular.isEmpty || clave.isEmpty) {
            throw ApplicationException.Info("Los datos no son válidos")
        }

        try sesionUsuarioRepositorio.iniciarSesion(codigoPais: codigoPais, numeroCelular: numeroCelular, clave: clave, onFinish: onFinish, onError: onError)
    }

    open func guardarIdEmpresa(idEmpresa: String) {
        repositorioLocal.guardarIdEmpresa(idEmpresa: idEmpresa)
    }

    open func consultarIdEmpresa() -> String? {
        return repositorioLocal.consultarIdEmpresa()
    }

    open func eliminarIdEmpresa() {
        repositorioLocal.eliminarIdEmpresa()
    }

    open func guardarTokenPagos(token: String) {
        repositorioLocal.guardarTokenPagos(token: token)
    }

    open func consultarTokenPagos() -> String? {
        return repositorioLocal.consultarTokenPagos()
    }

    open func eliminarTokenPagos() {
        repositorioLocal.eliminarTokenPagos()
    }

    open func obtenerSesionActual(onFinish: @escaping (_ object: UsuarioDTO) -> Void, onError: @escaping (_ error: Error) -> Void) throws{
        try sesionUsuarioRepositorio.obtenerUsuarioActual(onFinish: onFinish, onError: onError)
    }
    
    open func cerrarSesion(onFinish: @escaping () -> Void) {
        repositorioLocal.cerrarSesion(onFinish: onFinish)
    }

}
