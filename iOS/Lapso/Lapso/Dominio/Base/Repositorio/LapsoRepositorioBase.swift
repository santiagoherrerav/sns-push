import Foundation
import AWSCognitoIdentityProvider

open class LapsoRepositorioBase {

    public init() { }

    public func crearClientAsegurado(poolCred: AWSCognitoIdentityUserPool) -> LAPSOLapsoVPCClient {

        let credentialsProvider = AWSCognitoCredentialsProvider(regionType: .USEast1, identityPoolId: LAPSOIdentityPoolID, identityProviderManager: poolCred)
        let serviceConfiguration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: credentialsProvider)
        AWSServiceManager.default().defaultServiceConfiguration = serviceConfiguration

        return LAPSOLapsoVPCClient.default()
    }
}
