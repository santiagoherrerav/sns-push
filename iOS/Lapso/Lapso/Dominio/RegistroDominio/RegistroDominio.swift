import Foundation
import CeibaMobileCore


open class RegistroDominio {

    var registroRepositorio: RegistroRepositorioProtocolo
    var uCadena = UCadenas()

    public init(registroRepositorio: RegistroRepositorioProtocolo) throws {
        self.registroRepositorio = registroRepositorio
    }

    open func validarInvitacionUsuario(codigoPais: String, numeroCelular: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws {
        if(codigoPais.isEmpty || numeroCelular.isEmpty) {
            throw ApplicationException.Info("El codigo del Pais y numero de celular no son válidos")
        }

        try registroRepositorio.validarInvitacionUsuario(codigoPais: codigoPais, numeroCelular: numeroCelular, onFinish: onFinish, onError: onError)
    }
    
    open func validarClave( clave : String) throws{
        if !uCadena.caracteresSonDigitos(cadena: clave) || clave.isEmpty{
            throw ApplicationException.Info("Debe de llenar todos los campos para continuar")
        }else if uCadena.caracteresSonDigitosConsecutivos(cadena: clave ) || uCadena.caracteresSonDigitosConsecutivosDesc(cadena: clave){
            throw ApplicationException.Info("Ups, no es posible usar números consecutivos")
        }else if uCadena.caracteresSonIguales(cadena: clave){
            throw ApplicationException.Info("Ups, no es posible usar un mismo número")
        }
    }
    
    open func validarConfirmacionlave( clave : String, claveConfirmacion : String) -> Bool{
        return clave == claveConfirmacion
    }

    open func consultarInformacionUsuario(codigoPais: String, numeroCelular: String, onFinish: @escaping (_ object: EmpleadoDTO) -> Void, onError: @escaping (_ error: Error) -> Void) throws {
        if(codigoPais.isEmpty || numeroCelular.isEmpty) {
            throw ApplicationException.Info("El codigo del Pais y numero de celular no son válidos")
        }

        try registroRepositorio.consultarInformacionUsuario(codigoPais: codigoPais, numeroCelular: numeroCelular, onFinish: onFinish, onError: onError)
    }
    
    open func registrarUsuario(codigoPais: String, numeroCelular: String, correo: String, clave: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws {
        
        if(codigoPais.isEmpty || numeroCelular.isEmpty || correo.isEmpty || clave.isEmpty) {
            throw ApplicationException.Info("Los datos no son válidos")
        }
        
        try registroRepositorio.registrarUsuario(codigoPais: codigoPais, numeroCelular: numeroCelular, correo: correo, clave: clave, onFinish:onFinish, onError: onError)
    }
    
    open func validarCodigoVerificacion(codigoPais: String, numeroCelular: String, codigoVerificacion: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws {
        
        if(codigoPais.isEmpty || numeroCelular.isEmpty || codigoVerificacion.isEmpty ) {
            throw ApplicationException.Info("Los datos no son válidos")
        }
        
        try registroRepositorio.validarCodigoVerificacion(codigoPais: codigoPais, numeroCelular: numeroCelular, codigoVerificacion: codigoVerificacion, onFinish: onFinish, onError: onError)
    }
    
    
    open func reenviarCodigoVerificacion(codigoPais: String, numeroCelular: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws {
        
        if(codigoPais.isEmpty || numeroCelular.isEmpty) {
            throw ApplicationException.Info("Los datos no son válidos")
        }
        
        try registroRepositorio.reenviarCodigoVerificacion(codigoPais: codigoPais, numeroCelular: numeroCelular, onFinish: onFinish, onError: onError)
    }
    
    open func cargarFotoDNI(uuidUsuario: String, fotoDNI: UIImage, tokenAutorizacion: String, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {
        
        if(uuidUsuario.isEmpty || tokenAutorizacion.isEmpty) {
            throw ApplicationException.Info("Los datos no son válidos")
        }
        
        try registroRepositorio.cargarFotoDNI(uuidUsuario: uuidUsuario, fotoDNI: fotoDNI, tokenAutorizacion: tokenAutorizacion, onFinish: onFinish, onError: onError)
    }
    

}

