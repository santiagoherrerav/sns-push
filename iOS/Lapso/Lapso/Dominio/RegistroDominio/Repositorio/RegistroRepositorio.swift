import Foundation
import CeibaMobileCore
import Alamofire
import SwiftyJSON
import AWSCognitoIdentityProvider



#if !MOCKS
    open class RegistroRepositorio: LapsoRepositorioBase, RegistroRepositorioProtocolo {

        var pool: AWSCognitoIdentityUserPool? = AWSCognitoIdentityUserPool.init(forKey: AWSCognitoUserPoolsSignInProviderKey)

        let uNumeroCelular: UNumeroCelular = UNumeroCelular()

        public override init() { }

        open func validarInvitacionUsuario(codigoPais: String, numeroCelular: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws {

            let client = LAPSOLapsoVPCClient.client(forKey: LAPSOIdVPCClient)

            let body: LAPSOValidarInvitacionrequest = LAPSOValidarInvitacionrequest()

            body.celular = numeroCelular
            body.codigoPais = codigoPais

            client.empresaValidarInvitacionPost(body: body).continueWith { [weak self] (task) -> Any? in
                guard self != nil else { return nil }
                DispatchQueue.main.async(execute: {
                    if task.error != nil {
                        print(task.error)
                        onError(ApplicationException.Error("Ocurrió un problema consultando la invitación"))
                    } else if task.result != nil {
                        onFinish()
                }

                })
                return nil
            }
            
        }

        open func consultarInformacionUsuario(codigoPais: String, numeroCelular: String, onFinish: @escaping (_ object: EmpleadoDTO) -> Void, onError: @escaping (_ error: Error) -> Void) throws {

            let client = LAPSOLapsoVPCClient.client(forKey: LAPSOIdVPCClient)


            client.empresaEmpleadosGet(codigoPais: codigoPais, celular: numeroCelular).continueWith { [weak self] (task) -> Any? in
                guard self != nil else { return nil }
                DispatchQueue.main.async(execute: {
                    if task.error != nil {
                        onError(ApplicationException.Error("Ocurrió un problema creando al ahorrador"))
                    } else if let result = task.result {
                        print(result)
                        if result is LAPSOConsultarEmpleadoResponse{
                            onFinish(self!.procesarRespuestaConsultaInfoUsuario(respuesta: result ))
                        } else {
                            onError(ApplicationException.Error("Ocurrió un problema consultando la información"))
                        }
                    }
                    
                })
                return nil
            }
        }

        open func registrarUsuario(codigoPais: String, numeroCelular: String, correo: String, clave: String, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {

            var attributes = [AWSCognitoIdentityUserAttributeType]()

            let celularFormatoE164 = uNumeroCelular.obtenerCelularEnFormatoE164(codigoPais: codigoPais, numeroCelular: numeroCelular)

            let phone = AWSCognitoIdentityUserAttributeType()
            phone?.name = "phone_number"
            phone?.value = celularFormatoE164
            attributes.append(phone!)

            let email = AWSCognitoIdentityUserAttributeType()
            email?.name = "email"
            email?.value = correo
            attributes.append(email!)

            let custom =  AWSCognitoIdentityUserAttributeType()
            custom?.name = "custom:role"
            custom?.value = "ahorrador"
            attributes.append(custom!)

            self.pool?.signUp
            self.pool?.signUp(celularFormatoE164, password: "\(clave)00", userAttributes: attributes, validationData: nil).continueWith { [weak self] (task) -> Any? in
                guard self != nil else { return nil }
                DispatchQueue.main.async(execute: {
                    if let error = task.error as NSError? {
                        print(error)
                        onError(error)
                    } else if let result = task.result {
                        if (result.user.confirmedStatus != AWSCognitoIdentityUserStatus.confirmed) {
                            onFinish()
                        } else {
                            onError(ApplicationException.Error("No se pudo registrar el usuario"))
                        }
                    }

                })
                return nil
            }
        }

        open func reenviarCodigoVerificacion(codigoPais: String, numeroCelular: String, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {

            let celularFormatoE164 = uNumeroCelular.obtenerCelularEnFormatoE164(codigoPais: codigoPais, numeroCelular: numeroCelular)

            self.pool?.getUser(celularFormatoE164).resendConfirmationCode().continueWith { [weak self] (task: AWSTask) -> AnyObject? in
                guard let _ = self else { return nil }
                DispatchQueue.main.async(execute: {
                    if let error = task.error as NSError? {
                        onError(error)
                    } else if task.result != nil {
                        onFinish()
                    }
                })
                return nil
            }
        }

        open func validarCodigoVerificacion(codigoPais: String, numeroCelular: String, codigoVerificacion: String, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {

            let celularFormatoE164 = uNumeroCelular.obtenerCelularEnFormatoE164(codigoPais: codigoPais, numeroCelular: numeroCelular)

            self.pool?.getUser(celularFormatoE164).confirmSignUp(codigoVerificacion).continueWith { [weak self] (task: AWSTask) -> AnyObject? in
                guard self != nil else { return nil }
                DispatchQueue.main.async(execute: {
                    if let error = task.error as NSError? {
                        onError(ApplicationException.Error("Ocurrió un problema al validar código de verificación"))
                    } else {
                        onFinish()
                    }
                })
                return nil
            }
        }

        open func cargarFotoDNI(uuidUsuario: String, fotoDNI: UIImage, tokenAutorizacion: String, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {

            let URL = try! URLRequest(url: "\(domain)/documento/ahorrador/cargar_documento_identidad", method: .post, headers: ["Authorization": "Bearer \(tokenAutorizacion)"])

            Alamofire.upload(multipartFormData: { multipartFormData in
                self.addImageData(multipartFormData: multipartFormData, image: fotoDNI)
                multipartFormData.append(uuidUsuario.data(using: String.Encoding.utf8)!, withName: "uuid")

            }, with: URL, encodingCompletion: {
                    encodingResult in
                    switch encodingResult {
                    case .success(let upload, _, _):
                        upload.validate(statusCode: 200..<300)
                            .response { response in
                                onFinish()
                        }
                    case .failure(_):
                        onError(ApplicationException.Error("Ocurrió un problema subiendo la imagen"))
                    }
                })
        }

        private func addImageData(multipartFormData: MultipartFormData, image: UIImage!) {
            var data = UIImagePNGRepresentation(image!)
            if data != nil {
                multipartFormData.append(data!, withName: "file", fileName: "picture.png", mimeType: "image/png")
            } else {
                data = UIImageJPEGRepresentation(image!, 1.0)
                multipartFormData.append((data?.base64EncodedData())!, withName: "file", fileName: "picture.jpeg", mimeType: "image/jpeg")
            }
        }

        private func procesarRespuestaConsultaInfoUsuario(respuesta: LAPSOConsultarEmpleadoResponse) -> EmpleadoDTO {
            let empleado = EmpleadoDTO()
            empleado.idEmpleado = respuesta.idEmpleado
            empleado.primerNombre = respuesta.primerNombre
            empleado.otrosNombres = respuesta.otrosNombres
            empleado.primerApellido = respuesta.primerApellido
            empleado.segundoApellido = respuesta.segundoApellido
            let empresa = EmpresaDTO()
            empresa.uuid = respuesta.empresa.uuid
            empresa.nombre = respuesta.empresa.nombre
            empleado.empresa = empresa
            return empleado
        }

    }
#endif

