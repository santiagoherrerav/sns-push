import Foundation


public protocol RegistroRepositorioProtocolo {
    
    func validarInvitacionUsuario(codigoPais: String, numeroCelular: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws
    
    func consultarInformacionUsuario(codigoPais: String, numeroCelular: String, onFinish: @escaping (_ object: EmpleadoDTO) -> Void, onError: @escaping (_ error: Error) -> Void) throws
    
    func registrarUsuario(codigoPais: String, numeroCelular: String, correo: String, clave: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws
    
    func reenviarCodigoVerificacion(codigoPais: String, numeroCelular: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws
    
    func validarCodigoVerificacion(codigoPais: String, numeroCelular: String, codigoVerificacion: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws
    
    func cargarFotoDNI(uuidUsuario: String, fotoDNI: UIImage, tokenAutorizacion: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws
    
}
