import Foundation
import CeibaMobileCore
import Alamofire
import SwiftyJSON


#if MOCKS
    open class RegistroMockRepositorio: RegistroRepositorioProtocolo {

        public init() { }

        open func validarInvitacionUsuario(codigoPais: String, numeroCelular: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws {
            if(codigoPais != "CO" || numeroCelular != "3002134543") {
                onError(ApplicationException.Info("El codigo del Pais y numero de celular no son válidos"))
            }
            onFinish()
        }

        open func consultarInformacionUsuario(codigoPais: String, numeroCelular: String, onFinish: @escaping (_ object: EmpleadoDTO) -> Void, onError: @escaping (_ error: Error) -> Void) throws {
            if(codigoPais != "CO" || numeroCelular != "3002134543") {
                onError(ApplicationException.Info("El codigo del Pais y numero de celular no son válidos"))
            }

            let empresaDTO = EmpresaDTO.init(uuid: "1", nombre: "Chiguagua Corp")

            let empleadoDTO = EmpleadoDTO.init(idEmpleado: "1", primerNombre: "Alvaro", otrosNombres: "", primerApellido: "Perez", segundoApellido: "Salcedo", correo: "alvaro.perez@ceiba.com.co", empresa: empresaDTO)

            onFinish(empleadoDTO)
        }

        open func registrarUsuario(codigoPais: String, numeroCelular: String, correo: String, clave: String, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {

            if(codigoPais != "CO" || numeroCelular != "3002134543" || correo != "lapso.app@gmail.com" || clave != "1234") {
                onError(ApplicationException.Info("No se pudo registrar el usuario"))
            }

            onFinish()

        }

        open func reenviarCodigoVerificacion(codigoPais: String, numeroCelular: String, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {

            if(codigoPais != "CO" || numeroCelular != "3002134543") {
                onError(ApplicationException.Info("Ocurrió un problema al reenviar código de verificación"))
            }

            onFinish()
        }

        open func validarCodigoVerificacion(codigoPais: String, numeroCelular: String, codigoVerificacion: String, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {

            if(codigoPais != "CO" || numeroCelular != "3002134543" || codigoVerificacion != "123456") {
                onError(ApplicationException.Info("Ocurrió un problema al validar código de verificación"))
            }

            onFinish()
        }

        open func cargarFotoDNI(uuidUsuario: String, fotoDNI: UIImage, tokenAutorizacion: String, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {

            let accessToken: String = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJzdWIiOiIxMjM0NTY3ODkwIiwibmFtZSI6IkpvaG4gRG9lIiwiYWRtaW4iOnRydWV9.TJVA95OrM7E2cBab30RMHrHDcEfxjoYZgeFONFh7HgQ"

            if(uuidUsuario != "1" || tokenAutorizacion != accessToken) {
                onError(ApplicationException.Info("Ocurrió un problema subiendo la imagen"))
            }

            onFinish()
        }

    }
    
#endif
