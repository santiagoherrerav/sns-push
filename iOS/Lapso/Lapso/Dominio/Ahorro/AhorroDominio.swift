import Foundation
import CeibaMobileCore


open class AhorroDominio {
    
    var ahorroRepositorio: AhorroRepositorioProtocolo
    
    public init(ahorroRepositorio: AhorroRepositorioProtocolo) throws {
        self.ahorroRepositorio = ahorroRepositorio
    }
    
    open func crearAhorrador(codigoEmpleado: String, idEmpresa: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws {
        
        if(codigoEmpleado.isEmpty || idEmpresa.isEmpty) {
            throw ApplicationException.Info("Los datos no son válidos")
        }
        
        try ahorroRepositorio.crearAhorrador(codigoEmpleado: codigoEmpleado, idEmpresa: idEmpresa, onFinish: onFinish, onError: onError)
    }
    
    open func consultarConfiguracionAhorro(idEmpresa: String, onFinish: @escaping (ConfiguracionAhorroDTO) -> Void, onError: @escaping (Error) -> Void) throws {
        
        if(idEmpresa.isEmpty) {
            throw ApplicationException.Info("Los datos no son válidos")
        }
        
        try ahorroRepositorio.consultarConfiguracionAhorro(idEmpresa: idEmpresa, onFinish: onFinish, onError: onError)
        
    }
    
    open func configurarAhorro(idEmpresa: String, porcentajeAhorro: Int, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {
        
        if(idEmpresa.isEmpty) {
            throw ApplicationException.Info("Los datos no son válidos")
        }
        
        try ahorroRepositorio.configurarAhorro( idEmpresa: idEmpresa,porcentajeAhorro: porcentajeAhorro, onFinish: onFinish, onError: onError)
    }
    
    open func consultarAhorrador(onFinish: @escaping (ResumenAhorradorDTO) -> Void, onError: @escaping (Error) -> Void) throws {
        try ahorroRepositorio.consultarAhorrador( onFinish: onFinish, onError: onError)
    }
    
    open func consultarCuentaCLABE(onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {
        try ahorroRepositorio.consultarCuentaCLABE(onFinish: onFinish, onError: onError)
    }
    
}
