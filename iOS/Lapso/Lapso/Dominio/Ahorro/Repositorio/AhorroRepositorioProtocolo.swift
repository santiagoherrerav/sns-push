import Foundation


public protocol AhorroRepositorioProtocolo {

    func crearAhorrador(codigoEmpleado: String, idEmpresa: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws

    func consultarConfiguracionAhorro(idEmpresa: String, onFinish: @escaping (_ object: ConfiguracionAhorroDTO) -> Void, onError: @escaping (_ error: Error) -> Void) throws

    func configurarAhorro(idEmpresa: String, porcentajeAhorro: Int, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws
    
    func consultarAhorrador(onFinish: @escaping (_ object: ResumenAhorradorDTO) -> Void, onError: @escaping (_ error: Error) -> Void) throws
    
    func consultarCuentaCLABE(onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws

}

