import Foundation
import CeibaMobileCore
import Alamofire
import SwiftyJSON
import AWSCognitoIdentityProvider


#if !MOCKS
    open class AhorroRepositorio: LapsoRepositorioBase, AhorroRepositorioProtocolo {
        
        public override init() { }

        open func crearAhorrador(codigoEmpleado: String, idEmpresa: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws {

            let poolCred = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
            let cliente = self.crearClientAsegurado(poolCred: poolCred)
            let body: LAPSOCrearAhorradorRequest = LAPSOCrearAhorradorRequest()
            
            body.codigoEmpleado = codigoEmpleado
            body.uuidEmpresa = idEmpresa
            
            cliente.ahorroCrearAhorradorPost(body: body).continueWith { [weak self] (task) -> Any? in
                guard self != nil else { return nil }
                DispatchQueue.main.async(execute: {
                    if let error = task.error as NSError? {
                        print(error)
                        onError(ApplicationException.Error("Ocurrió un problema creando al ahorrador"))
                    } else if task.result != nil {
                        onFinish()
                    }
                    
                })
                return nil
            }
        }
        
        open func consultarConfiguracionAhorro(idEmpresa: String, onFinish: @escaping (ConfiguracionAhorroDTO) -> Void, onError: @escaping (Error) -> Void) throws {
            
            let poolCred = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
            let idUsuario = poolCred.currentUser()?.username
            let cliente = self.crearClientAsegurado(poolCred: poolCred)

            cliente.empresaUuidEmpresaEmpleadosIdusuarioMontosEmpleadoGet(idusuario: idUsuario!, uuidEmpresa: idEmpresa).continueWith { [weak self] (task) -> Any? in
                guard self != nil else { return nil }
                DispatchQueue.main.async(execute: {
                    if task.error != nil {
                        onError(ApplicationException.Error("Ocurrió un problema consultando configuración de ahorro"))
                    } else if let result = task.result {
                        print(result)
                        let configuracionAhorro = ConfiguracionAhorroDTO(montoPago: result.montoPago as! Double, porcentajeMinimo: result.porcentajeMinimo as! Int, porcentajeMaximo: result.porcentajeMaximo as! Int, porcentajeAhorro: result.porcentajeAhorro as! Int)
                        onFinish(configuracionAhorro)
                    }
                })
                return nil
            }
        }

        open func configurarAhorro(idEmpresa: String, porcentajeAhorro: Int, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {
            
            let poolCred = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
            let idUsuario = poolCred.currentUser()?.username
            let cliente = self.crearClientAsegurado(poolCred: poolCred)
            
            let body : LAPSOConfigurarAhorroRequest = LAPSOConfigurarAhorroRequest()
            body.idUsuario = idUsuario
            body.uuidEmpresa = idEmpresa
            body.porcentajeAhorro = porcentajeAhorro as NSNumber
            cliente.empresaConfigurarAhorroPost(body: body).continueWith { [weak self] (task) -> Any? in
                guard self != nil else { return nil }
                DispatchQueue.main.async(execute: {
                    if task.error != nil {
                        onError(ApplicationException.Error("Ocurrió un problema consultando al ahorrador"))
                    } else if let result = task.result {
                        print(result)
                        onFinish()
                    }
                })
                return nil
            }
        }
        
        public func consultarAhorrador(onFinish: @escaping (ResumenAhorradorDTO) -> Void, onError: @escaping (Error) -> Void) throws {
            
            let poolCred = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
            let idUsuario = poolCred.currentUser()?.username
            let cliente = self.crearClientAsegurado(poolCred: poolCred)

            cliente.ahorroAhorradoresIdUsuarioGet(idUsuario: idUsuario!).continueWith { [weak self] (task) -> Any? in
                guard self != nil else { return nil }
                DispatchQueue  .main.async(execute: {
                    if task.error != nil {
                        onError(ApplicationException.Error("Ocurrió un problema consultando al ahorrador"))
                    } else if let result = task.result {
                        print(result)
                        onFinish(self!.procesarRespuestaConsultaAhorrador(respuesta: result ))
                    }
                    
                })
                return nil
            }
        }
        
        public func consultarCuentaCLABE(onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {
            
            let poolCred = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
            let idUsuario = poolCred.currentUser()?.username
            let cliente = self.crearClientAsegurado(poolCred: poolCred)
            
            cliente.cashOutAhorradoresIdAhorradorCuentaGet(idAhorrador: idUsuario!).continueWith { [weak self] (task) -> Any? in
                guard self != nil else { return nil }
                DispatchQueue  .main.async(execute: {
                    if task.error != nil {
                        onError(ApplicationException.Error("Ocurrió un problema consultando la cuenta"))
                    } else if let result = task.result {
                        print(result)
                        onFinish()
                    }
                    
                })
                return nil
            }
        }
        
        private func procesarRespuestaConsultaAhorrador(respuesta: LAPSOConsultarAhorradorResponse) -> ResumenAhorradorDTO {
            let ahorrador = ResumenAhorradorDTO()
            ahorrador.id = respuesta.id
            ahorrador.totalMontoBloqueado = respuesta.totalMontoBloqueado as! Int!
            ahorrador.totalMontoDisponible = respuesta.totalMontoDisponible as! Int!
            ahorrador.totalMontoMetas = respuesta.totalMontoMetas as! Int!
            ahorrador.tieneDocumentoIdentidad = respuesta.tieneDocumentoIdentidad as! Bool!
            ahorrador.porcentajeAhorro = self.procesarRespuestaConsultaAhorrador(respuesta: respuesta.porcentajeAhorro)
            return ahorrador
        }
        
        private func procesarRespuestaConsultaAhorrador(respuesta: [LAPSOConsultarSiTienePorcentajesResponse]) -> [AhorroEmpresaDTO] {
            var ahorroEmpresas = [AhorroEmpresaDTO]()
            for porcentaje in respuesta {
                let tienePorcentaje = Bool(exactly: porcentaje.tienePorcentajeAhorro)
                let ahorro = AhorroEmpresaDTO(uuidEmpresa: porcentaje.uuidEmpresa, nombreEmpresa: porcentaje.nombreEmpresa, tienePorcentajeAhorro: tienePorcentaje!)
                ahorroEmpresas.append(ahorro)
            }
            return ahorroEmpresas
        }
        
        
    }
    
#endif



