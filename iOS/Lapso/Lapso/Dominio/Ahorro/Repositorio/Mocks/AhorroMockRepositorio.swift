import Foundation
import CeibaMobileCore
import Alamofire
import SwiftyJSON


#if MOCKS
    open class AhorroMockRepositorio: AhorroRepositorioProtocolo {
        
        public init() { }
    
        public func crearAhorrador(codigoEmpleado: String, idEmpresa: String, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {
        
            if(codigoEmpleado != "1" || idEmpresa != "1") {
                onError(ApplicationException.Error("Ocurrió un problema creando al ahorrador"))
            }
            
            onFinish()
        }
        
        public func consultarConfiguracionAhorro(idEmpresa: String, onFinish: @escaping (ConfiguracionAhorroDTO) -> Void, onError: @escaping (Error) -> Void) throws {
           if(idEmpresa != "1") {
                onError(ApplicationException.Error("Ocurrió un problema consultando configuración de ahorro"))
            }
            
            let configuracionAhorroDTO = ConfiguracionAhorroDTO.init(montoPago: 20.00, porcentajeMinimo: 1, porcentajeMaximo: 10, porcentajeAhorro: 1)
            
            onFinish(configuracionAhorroDTO)
            
        }
        
        public func configurarAhorro(idEmpresa: String, porcentajeAhorro: Int, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {
           if(idEmpresa != "1" || porcentajeAhorro != 5) {
                onError(ApplicationException.Error("Ocurrió un problema configurando el ahorro"))
            }
            
            onFinish()
            
        }
        
        public func consultarAhorrador(onFinish: @escaping (ResumenAhorradorDTO) -> Void, onError: @escaping (Error) -> Void) throws {
            var ahorroEmpresas = [AhorroEmpresaDTO]()
            let ahorro = AhorroEmpresaDTO()
            ahorro.uuidEmpresa = "1"
            ahorro.nombreEmpresa = "Ceiba"
            ahorro.tienePorcentajeAhorro = false

           
            ahorroEmpresas.append(ahorro)
            
            
            let resumenAhorrador = ResumenAhorradorDTO.init(id:"1", totalMontoBloqueado: 100000, totalMontoDisponible: 40000, totalMontoMetas: 20000, tieneDocumentoIdentidad: true, porcentajeAhorro: ahorroEmpresas)
            onFinish(resumenAhorrador)
        }
        
        public func consultarCuentaCLABE(onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {
            
        }
        
    }
    
#endif
