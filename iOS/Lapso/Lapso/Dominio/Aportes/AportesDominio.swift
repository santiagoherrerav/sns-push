import Foundation
import CeibaMobileCore


open class AportesDominio {
    
    var aportesRepositorio: AportesRepositorioProtocolo

    
    public init(aportesRepositorio: AportesRepositorioProtocolo) throws {
        self.aportesRepositorio = aportesRepositorio
    }
    
    open func iniciarAporteMeta(uuidMeta: String, monto: Double, destinoAporteMeta: String, onFinish: @escaping (_ movimiento: MovimientoDTO) -> Void, onError: @escaping (Error) -> Void) throws {
        
        if(monto <= 0) {
            throw ApplicationException.Info("Los datos no son válidos")
        }
        
        let aporteExtra = AporteExtraDTO(uuidMeta: uuidMeta, monto: monto, destinoAporte: destinoAporteMeta)
        
        
        try aportesRepositorio.iniciarAporte(aporteExtra: aporteExtra, onFinish: onFinish, onError: onError)
    }
    
    open func actualizarAporte(movimientoDTO: MovimientoDTO, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {
        try aportesRepositorio.actualizarAporte(movimientoDTO: movimientoDTO, onFinish: onFinish, onError: onError)
    }
}
