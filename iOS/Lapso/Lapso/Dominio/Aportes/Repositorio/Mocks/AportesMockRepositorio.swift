import Foundation
import CeibaMobileCore
import Alamofire
import SwiftyJSON


#if MOCKS
    open class AportesMockRepositorio: AportesRepositorioProtocolo {

        public init() { }

        open func iniciarAporte(aporteExtra: AporteExtraDTO, onFinish: @escaping (_ movimiento: MovimientoDTO) -> Void, onError: @escaping (Error) -> Void) throws {
            let movimiento = MovimientoDTO()
            movimiento.idMovimiento = 1
            onFinish(movimiento)
        }

        public func actualizarAporte(movimientoDTO: MovimientoDTO, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {
            onFinish()
        }

    }
#endif

