import Foundation
import CeibaMobileCore
import Alamofire
import SwiftyJSON
import AWSCognitoIdentityProvider



#if !MOCKS
    open class AportesRepositorio: LapsoRepositorioBase, AportesRepositorioProtocolo {

        public override init() { }
        var pool: AWSCognitoIdentityUserPool? = AWSCognitoIdentityUserPool.init(forKey: AWSCognitoUserPoolsSignInProviderKey)


        public func iniciarAporte(aporteExtra: AporteExtraDTO, onFinish: @escaping (_ movimiento: MovimientoDTO) -> Void, onError: @escaping (Error) -> Void) throws {
            
            let poolCred = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
            let idAhorrador = poolCred.currentUser()?.username
            let cliente = self.crearClientAsegurado(poolCred: poolCred)
            
            let body: LAPSOIniciarAporteRequest = LAPSOIniciarAporteRequest()
            
            body.destinoAporte = aporteExtra.destinoAporte
            body.idUsuario = idAhorrador
            body.monto = NSNumber.init(value: aporteExtra.monto)
            if(aporteExtra.destinoAporte == DestinoAporte.META.rawValue){
                body.uuidMeta = aporteExtra.uuidMeta
            }
            
            cliente.cashInIniciarAporteAhorradorPost(body: body).continueWith { [weak self] (task) -> Any? in
                guard self != nil else { return nil }
                DispatchQueue.main.async(execute: {
                    if task.error != nil {
                        onError(ApplicationException.Error("Ocurrió un error al iniciar aporte"))
                    } else if let result = task.result {
                        print(result)
                        let movimiento = MovimientoDTO()
                        movimiento.idMovimiento = Int(truncating: result.idMovimiento)
                        movimiento.referencia = result.referencia
                        onFinish(movimiento)
                    }
                })
                return nil
            }
        }

        public func actualizarAporte(movimientoDTO: MovimientoDTO, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {
            
            let poolCred = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
            let cliente = self.crearClientAsegurado(poolCred: poolCred)
            
            let body  : LAPSOAcreditarAporteRequest = LAPSOAcreditarAporteRequest()
            
            body.referencia = movimientoDTO.referencia! 
            body.estado = movimientoDTO.estado!
            body.monto = NSNumber.init(value: movimientoDTO.monto)
            body.mensaje = movimientoDTO.mensaje ?? ""
            body.idTransaccion = movimientoDTO.idTransaccion ?? ""
            body.idMovimiento = NSNumber.init(value: movimientoDTO.idMovimiento)
            
            cliente.cashInAcreditarAporteAhorradorPost(body: body).continueWith { [weak self] (task) -> Any? in
                guard self != nil else { return nil }
                DispatchQueue.main.async(execute: {
                    if let error = task.error {
                        print(error)
                        onError(ApplicationException.Error("Ocurrió un error al actualizar aporte"))
                    } else if let result = task.result {
                        print(result)
                        onFinish()
                    }
                })
                return nil
            }
        }
    }

#endif


