import Foundation


public protocol AportesRepositorioProtocolo {
    
    func iniciarAporte(aporteExtra: AporteExtraDTO, onFinish: @escaping (_ movimiento: MovimientoDTO) -> Void, onError: @escaping (_ error: Error) -> Void) throws
    
     func actualizarAporte(movimientoDTO: MovimientoDTO, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws
}
