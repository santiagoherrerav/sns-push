import Foundation
import SwiftDate

open class UFechas {

    let gregorian = Calendar(identifier: .gregorian)

    init() { }

    open func contarMeses(fechaInicial: Date, fechaFinal: Date) -> Int {
        let compFechaInicial = gregorian.dateComponents([.year, .month], from: fechaInicial)

        let compFechaFinal = gregorian.dateComponents([.year, .month], from: fechaFinal)

        let primerDiaMesFechaInicial = gregorian.date(from: compFechaInicial)
        let primerDiaMesFechaFinal = gregorian.date(from: compFechaFinal)

        let theComponents = gregorian.dateComponents([.month], from: primerDiaMesFechaInicial!, to: primerDiaMesFechaFinal!)

        return theComponents.month!
    }

    open func contarSemanas(fechaInicial: Date, fechaFinal: Date) -> Int {

        let primerDiaSemanaFechaInicial = fechaInicial.startOfWeek
        let primerDiaSemanaFechaFinal = fechaFinal.startOfWeek


        let theComponents = gregorian.dateComponents([.weekOfYear], from: primerDiaSemanaFechaInicial!, to: primerDiaSemanaFechaFinal!)
        return theComponents.weekOfYear!
    }


}
