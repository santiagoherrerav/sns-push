import Foundation

open class UCadenas{
    
    init() {
    }
    
    open func caracteresSonDigitos ( cadena : String ) -> Bool{
        for caracter in cadena.map(String.init) {
            let num = Int(caracter)
            if num == nil {
                return false
            }
        }
        return true
    }
    
    open func caracteresSonDigitosConsecutivos ( cadena : String) -> Bool {
        let numeros = cadena.map(String.init)
        for index in 0...(numeros.count - 2){
            let digitoActual : Int =  Int(numeros[index])!
            let digitoPosterior : Int = Int(numeros[index + 1])!
            let valorSiguiente : Int = digitoActual == 9 ? 0 :  digitoActual + 1
            
            if valorSiguiente !=  digitoPosterior{
                return false
            }
        }
        return true
    }
    
    open func caracteresSonDigitosConsecutivosDesc ( cadena : String) -> Bool {
        let numeros = cadena.map(String.init)
        for index in 0...(numeros.count - 2){
            let digitoActual : Int =  Int(numeros[index])!
            let digitoPosterior : Int = Int(numeros[index + 1])!
            let valorSiguiente : Int = digitoActual == 0 ? 9 :  digitoActual - 1
            
            if valorSiguiente !=  digitoPosterior{
                return false
            }
        }
        return true
    }
    
    open func caracteresSonIguales (cadena : String) -> Bool { 
        let numeros = cadena.map(String.init)
        for index in 1...(numeros.count - 1){
            let digitoActual : Int =  Int(numeros[index])!
            let digitoAnterior : Int = Int(numeros[index - 1])!
            if digitoActual !=  digitoAnterior{
                return false
            }
        }
        return true
    }
    
}
