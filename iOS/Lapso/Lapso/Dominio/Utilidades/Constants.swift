import Foundation
import AWSCognitoIdentityProvider

public let CognitoIdentityUserPoolRegion: AWSRegionType = .USEast1
public let AWSCognitoUserPoolsSignInProviderKey = "UserPool"
public let LAPSOIdVPCClient = "LAPSOLapsoVPCClient";

#if DEV
    public let CognitoIdentityUserPoolId = "us-east-1_aK7sR14S5"
    public let CognitoIdentityUserPoolAppClientId = "48i7ei75bfv64efadujnn45hq1"
    public let CognitoIdentityUserPoolAppClientSecret = "1sggqc58q3m4jv13kq46td6busgqhc95ft6rfvnn6rkn7a6ui7ja"
    public let LAPSOIdentityPoolID = "us-east-1:a0efdce7-ed2f-4faf-a4fe-027bc5d307d9";
    public let domain = "https://ohwrvs29yb.execute-api.us-east-1.amazonaws.com/dev"

#elseif QA
    public let CognitoIdentityUserPoolId = "us-east-1_oUBSGrPDD"
    public let CognitoIdentityUserPoolAppClientId = "a2cb2vhesitk69uo05t1vr1sm"
    public let CognitoIdentityUserPoolAppClientSecret = "84h5q7qfncjuvkkg2oqtv0ehe8gn9dddiu28ne2q71l5qg7ee9g"
    public let LAPSOIdentityPoolID = "us-east-1:a9196286-3c9d-4625-9210-6cd5082fbbb7";
    public let domain = "https://ohwrvs29yb.execute-api.us-east-1.amazonaws.com/qa"

#elseif PROD
    public let CognitoIdentityUserPoolId = ""
    public let CognitoIdentityUserPoolAppClientId = ""
    public let CognitoIdentityUserPoolAppClientSecret = ""
    public let LAPSOIdentityPoolID = "";
    public let domain = ""

#elseif MOCKS
    public let CognitoIdentityUserPoolId = ""
    public let CognitoIdentityUserPoolAppClientId = ""
    public let CognitoIdentityUserPoolAppClientSecret = ""
    public let LAPSOIdentityPoolID = "";
    public let domain = ""

#endif

