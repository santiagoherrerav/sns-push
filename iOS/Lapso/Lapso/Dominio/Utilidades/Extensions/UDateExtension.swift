import Foundation

extension Date {
    
    var startOfWeek: Date? {
        return Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))
    }
    
    var endOfWeek: Date? {
        guard let sunday = Calendar.gregorian.date(from: Calendar.gregorian.dateComponents([.yearForWeekOfYear, .weekOfYear], from: self)) else { return nil }
        return Calendar.gregorian.date(byAdding: .day, value: 7, to: sunday)
    }
    
    func startOfMonth() -> Date? {
        let comp: DateComponents = Calendar.current.dateComponents([.year, .month, .hour], from: Calendar.current.startOfDay(for: self))
        return Calendar.current.date(from: comp)!
    }
    
    func endOfMonth() -> Date? {
        var comp: DateComponents = Calendar.current.dateComponents([.month, .day, .hour], from: Calendar.current.startOfDay(for: self))
        comp.month = 1
        comp.day = -1
        return Calendar.current.date(byAdding: comp, to: self.startOfMonth()!)
    }
    
    func withDayOfMonth(day: Int) -> Date {
        return Calendar.current.date(from: DateComponents(year: self.year, month: self.month, day: day))!
    }
    
    func getMonthOfYear() -> Int {
        return Calendar.current.component(.month, from: self)
    }
    
    func getDayOfMonth () -> Int {
        return Calendar.current.component(.day, from: self)
    }
    
    func plusDays (number: Int) -> Date {
        return Calendar.current.date(byAdding: .day, value: number, to: self)!
    }
    
    func isBefore(date: Date) -> Bool {
        return Calendar.current.compare(date, to: self, toGranularity: .day) == .orderedDescending
    }
    
    func isAfter(date: Date) -> Bool {
        return Calendar.current.compare(date, to: self, toGranularity: .day) == .orderedAscending
    }
    
    func isEqual(date: Date) -> Bool {
        return Calendar.current.compare(date, to: self, toGranularity: .day) == .orderedSame
    }
    
    func getDayOfWeek() -> Int? {
        return Calendar(identifier: .iso8601).dateComponents([.weekday], from: self).weekday
    }
    
    func withDayOfWeek (day: Int) -> Date {
        
        var mondaysDate: Date {
            return Calendar(identifier: .iso8601).date(from: Calendar(identifier: .iso8601).dateComponents([.yearForWeekOfYear, .weekOfYear], from: self))!
        }
        
        var components = Calendar(identifier: .iso8601).dateComponents([.yearForWeekOfYear, .weekOfYear, .weekday], from: mondaysDate)
        
        //mm buque o no_
        components.weekday? += (day - 1)
        
        var dayOfWeekDate: Date {
            return Calendar(identifier: .iso8601).date(from: components)!
        }
        
        return dayOfWeekDate
    }
    
}
