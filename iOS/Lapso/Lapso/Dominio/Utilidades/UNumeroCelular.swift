import Foundation
import PhoneNumberKit

open class UNumeroCelular {

    init() {
    }
    
   open func obtenerCelularEnFormatoE164(codigoPais: String, numeroCelular: String) -> String {
        var formattedString: String? = nil
        
        do {
            let phoneNumberKit = PhoneNumberKit()
            
            let phoneNumber = try phoneNumberKit.parse(numeroCelular, withRegion: codigoPais, ignoreType: true)
            
            formattedString = phoneNumberKit.format(phoneNumber, toType: .e164)
            
        } catch {
            print("Generic parser error")
        }
        
        return formattedString!
    }
    
}
