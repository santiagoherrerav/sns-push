import Foundation
import CeibaMobileCore

open class MetasDominio {

    var metasRepositorio: MetasRepositorioProtocolo
    let uFechas: UFechas = UFechas()

    let diasMesContable = 30
    let mitadMesContable = 15
    let numeroQuincenasPorMes = 2
    
    var metasUsuario : [MetaDTO] =  [MetaDTO]()

    public init(metasRepositorio: MetasRepositorioProtocolo) throws {
        self.metasRepositorio = metasRepositorio
    }
    
    open func consultarMetas( onFinish: @escaping (_ object: [MetaDTO]) -> Void, onError: @escaping (_ error: Error) -> Void) throws{
        try metasRepositorio.consultarMetasPorUsuario( onFinish: onFinish, onError: onError)
    }
    
    open func crearMeta(meta : MetaDTO, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws{
        try metasRepositorio.crearMeta(meta: meta, onFinish: onFinish, onError: onError)
    }
    
    open func consultarFrecuenciaPago( idEmpresa : String, onFinish: @escaping (_ object: FrecuenciaPago) -> Void, onError: @escaping (_ error: Error) -> Void) throws{
        
        if(idEmpresa.isEmpty ) {
            throw ApplicationException.Info("Los datos no son válidos")
        }
        
        try metasRepositorio.consultarFrecuenciaPago( idEmpresa: idEmpresa, onFinish: onFinish, onError: onError)
    }
    
    open func simularAportePeriodico(fechaActual: Date, fechaFinal: Date, montoObjetivo: Int, frecuenciaPago: FrecuenciaPago) -> Double {
        
        var numPagos: Int = 0;
        
        switch frecuenciaPago {
        case .SEMANAL:
            numPagos = uFechas.contarSemanas(fechaInicial: fechaActual, fechaFinal: fechaFinal) + 1 - calcularNumSemanasMesAntesCorte(fecha: fechaFinal)
        case .QUINCENAL:
            numPagos = ((uFechas.contarMeses(fechaInicial: fechaActual, fechaFinal: fechaFinal) + 1) * numeroQuincenasPorMes)
                - calcularQuincenasDespuesCorte(fecha: fechaActual) - calcularQuincenasAntesCorte(fecha: fechaFinal)
        case .MENSUAL:
            let numPagos2 = uFechas.contarMeses(fechaInicial: fechaActual, fechaFinal: fechaFinal)
            numPagos = numPagos2 + 1 - calcularNumPagosMesAntesFechaCorte(fecha: fechaFinal);
        }
        
        //todo: validar division por cero hay que cambiar
        return Double(montoObjetivo / (numPagos == 0 ? 1 : numPagos))
    }
    
    
    private func calcularNumSemanasMesAntesCorte( fecha: Date ) -> Int{
        return fecha.getDayOfWeek()! < Dias.VIERNES.rawValue ? 1 : 0
    }
    
    private func calcularQuincenasDespuesCorte( fecha : Date ) -> Int{
         return fecha.getDayOfMonth() <= mitadMesContable ? 0 : 1
    }
    
    private func calcularQuincenasAntesCorte( fecha : Date ) -> Int{
        
        if fecha.getDayOfMonth()  < mitadMesContable {
            return numeroQuincenasPorMes
        } else{
             let fechaCorteMensual = calcularFechaCorteMensual(fecha: fecha)
            
            if fecha.isBefore(date: fechaCorteMensual) {
                return 1
            } else {
                return 0
            }
        }
        
    }
    
    private func calcularNumPagosMesAntesFechaCorte( fecha : Date ) -> Int{
        let fechaCorteMensual = calcularFechaCorteMensual(fecha: fecha)
        return fecha.isBefore(date: fechaCorteMensual) ? 1: 0
    }
    
    open func calcularFechaMininaMeta(fechaActual: Date, frecuenciaPago: FrecuenciaPago) -> Date {

        var fechaMininaMeta: Date? = nil

        switch frecuenciaPago {
        case .SEMANAL:
            fechaMininaMeta = calcularFechaMinimaSemanal(fechaActual: fechaActual)
        case .QUINCENAL:
            fechaMininaMeta = calcularFechaMinimaQuincenal(fechaActual: fechaActual)
        case .MENSUAL:
            fechaMininaMeta = calcularFechaMinimaMensual(fechaActual: fechaActual)
        }

        return fechaMininaMeta!
    }

    private func calcularFechaMinimaMensual(fechaActual: Date) -> Date {
        let fechaCorteMensual: Date = calcularFechaCorteMensual(fecha: fechaActual)
        var fechaMininaMeta: Date? = nil;

        if fechaActual.isBefore(date: fechaCorteMensual) {
            fechaMininaMeta = fechaCorteMensual
        } else {
            fechaMininaMeta = fechaCorteMensual.plusDays(number: 1)
            if (fechaMininaMeta?.isEqual(date: fechaActual))! {
                fechaMininaMeta = fechaMininaMeta?.plusDays(number: 1)
            }
        }

        return fechaMininaMeta!
    }

    private func calcularFechaMinimaQuincenal(fechaActual: Date) -> Date {
        let fechaCorteQuincenal: Date = calcularFechaCorteQuincenal(fecha: fechaActual)
        var fechaMininaMeta: Date? = nil;

        if fechaActual.isBefore(date: fechaCorteQuincenal) {
            fechaMininaMeta = fechaCorteQuincenal
        } else {
            if fechaActual.isEqual(date: fechaCorteQuincenal) || fechaActual.isAfter(date: fechaCorteQuincenal) {
                fechaMininaMeta = fechaActual.plusDays(number: 1);
            } else {
                fechaMininaMeta = fechaCorteQuincenal;
            }
        }

        return fechaMininaMeta!
    }

    
    private func calcularFechaMinimaSemanal(fechaActual: Date) -> Date {
        let fechaCorteSemanal: Date = fechaActual.withDayOfWeek(day: Dias.VIERNES.rawValue)
        var fechaMininaMeta: Date? = nil;
        
        if fechaActual.isBefore(date: fechaCorteSemanal) {
            fechaMininaMeta = fechaCorteSemanal
        } else {
            fechaMininaMeta = fechaActual.plusDays(number: 1)
        }
        
        return fechaMininaMeta!
    }

    
    private func calcularFechaCorteMensual(fecha: Date) -> Date {
        return fecha.getMonthOfYear() == Meses.FEBRERO.rawValue ? fecha.endOfMonth()! : fecha.withDayOfMonth(day: diasMesContable)
    }

    private func calcularFechaCorteQuincenal(fecha: Date) -> Date {
        return fecha.getDayOfMonth() <= mitadMesContable ? fecha.withDayOfMonth(day: mitadMesContable) : calcularFechaCorteMensual(fecha: fecha)
    }

}



