import Foundation


public protocol MetasRepositorioProtocolo {
    
    func consultarFrecuenciaPago( idEmpresa: String, onFinish: @escaping (_ object: FrecuenciaPago) -> Void, onError: @escaping (_ error: Error) -> Void) throws
    
    func crearMeta( meta : MetaDTO, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws
    
    func consultarMetasPorUsuario( onFinish: @escaping (_ object: [MetaDTO]) -> Void, onError: @escaping (_ error: Error) -> Void) throws
    
}

