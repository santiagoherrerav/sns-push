import Foundation
import CeibaMobileCore
import Alamofire
import SwiftyJSON
import AWSCognitoIdentityProvider



#if !MOCKS
    open class MetasRepositorio: LapsoRepositorioBase, MetasRepositorioProtocolo {
        
        let formateDate = "yyyy-MM-dd'T'HH:mm:ss"

        public override init() { }
        
        public func consultarFrecuenciaPago(idEmpresa: String, onFinish: @escaping (_ object: FrecuenciaPago)  -> Void, onError: @escaping (_ error: Error) -> Void) throws {
            
            let poolCred = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
            let idUsuario = poolCred.currentUser()?.username
            let cliente = self.crearClientAsegurado(poolCred: poolCred)
            
            cliente.empresaUuidEmpresaEmpleadosIdusuarioFrecuenciaDePagoGet(idusuario: idUsuario!, uuidEmpresa: idEmpresa).continueWith { [weak self] (task) -> Any? in
                guard self != nil else { return nil }
                DispatchQueue.main.async(execute: {
                    if task.error != nil {
                        onError(ApplicationException.Error("Ocurrió un problema consultando la frecuencia pago"))
                    } else if let result = task.result {
                        print(result)
                        if result is LAPSOConsultarFrecuenciaPagoResponse{
                            let frecuenciaPago = FrecuenciaPago(rawValue: result.frecuenciaDePago)!
                            onFinish(frecuenciaPago)
                        } else {
                            onError(ApplicationException.Error("Ocurrió un problema consultando la frecuencia pago"))
                        }
                    }
                    
                })
                return nil
            }
        }
        
        public func crearMeta(meta: MetaDTO, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {
            
            let poolCred = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
            let idUsuario = poolCred.currentUser()?.username
            let cliente = self.crearClientAsegurado(poolCred: poolCred)
            let fechaFinal : String = meta.fechaFinal.stringFromDate8601()
            
            let body : LAPSOCrearMetaRequest = LAPSOCrearMetaRequest()
            body.idAhorrador = idUsuario
            body.nombre = meta.nombre
            body.monto = meta.monto! as NSNumber
            body.fechaFinal = fechaFinal
            body.montoProgramado = 0
            
            cliente.metaCrearMetaPost(body: body).continueWith { [weak self] (task) -> Any? in
                guard self != nil else { return nil }
                DispatchQueue.main.async(execute: {
                    if task.error != nil {
                        onError(ApplicationException.Error("Ocurrió un problema creando la meta"))
                    } else if let result = task.result {
                        print(result)
                        onFinish()
                    }
                })
                return nil
            }
        }
        
        public func consultarMetasPorUsuario(onFinish: @escaping ([MetaDTO]) -> Void, onError: @escaping (Error) -> Void) throws {
            
            let poolCred = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
            let idUsuario = poolCred.currentUser()?.username
            let cliente = self.crearClientAsegurado(poolCred: poolCred)
            
            cliente.metaAhorradoresIdAhorradorMetasGet(idAhorrador: idUsuario!).continueWith { [weak self] (task) -> Any? in
                guard self != nil else { return nil }
                DispatchQueue.main.async(execute: {
                    if task.error != nil {
                        onError(ApplicationException.Error("Ocurrió un problema consultando las metas del usuario"))
                    } else if let result = task.result {
                        print(result)
                        onFinish(self!.procesarRespuestaConsultarMetas(respuesta: result))
                    }
                })
                return nil
            }
        }
        
        private func procesarRespuestaConsultarMetas(respuesta: LAPSOConsultarMetasResponse) -> [MetaDTO] {
            var metas = [MetaDTO]()
            
            for meta in respuesta.metas{
                let fechaFinal =  Date.dateFromISO8601String(string: meta.fechaFinal)
                let metaDTO = MetaDTO(uuid: meta.uuid, nombre: meta.nombre, fechaFinal: fechaFinal!, monto: meta.monto as! Int, montoProgramado: meta.montoProgramado as! Int, idImagen: meta.idImagen as! Int, bloqueada: (meta.bloqueada != nil), ahorro: meta.ahorro as! Int)
                metas.append(metaDTO)
            }
            
            return metas
        }
    }
#endif

extension Formatter {
    static let dotNetDateTime: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-MM-dd'T'HH:mm:ss"
        return formatter
    }()
}

extension Date {
    func stringFromDate8601() -> String {
        let dateFormatter = ISO8601DateFormatter()
        return dateFormatter.string(from: self)
    }
    
    static func dateFromISO8601String(string: String) -> Date? {
        let dateFormatter = ISO8601DateFormatter()
        return dateFormatter.date(from: string)
    }
}
