import Foundation
import CeibaMobileCore
import Alamofire
import SwiftyJSON


#if MOCKS
    open class MetasMockRepositorio: MetasRepositorioProtocolo {
        
        
        public func consultarFrecuenciaPago(idEmpresa: String, onFinish: @escaping (FrecuenciaPago) -> Void, onError: @escaping (Error) -> Void) throws {
            if(idEmpresa != "1") {
                onError(ApplicationException.Error("Ocurrió un problema al consultar frecuencia pago"))
            }
            
            let frecuencia  = FrecuenciaPago(rawValue: "MENSUAL")!
            onFinish(frecuencia)
        }
        
        
        public init() { }
        
        open func consultarFrecuenciaPago(idUsuario: String, idEmpresa: String, tokenAutorizacion: String, onFinish: @escaping () -> Void, onError: @escaping (_ error: Error) -> Void) throws {

        }
        
        public func crearMeta(meta: MetaDTO, onFinish: @escaping () -> Void, onError: @escaping (Error) -> Void) throws {
            onFinish()
        }
        
        public func consultarMetasPorUsuario(onFinish: @escaping ([MetaDTO]) -> Void, onError: @escaping (Error) -> Void) throws {
                var metas = [MetaDTO]()
                let meta = MetaDTO()
                meta.nombre = "iphone"
                meta.ahorro = 10000
                meta.idImagen = 1
                meta.uuid = "e8dcac30-fa60-11e7-a084-6b4cba42674c"
                meta.monto = 100000
                meta.fechaFinal = Calendar.current.date(from: DateComponents(year: 2019, month: 12, day: 1))
                metas.append(meta)
                
                let meta2 = MetaDTO()
                meta2.nombre = "computador"
                meta2.ahorro = 30000
                meta2.idImagen = 2
                meta2.uuid = "e6dcac30-fa60-11e7-a084-6b4cba42674c"
                meta2.monto = 600000
                metas.append(meta2)
                
                onFinish(metas)
        }
    }
#endif
