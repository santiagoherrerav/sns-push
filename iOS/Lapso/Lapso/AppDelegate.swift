import UIKit
import UserNotifications
import Swinject
import Fabric
import Crashlytics
import AWSCognitoIdentityProvider

class AppDelegate: UIResponder, UIApplicationDelegate, AWSCognitoIdentityInteractiveAuthenticationDelegate {

    var window: UIWindow?
    var sesionUsuarioDominio: SesionUsuarioDominio!

    let container: Container = {
        let container = Container()
        #if MOCKS
            container.register(RegistroRepositorioProtocolo.self) { _ in RegistroMockRepositorio() }
            container.register(MetasRepositorioProtocolo.self) { _ in MetasMockRepositorio() }
            container.register(SesionUsuarioRepositorioProtocolo.self) { _ in SesionUsuarioMockRepositorio() }
            container.register(AhorroRepositorioProtocolo.self) { _ in AhorroMockRepositorio() }
            container.register(AportesRepositorioProtocolo.self) { _ in AportesMockRepositorio() }
        #else
            container.register(RegistroRepositorioProtocolo.self) { _ in RegistroRepositorio() }
            container.register(MetasRepositorioProtocolo.self) { _ in MetasRepositorio() }
            container.register(SesionUsuarioRepositorioProtocolo.self) { _ in SesionUsuarioRepositorio() }
            container.register(AhorroRepositorioProtocolo.self) { _ in AhorroRepositorio() }
            container.register(AportesRepositorioProtocolo.self) { _ in AportesRepositorio() }
        #endif
        return container

    }()


    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        Fabric.with([Crashlytics.self])
        configureStatusBarStyle()
        setupCognitoUserPool()
        inicializarDominio()
        inicializarTimer()
        //changeInitialViewController(storyboardName: "PanelControl", withIdentifier: "PanelContainerViewController")
        
        registerForPushNotifications()
        return true
    }    
    
    func registerForPushNotifications() {
        UNUserNotificationCenter.current().requestAuthorization(options: [.alert, .sound, .badge]) {
            (granted, error) in
            print("Permission granted: \(granted)")
            
            guard granted else { return }
            self.getNotificationSettings()
        }
    }
    
    func getNotificationSettings() {
        UNUserNotificationCenter.current().getNotificationSettings { (settings) in
            print("Notification settings: \(settings)")
            guard settings.authorizationStatus == .authorized else { return }
            UIApplication.shared.registerForRemoteNotifications()
        }
    }
    
    private func inicializarTimer() {
        NotificationCenter.default.addObserver(self,
            selector: #selector(AppDelegate.applicationDidTimeout(notification:)),
            name: .appTimeout,
            object: nil
        )
    }

    private func inicializarDominio() {

        let sesionUsuarioRepositorio = container.resolve(SesionUsuarioRepositorioProtocolo.self)!

        do {
            try sesionUsuarioDominio = SesionUsuarioDominio(sesionUsuarioRepositorio: sesionUsuarioRepositorio)
        } catch {
            print("Ocurrió un error iniciando la app")
        }
    }


    func changeInitialViewController(storyboardName: String, withIdentifier: String) {
        let storyboard = UIStoryboard(name: storyboardName, bundle: nil)

        let initialViewController = storyboard.instantiateViewController(withIdentifier: withIdentifier)

        self.window?.rootViewController = initialViewController
        self.window?.makeKeyAndVisible()
    }

    func configureStatusBarStyle() {
        UIApplication.shared.statusBarStyle = .lightContent

        UINavigationBar.appearance().clipsToBounds = true

        let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView

        statusBar.backgroundColor = UIColor(red: 22 / 255, green: 27 / 255, blue: 105 / 255, alpha: 1.0)
    }

    func setupCognitoUserPool() {
        if (CognitoIdentityUserPoolId == "YOUR_USER_POOL_ID") {
            print("Invalid Configuration: Please configure user pool constants in Constants.swift file.")
        }

        AWSDDLog.sharedInstance.logLevel = .verbose

        let serviceConfiguration = AWSServiceConfiguration(region: .USEast1, credentialsProvider: nil)

        let userPoolConfiguration = AWSCognitoIdentityUserPoolConfiguration(clientId: CognitoIdentityUserPoolAppClientId, clientSecret: CognitoIdentityUserPoolAppClientSecret, poolId: CognitoIdentityUserPoolId)

        AWSCognitoIdentityUserPool.register(with: serviceConfiguration, userPoolConfiguration: userPoolConfiguration, forKey: AWSCognitoUserPoolsSignInProviderKey)

        let pool = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
        pool.delegate = self

        LAPSOLapsoVPCClient.registerClient(withConfiguration: serviceConfiguration!, forKey: LAPSOIdVPCClient)
    }

    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    func application(_ application: UIApplication,
                     didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        let deviceTokenAsString = deviceToken.reduce("", {$0 + String(format: "%02X", $1)})
        print("Device Token: \(deviceTokenAsString)")
    }
    
    func application(_ application: UIApplication,
                     didFailToRegisterForRemoteNotificationsWithError error: Error) {
        print("Failed to register: \(error)")
    }
    
    func application(_ application: UIApplication, didReceiveRemoteNotification userInfo: [AnyHashable : Any], fetchCompletionHandler completionHandler: @escaping (UIBackgroundFetchResult) -> Void) {
        
        print(userInfo)
        completionHandler(UIBackgroundFetchResult.noData)
        
        UserDefaults.standard.set(true, forKey: "didReceiveRemoteNotification")
        UserDefaults.standard.synchronize()
        
    }

    
    @objc func applicationDidTimeout(notification: NSNotification) {
        self.cerrarSesion()
    }
    
    private func cerrarSesion() {
        self.sesionUsuarioDominio.cerrarSesion(onFinish: {
            self.changeInitialViewController(storyboardName: "IniciarSesion", withIdentifier: "IniciarSesionViewController")
        })
    }
}

