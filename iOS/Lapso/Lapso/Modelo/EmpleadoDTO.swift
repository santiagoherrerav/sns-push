import Foundation

open class EmpleadoDTO: Codable {
    
    public var idEmpleado: String!
    public var primerNombre: String!
    public var otrosNombres: String!
    public var primerApellido: String!
    public var segundoApellido: String!
    public var correo: String!
    public var empresa: EmpresaDTO!
    public var clave: String!
    public var siglaPais: String!
    public var celular: String!

    public init(){}
    
    public init(idEmpleado:String, primerNombre: String, otrosNombres: String, primerApellido: String, segundoApellido: String, correo: String, empresa: EmpresaDTO) {
        self.idEmpleado = idEmpleado
        self.primerNombre = primerNombre
        self.otrosNombres = otrosNombres
        self.primerApellido = primerApellido
        self.segundoApellido = segundoApellido
        self.correo = correo
        self.empresa = empresa
    }
    
}
