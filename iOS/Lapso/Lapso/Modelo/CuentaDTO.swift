import Foundation


open class CuentaDTO: Codable {
    
    public var id: Int!
    public var numero: String!
    public var estadoCuenta: EstadoCuenta!
    
    public init(){}
    
    public init(id: Int,  numero: String, estadoCuenta : EstadoCuenta) {
        self.id = id
        self.numero = numero
        self.estadoCuenta = estadoCuenta
    }
    
}


