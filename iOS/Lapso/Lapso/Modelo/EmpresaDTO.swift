import Foundation

open class EmpresaDTO: Codable {
    
    public var uuid: String!
    public var nombre: String!
    
    public init(){}
    
    public init(uuid: String,  nombre: String) {
        self.uuid = uuid
        self.nombre = nombre
    }
    
}
