import Foundation

open class ConfiguracionAhorroDTO: Codable {
    
    public var montoPago: Double!
    public var porcentajeMinimo: Int!
    public var porcentajeMaximo: Int!
    public var porcentajeAhorro: Int!
   
    public init(){}
    
    public init(montoPago:Double, porcentajeMinimo: Int, porcentajeMaximo: Int, porcentajeAhorro: Int) {
        self.montoPago = montoPago
        self.porcentajeMinimo = porcentajeMinimo
        self.porcentajeMaximo = porcentajeMaximo
        self.porcentajeAhorro = porcentajeAhorro
    }
    
}
