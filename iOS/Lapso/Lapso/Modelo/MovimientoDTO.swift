import Foundation

open class MovimientoDTO: Codable {

    public var idMovimiento: Int!
    public var referencia: String!
    public var idTransaccion: String!
    public var mensaje: String!
    public var estado: String!
    public var monto: Double!


    public init() { }

    public init(idMovimiento: Int, referencia: String, idTransaccion: String, mensaje: String, estado: String, monto: Double) {
        self.idMovimiento = idMovimiento
        self.referencia = referencia
        self.idTransaccion = idTransaccion
        self.mensaje = mensaje
        self.estado = estado
        self.monto = monto

    }

}

