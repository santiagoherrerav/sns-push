import Foundation

open class PorcentajeAhorro: Codable {
    
    public var texto : String!
    public var valor : Int!
    
    public init() {
    }
    
    public init(texto: String,  valor: Int) {
        self.texto = texto
        self.valor = valor
    }
    
}
