import Foundation

open class UsuarioDTO {
    
    public var id: String!
    public var token: String!
    
    
    public init(){}
    
    public init(id: String, token: String) {
        self.id = id
        self.token = token
    }
}
