import Foundation

open class AhorroEmpresaDTO: Codable {
    
    public var uuidEmpresa: String!
    public var nombreEmpresa: String!
    public var tienePorcentajeAhorro: Bool!

    public init(){}
    
    public init(uuidEmpresa: String,  nombreEmpresa: String, tienePorcentajeAhorro : Bool) {
        self.uuidEmpresa = uuidEmpresa
        self.nombreEmpresa = nombreEmpresa
        self.tienePorcentajeAhorro = tienePorcentajeAhorro
    }
    
}
