import Foundation

public enum FrecuenciaPago : String {
    case SEMANAL
    case QUINCENAL
    case MENSUAL
}
