import Foundation

public enum EstadoMovimiento: String {
    case PENDIENTE = "pending"
    case CONFIRMADO = "approved"
    case ACREDITADO = "accredited"
    case DENEGADO = "denied"
    case ERROR = "error"
}
