import Foundation

open class ResumenAhorradorDTO: Codable {
    
    public var id: String!
    public var totalMontoBloqueado: Int!
    public var totalMontoDisponible: Int!
    public var totalMontoMetas: Int!
    public var tieneDocumentoIdentidad: Bool!
    public var porcentajeAhorro: [AhorroEmpresaDTO]!

    
    public init(){}
    
    public init(id:String, totalMontoBloqueado: Int, totalMontoDisponible: Int, totalMontoMetas: Int, tieneDocumentoIdentidad: Bool, porcentajeAhorro: [AhorroEmpresaDTO]) {
        self.id = id
        self.totalMontoBloqueado = totalMontoBloqueado
        self.totalMontoDisponible = totalMontoDisponible
        self.totalMontoMetas = totalMontoMetas
        self.tieneDocumentoIdentidad = tieneDocumentoIdentidad
        self.porcentajeAhorro = porcentajeAhorro
    }
}
