import Foundation

public struct MetasDTO: Codable {
    
    public var metas: [MetaDTO]!
    public var metasSugeridas: [MetaDTO]!
    
    public init(){}
    
    public init(metas: [MetaDTO], metasSugeridas: [MetaDTO]) {
        self.metas = metas
        self.metasSugeridas = metasSugeridas
    }
}
