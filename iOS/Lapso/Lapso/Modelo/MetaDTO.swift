import Foundation

open class MetaDTO: Codable {
    
    public var uuid : String!
    public var nombre : String!
    public var fechaFinal : Date!
    public var monto : Int!
    public var montoProgramado : Int!
    public var idImagen : Int!
    public var bloqueada : Bool!
    public var ahorro : Int!
    
    public init() {
    }
    
    public init(uuid: String,  nombre: String,  fechaFinal: Date,  monto: Int,  montoProgramado: Int, idImagen: Int, bloqueada: Bool, ahorro: Int) {
        self.uuid = uuid
        self.nombre = nombre
        self.fechaFinal = fechaFinal
        self.monto = monto
        self.montoProgramado = montoProgramado
        self.idImagen = idImagen
        self.bloqueada = bloqueada
        self.ahorro = ahorro
    }
    
}
