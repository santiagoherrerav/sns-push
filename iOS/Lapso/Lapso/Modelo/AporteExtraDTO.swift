import Foundation


open class AporteExtraDTO: Codable {
    
    public var idUsuario: String!
    public var uuidMeta: String!
    public var monto: Double!
    public var destinoAporte: String!
    
    public init(){}
    
    public init(uuidMeta: String, monto: Double, destinoAporte: String) {
        self.uuidMeta = uuidMeta
        self.monto = monto
        self.destinoAporte = destinoAporte
    }
    
    public init(idUsuario: String,uuidMeta: String, monto: Double, destinoAporte: String) {
        self.idUsuario = idUsuario
        self.uuidMeta = uuidMeta
        self.monto = monto
        self.destinoAporte = destinoAporte
    }
    
}
