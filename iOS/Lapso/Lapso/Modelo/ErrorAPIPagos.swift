import Foundation

public enum ErrorAPIPagos: String {
    case T001 = "Branch invalido"
    case T002 = "Usuario invalido"
    case T003 = "Contraseña invalida"
    case T004 = "Tarjeta invalida"
    case T005 = "País invalido"
    case T006 = "Merchant invalido"
    case T007 = "Referencia invalida"
    case T008 = "Tipo de operación invalida"
    case T009 = "Token invalido"
    case T010 = "Monto invalido"
    case T011 = "Moneda invalida"
    case T012 = "Parámetros inválidos"
    case T013 = "La tarjeta proporcionada no corresponde con el token"
    case T014 = "Compañía invalida"
}
