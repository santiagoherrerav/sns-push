import Foundation

public enum EstadoCuenta: Int, Codable {
    case DESCONOCIDO = -1
    case PENDIENTE_VALIDACION = 0
    case VALIDA = 1
    case INVALIDA = 2
}
