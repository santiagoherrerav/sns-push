import Foundation

public enum DestinoAporte: String {
    case DISPONIBLE
    case BLOQUEADO
    case META
}
