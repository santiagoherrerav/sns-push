import Foundation

open class PaisDTO {
    
    public var id: String!
    public var nombre: String!
    public var indicativo: String!
    public var sigla: String!
    
    public init(){}
    
    public init(id: String, nombre: String, indicativo: String, sigla: String) {
        self.id = id
        self.nombre = nombre
        self.indicativo = indicativo
        self.sigla = sigla
    }
}
