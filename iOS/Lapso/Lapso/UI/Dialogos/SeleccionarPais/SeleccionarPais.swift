import UIKit
import Foundation
import PhoneNumberKit



class SeleccionarPais: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    @IBOutlet weak var tableView: UITableView!
    
    @IBOutlet weak var buscarPais: UITextField!
    
    var delegate: SeleccionarPaisDelegate?
    
    var paises: [PaisDTO] = []
    
    var paisesAMostrar: [PaisDTO] = []
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return self.paisesAMostrar.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = Bundle.main.loadNibNamed("PaisTableViewCell", owner: self, options: nil)?.first as! PaisTableViewCell

        if let nombre = self.paisesAMostrar[indexPath.row].nombre {
            cell.labelNombrePais?.text = nombre
        }
        
        if let indicativo = self.paisesAMostrar[indexPath.row].indicativo {
           cell.labelIndicativoPais?.text = "(\(indicativo))"
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        self.delegate?.dismissDialogPais(pais : self.paisesAMostrar[indexPath.row])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        buscarPais.rightViewMode = UITextFieldViewMode.always
        let imageView = UIImageView(frame: CGRect(x: 0, y: 0, width: 20, height: 20))
        let image = UIImage(named: "Image-6");
        imageView.image = image;
        buscarPais.rightView = imageView;
      
        self.tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
        cargarPaises()
        paisesAMostrar = paises.filter() {_ in
            if buscarPais.text == "" {
                return true
            } else {
                return false
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func buscarPais(_ sender: Any) {
        paisesAMostrar = paises.filter() {(pais : PaisDTO) -> Bool in
            if buscarPais.text == "" {
                return true
            } else if pais.nombre.containsIgnoringCase(find: buscarPais.text!)  {
                return true
            } else {
                return false
            }
        }
        tableView.reloadData()
    }
    
    func cargarPaises(){
        let phoneNumberKit = PhoneNumberKit()
        for code in NSLocale.isoCountryCodes {
            let countryCode: UInt64? = phoneNumberKit.countryCode(for: code)
            
            if let newValue = countryCode {
                let country = PaisDTO(id: UUID().uuidString,  nombre: countryName(countryCode: code)!, indicativo: "+\(newValue)", sigla: code)
                paises.append(country)
            }
            
        }
    }
    
    func countryName(countryCode: String) -> String? {
        let current = Locale(identifier: "es_CO")
        return current.localizedString(forRegionCode: countryCode) ?? nil
    }

}

extension String {
    func contains(find: String) -> Bool{
        return self.range(of: find) != nil
    }
    func containsIgnoringCase(find: String) -> Bool{
        return self.range(of: find, options: .caseInsensitive) != nil
    }
}
