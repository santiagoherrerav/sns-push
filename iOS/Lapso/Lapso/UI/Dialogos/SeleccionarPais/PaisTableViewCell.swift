import UIKit

class PaisTableViewCell: UITableViewCell {

    
    @IBOutlet weak var labelNombrePais: UILabel!
    @IBOutlet weak var labelIndicativoPais: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        let selView = UIView()
        
        selView.backgroundColor = UIColor(red: 22/255, green: 27/255, blue: 105/255, alpha: 1.0)
        self.selectedBackgroundView = selView
        self.labelNombrePais.highlightedTextColor = UIColor.white
        self.labelIndicativoPais.highlightedTextColor = UIColor.white
        
    }
    
}
