import Foundation

class Icons {

    open func getIconById(id: Int) -> String {
        switch id {
        case 1: return "METAS_CARD_1"
        case 2: return "METAS_CARD_2"
        case 3: return "METAS_CARD_3"
        case 4: return "METAS_CARD_4"
        case 5: return "METAS_CARD_5"
        case 6: return "METAS_CARD_6"
        case 7: return "METAS_CARD_7"
        case 8: return "METAS_CARD_8"
        case 9: return "METAS_CARD_9"
        case 10: return "METAS_CARD_10"
        case 11: return "METAS_CARD_11"
        case 12: return "METAS_CARD_12"
        case 13: return "METAS_CARD_13"
        case 14: return "METAS_CARD_14"
        case 15: return "METAS_CARD_15"
        default:
            return ""
        }
    }

}
