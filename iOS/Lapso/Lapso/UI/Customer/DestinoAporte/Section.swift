import Foundation

open class Section {
    var title: String!
    var childs: [SectionChilds]!
    var expanded: Bool!
    var icon : String!

    public init(title: String, childs: [SectionChilds], expanded: Bool, icon: String) {
        self.title = title
        self.childs = childs
        self.expanded = expanded
        self.icon = icon
    }
}
