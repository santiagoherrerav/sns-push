import UIKit

class MetasTableViewCell: UITableViewCell {
    
    @IBOutlet weak var label: UILabel!
    @IBOutlet weak var iconView: UIImageView?
    @IBOutlet weak var viewMeta: UIView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        viewMeta.backgroundColor = UIColor(hex: 0xC2C8D2)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        let selView = UIView()
        selView.backgroundColor = UIColor(red: 22/255, green: 27/255, blue: 105/255, alpha: 1.0)
        self.selectedBackgroundView = selView
        self.label.highlightedTextColor = UIColor.white
    }
    
}
