import UIKit

protocol ExpandableHeaderViewDelegate {
    func toggleSection(header: ExpandableHeaderView, section: Int)
}

class ExpandableHeaderView: UITableViewHeaderFooterView {
    var delegate: ExpandableHeaderViewDelegate?
    var section: Int!


    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var viewHeader: UIView!

    override init(reuseIdentifier: String?) {
        super.init(reuseIdentifier: reuseIdentifier)
        addGesture()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        addGesture()
    }

    private func addGesture() {
        let tap = UILongPressGestureRecognizer(target: self, action: #selector(selectHeaderAction))
        tap.minimumPressDuration = 0
        self.addGestureRecognizer(tap)
    }

    @objc func selectHeaderAction(gestureRecognizer: UITapGestureRecognizer) {
        let cell = gestureRecognizer.view as! ExpandableHeaderView

        if gestureRecognizer.state == .began {
            self.viewHeader.backgroundColor = UIColor(hex: 0x161B69)
            self.title.textColor = UIColor.white

        } else if gestureRecognizer.state == .ended {
            self.viewHeader.backgroundColor = UIColor.white
            self.title.textColor = UIColor.black
            delegate?.toggleSection(header: self, section: cell.section)
        }
    }

    func customInit(title: String, section: Int, delegate: ExpandableHeaderViewDelegate) {
        self.title.text = title
        self.section = section
        self.delegate = delegate
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        self.textLabel?.textColor = UIColor.white
        self.contentView.backgroundColor = UIColor.darkGray
    }

}

