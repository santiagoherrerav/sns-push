import Foundation

open class SectionChilds {
    var id: String!
    var title: String!
    var icon : String!

    public init(id: String, title: String, icon: String) {
        self.id = id
        self.title = title
        self.icon = icon
    }
}

