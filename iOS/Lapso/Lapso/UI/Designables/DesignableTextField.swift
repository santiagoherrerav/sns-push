import UIKit

enum TextFieldValidation {
    case Valid, Invalid, Unknown
}

@IBDesignable class DesignableTextField: UITextField {
    
    @IBOutlet var nextTextField: UITextField?
    
    @IBInspectable var leftPadding: CGFloat = 0 {
        didSet {
            let padding = UIView(frame: CGRect(x:0, y:0, width: leftPadding, height: 0))
            
            leftViewMode = UITextFieldViewMode.always
            leftView = padding
        }
    }
    
    @IBInspectable var rightPadding: CGFloat = 0 {
        didSet {
            let padding = UIView(frame: CGRect(x: 0, y: 0, width: 0, height: rightPadding))
            
            rightViewMode = UITextFieldViewMode.always
            rightView = padding
        }
    }
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var validColor: UIColor = UIColor(red:0.25, green:0.68, blue:0.35, alpha:1)
    
    @IBInspectable var invalidColor: UIColor = UIColor(red:0.73, green:0.24, blue:0.17, alpha:1)
    
    @IBInspectable var defaultColor: UIColor = UIColor(red:0.59, green:0.65, blue:0.71, alpha:1)
    
    var validated: TextFieldValidation = .Unknown {
        didSet {
            switch (validated) {
            case .Valid:
                self.borderColor = validColor
            case .Invalid:
                self.borderColor = invalidColor
            case .Unknown:
                self.borderColor = defaultColor
            }
        }
    }
    
}



private var __maxLengths = [UITextField: Int]()
extension UITextField {
    @IBInspectable var maxLength: Int {
        get {
            guard let l = __maxLengths[self] else {
                return 150 // (global default-limit. or just, Int.max)
            }
            return l
        }
        set {
            __maxLengths[self] = newValue
            addTarget(self, action: #selector(fix), for: .editingChanged)
        }
    }
    @objc func fix(textField: UITextField) {
        let t = textField.text
        textField.text = t?.safelyLimitedTo(length: maxLength)
    }
}

extension UITextField {

}


extension String
{
    func safelyLimitedTo(length n: Int)->String {
        let c = self.characters
        if (c.count <= n) { return self }
        return String( Array(c).prefix(upTo: n) )
    }
}

