import UIKit

@IBDesignable class DesignableButtonPrimary: UIButton {
    
    
    @IBInspectable var borderColor: UIColor = UIColor.clear {
        didSet {
            layer.borderColor = borderColor.cgColor
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var cornerRadius: CGFloat = 0 {
        didSet {
            layer.cornerRadius = cornerRadius
        }
    }
    
    @IBInspectable var shadowColor: UIColor = UIColor.clear {
        didSet {
            layer.shadowColor = shadowColor.cgColor
        }
    }
    
    @IBInspectable var shadowRadius: CGFloat = 0 {
        didSet {
            layer.shadowRadius = shadowRadius
        }
    }
    
    @IBInspectable var shadowOpacity: CGFloat = 0 {
        didSet {
            layer.shadowOpacity = Float(shadowOpacity)
        }
    }
    
    @IBInspectable var shadowOffsetY: CGFloat = 0 {
        didSet {
            layer.shadowOffset.height = shadowOffsetY
        }
    }
    
    @IBInspectable var backgroundNormal: UIColor = UIColor.clear;
    
    @IBInspectable var backgroundPressed: UIColor = UIColor.clear;
    
    @IBInspectable var titleColorNormal: UIColor = UIColor.clear;
    
    @IBInspectable var titleColorPressed: UIColor = UIColor.clear;
    
    override var isEnabled: Bool {
        didSet {
            if isEnabled {
                self.alpha = 1.0
            } else {
                self.alpha = 0.5
            }
        }
    }
    
    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                estaPresionado()
            } else {
                noEstaPresionado()
            }
        }
    }
    
    override var isSelected: Bool {
        didSet {
            if isSelected {
                estaPresionado()
            } else {
                noEstaPresionado()
            }
        }
    }
    
    private func estaPresionado() {
        self.borderWidth = 0
        self.titleLabel?.textColor = self.titleColorPressed
        self.backgroundColor = self.backgroundPressed
        self.layoutIfNeeded()
    }
    
    private func noEstaPresionado() {
        self.borderWidth = 2
        self.titleLabel?.textColor = self.titleColorNormal
        self.backgroundColor = self.backgroundNormal
        self.layoutIfNeeded()
    }
}
