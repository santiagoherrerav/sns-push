import UIKit

class GestionAhorroDisponibleViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func irAtras(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PanelControl", bundle: nil)
        let panelViewController = storyboard.instantiateViewController(withIdentifier: "PanelContainerViewController")
        self.show(panelViewController, sender: nil)
    }
}
