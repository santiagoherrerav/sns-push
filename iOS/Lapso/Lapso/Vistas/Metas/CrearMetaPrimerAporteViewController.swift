import UIKit
import MaterialTextField


import CeibaMobileCore

class CrearMetaPrimerAporteViewController: BaseController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var pikerView: UIPickerView!
    @IBOutlet weak var switchAporteMensual: UISwitch!
    @IBOutlet weak var lblConseguiras: UILabel!
    var montos = [String]()
    
    @IBAction func proximaPagina(_ sender: Any) {
        let parentViewController = self.parent as! MetasUIPageViewController
        parentViewController.goToNextPage()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.cargarMontos()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return montos[row]
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return montos.count
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        
        
    }

    func cargarMontos(){
        let minimoMeta = 100
        let montoMeta = 500
        let rango = 10
        
        
        let media = (minimoMeta + montoMeta) / 2
        
        for item in stride(from: minimoMeta, to: montoMeta + rango, by: rango) {
            self.montos.append("$\(item)")
        }
        
        self.pikerView.selectRow(montos.index(of: "$\(media)")!, inComponent: 0, animated: true)
    }
}
