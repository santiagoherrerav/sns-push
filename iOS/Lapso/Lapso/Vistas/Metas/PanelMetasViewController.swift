import UIKit
import CeibaMobileCore

class PanelMetasViewController: BaseController, UITableViewDelegate, UITableViewDataSource {

    @IBOutlet weak var tableViewMetas: UITableView!
    
    var alert = UIAlertController(title: "Por favor espera...", message: "", preferredStyle: .alert)
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    let cellReuseIdentifier = "metaListaTableViewCell"
    var metasDominio: MetasDominio!
    var metasUsuario = [MetaDTO]()
    let date = Date()
    let dateFormatter = DateFormatter()
    var icons = Icons()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.inicializarDominio()
        self.hideKeyboardWhenTappedAround()
        self.customTableView()
        self.consultarMetas()
        tableViewMetas.delegate = self
        tableViewMetas.dataSource = self
    }
    
    func inicializarDominio() {
        let repository = appDelegate.container.resolve(MetasRepositorioProtocolo.self)!
        do {
            try metasDominio = MetasDominio(metasRepositorio: repository)
        } catch {
            showGenericDialog(title: "Error", message: "Ocurrió un error iniciando la app", closure: { exit(0) })
        }
    }
    
    private func consultarMetas() {
        
        executeEvent(event: createAsyncEvent(closure: {
            try self.metasDominio.consultarMetas(onFinish: self.obtenerMetasUsuario, onError: self.errorConsultarMetas)
        }))
    }
    
    private func obtenerMetasUsuario(metasDTO: [MetaDTO]) {
        self.metasUsuario = metasDTO
        cargarMetas()
    }
    
    private func cargarMetas() {
        tableViewMetas.reloadData()
        self.alert.dismiss(animated: true, completion: nil)
    }
    
    
    @IBAction func irAtras(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PanelControl", bundle: nil)
        
        let panelViewController = storyboard.instantiateViewController(withIdentifier: "PanelContainerViewController")
        self.show(panelViewController, sender: nil)
    }
    
    func customTableView(){
        self.tableViewMetas.contentInset = UIEdgeInsetsMake(-1, -10, -10, -10);
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    
    func numberOfSections(in tableView: UITableView) -> Int {
        return self.metasUsuario.count
    }
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 1
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat{
        return 150.0;
    }
    
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 5
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell = Bundle.main.loadNibNamed("ItemMetasCell", owner: self, options: nil)?.first as! ItemMetasCell
        
        
        let meta = metasUsuario[indexPath.section]
        
        let format = DateFormatter()
        format.dateFormat = "yyyy-MM-dd HH:mm:ss ZZZ"
        let progresoAhorro: Float = Float(calclularProgresoDeAhorro(meta: meta))
        
        cell.nombreMeta.text = meta.nombre
        cell.fechaFinal.text = "Finaliza el \(getDia(date: meta.fechaFinal)) de \(getMes(date: meta.fechaFinal))  de \(getAnio(date: meta.fechaFinal))"
        cell.monto.text = "$ \(meta.monto.formattedWithSeparator)"
        if(meta.ahorro != nil){
            cell.ahorro.text = "$\(meta.ahorro.formattedWithSeparator)"
        }else{
            cell.ahorro.text = "$0"
        }
        
        if(progresoAhorro == 1){
            cell.fechaFinal.text = "¡Meta completada!"
            cell.progressViewAhorro.gradientColors = [#colorLiteral(red: 0.8549019694, green: 0.250980407, blue: 0.4784313738, alpha: 1).cgColor, #colorLiteral(red: 0.9098039269, green: 0.4784313738, blue: 0.6431372762, alpha: 1).cgColor, #colorLiteral(red: 0.9411764741, green: 0.4980392158, blue: 0.3529411852, alpha: 1).cgColor, #colorLiteral(red: 0.9686274529, green: 0.78039217, blue: 0.3450980484, alpha: 1).cgColor]
        }
        cell.progressViewAhorro.progress = progresoAhorro
        cell.imageViewMeta?.image = UIImage(named: icons.getIconById(id: meta.idImagen))
        return cell
    }
    
    func calclularProgresoDeAhorro(meta: MetaDTO?) -> Double{
        if((meta?.ahorro) != nil){
            return Double((meta?.ahorro)!) / Double((meta?.monto)!)
        }else{
            return 0
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    func getDia(date: Date) -> String {
        dateFormatter.dateFormat = "dd"
        let currentDateString: String = dateFormatter.string(from: date)
        return currentDateString
    }
    
    func getMes(date: Date) -> String {
        dateFormatter.dateFormat = "MMMM"
        let currentDateString: String = dateFormatter.string(from: date)
        return currentDateString
    }
    
    func getAnio(date: Date) -> String {
        dateFormatter.dateFormat = "yyyy"
        let currentDateString: String = dateFormatter.string(from: date)
        return currentDateString
    }
    
    private func errorConsultarMetas(error: Error) {
        self.alert.dismiss(animated: true, completion: {
            self.processError(error: error)
        })
    }
}

extension Formatter {
    static let withSeparator: NumberFormatter = {
        let formatter = NumberFormatter()
        formatter.groupingSeparator = "."
        formatter.numberStyle = .decimal
        return formatter
    }()
}

extension BinaryInteger {
    var formattedWithSeparator: String {
        return Formatter.withSeparator.string(for: self) ?? ""
    }
}
