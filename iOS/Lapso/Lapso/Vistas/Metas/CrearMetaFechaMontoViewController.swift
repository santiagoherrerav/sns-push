import UIKit
import CeibaMobileCore


class CrearMetaFechaMontoViewController: BaseController {

    @IBOutlet weak var datePicker: UIDatePicker!
    @IBOutlet weak var lblMontoAportar: UILabel!
    @IBOutlet weak var lblFrecuenciaPago: UILabel!

    @IBOutlet weak var txtDineroAportar: DesignableTextField!
    @IBOutlet weak var btnContinuar: DesignableButtonPrimary!

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var metasDominio: MetasDominio!
    var sesionDomino: SesionUsuarioDominio!
    var metasUsuario2 = [MetaDTO]()
    var resp: String = ""
    var fechaFinal: Date!
    var frecuenciaPago: FrecuenciaPago!
    
     var alert = UIAlertController(title: "Por favor espera...", message: "", preferredStyle: .alert)


    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        btnContinuar.isEnabled = false
        self.inicializarDominio()
        self.present(alert, animated: true, completion: nil)
        self.consultarFrecuenciaPago()
    }

    func inicializarDominio() {
        let repository = appDelegate.container.resolve(MetasRepositorioProtocolo.self)!
        let repositorySesion = appDelegate.container.resolve(SesionUsuarioRepositorioProtocolo.self)!
        do {
            try metasDominio = MetasDominio(metasRepositorio: repository)
            try sesionDomino = SesionUsuarioDominio(sesionUsuarioRepositorio: repositorySesion)
        } catch {
            showGenericDialog(title: "Error", message: "Ocurrió un error iniciando la app", closure: { exit(0) })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func proximaPagina(_ sender: Any) {
        if fechaFinal != nil {
            self.present(alert, animated: true, completion: nil)
            let parentViewController = self.parent as! MetasUIPageViewController
            let metaDTO = parentViewController.getMetaDTO()
            metaDTO.fechaFinal = fechaFinal
            metaDTO.monto = Int(txtDineroAportar.text!)
            parentViewController.setMetaDTO(metaDTO: metaDTO)
            self.crearMetaAporte(metaDTO: metaDTO)
        }
    }

    @IBAction func textChanged(_ sender: UITextField) {
        if sender == txtDineroAportar {
            btnContinuar.isEnabled = false

            if sender.hasText {
                if(txtDineroAportar.text!.count >= 3) {
                    if let aporte = Int(txtDineroAportar.text!) {
                        if(aporte > 100) {
                            btnContinuar.isEnabled = true
                        }
                    }
                }
            }
        }
    }

    func cargarFechaMininaMetaPicker() {
        let fechaActual = metasDominio.calcularFechaMininaMeta(fechaActual: Date(), frecuenciaPago: frecuenciaPago)

        let gregorian = Calendar(identifier: .gregorian)
        let components = gregorian.dateComponents([.year, .month, .day, .hour, .minute, .second], from: fechaActual)

        let fechaSiguiente = gregorian.date(from: components)!
        datePicker.minimumDate = fechaSiguiente
    }

    @IBAction func datePickerValueChanged(_ sender: UIDatePicker) {
        if sender.date.isAfter(date: Date(), granularity: .day) {
            if let montoObjetivoString = txtDineroAportar.text {
                if let montoObjetivo = Int(montoObjetivoString) {
                    let aporte = metasDominio.simularAportePeriodico(fechaActual: Date(), fechaFinal: sender.date, montoObjetivo: montoObjetivo, frecuenciaPago: frecuenciaPago)
                    fechaFinal = sender.date
                    print(fechaFinal)
                    lblMontoAportar.text = String(format: "%.2f", aporte)
                }
            }
        }
    }

    func crearMetaAporte(metaDTO: MetaDTO) {
        executeEvent(event: createAsyncEvent(closure: {
            try self.metasDominio.crearMeta(meta: metaDTO, onFinish: self.obtenerRespuestaCrearMeta, onError: self.error)
        }))
    }
    
    private func obtenerRespuestaCrearMeta() {
        self.alert.dismiss(animated: true, completion: nil)
        let parentViewController = self.parent as! MetasUIPageViewController
        parentViewController.showMetaCreadaPopup()
    }

    private func error(error: Error) {
        //se finaliza progressdialog
        self.alert.dismiss(animated: true, completion: nil)
        processError(error: error)
    }

    func consultarFrecuenciaPago() {
        
        let idEmpresa = self.sesionDomino.consultarIdEmpresa()
        executeEvent(event: createAsyncEvent(closure: {
            try self.metasDominio.consultarFrecuenciaPago(idEmpresa: idEmpresa!, onFinish: self.obtenerFrecuencia, onError: self.error)
        }))

    }

    private func obtenerFrecuencia(frecuencia: FrecuenciaPago) {
        frecuenciaPago = frecuencia
        lblFrecuenciaPago.text = frecuencia.rawValue
        cargarFechaMininaMetaPicker()
        self.alert.dismiss(animated: true, completion: nil)
    }

}
