import UIKit
import CeibaMobileCore



class CrearMetaNombreViewController: BaseController {

    @IBOutlet weak var txtNombreMeta: DesignableTextField!
    @IBOutlet weak var lblError: UILabel!
    @IBOutlet weak var btnContinuar: DesignableButtonPrimary!
    @IBOutlet weak var viewError: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.viewError.isHidden = true
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @IBAction func proximaPagina(_ sender: Any) {
        if estaValidoNombreMeta(nombreMeta: txtNombreMeta.text!) {
            let parentViewController = self.parent as! MetasUIPageViewController
            let metaDTO = MetaDTO()
            metaDTO.nombre = txtNombreMeta.text!
            parentViewController.setMetaDTO(metaDTO: metaDTO)
            parentViewController.goToNextPage()
        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.viewError.isHidden = false
            }, completion: nil)
            cambiarColorTextField(color: 0xE62C17)
        }
    }
    
    @IBAction func textChanged(_ sender: UITextField) {
        if(sender == txtNombreMeta){
            UIView.animate(withDuration: 0.5, animations: {
                self.viewError.isHidden = true
            }, completion: nil)
            cambiarColorTextField(color: 0xC3C8D1)
            btnContinuar.isEnabled = false
            if sender.hasText {
                btnContinuar.isEnabled = true
            }
        }
    }
    
    func estaValidoNombreMeta( nombreMeta : String) -> Bool{
        if(nombreMeta.count > 2 && nombreMeta.count < 100 ){
            if yaExisteNombreMeta(nombreMeta: nombreMeta) {
                lblError.text = "Ups, ya tiene una meta con ese nombre"
                return false
            }else{
                return true
            }
        }else{
            lblError.text = "Ups, el nombre debe estar entre 2 y 100 caracteres"
            return false
        }
    }
    
    func yaExisteNombreMeta(nombreMeta : String) -> Bool{
        return false
    }
    
    func cambiarColorTextField( color : Int){
        txtNombreMeta.textColor = UIColor(hex: color)
        txtNombreMeta.layer.borderColor = UIColor(hex: color).cgColor
        txtNombreMeta.text = txtNombreMeta.text
    }
}
