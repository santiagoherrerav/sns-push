import UIKit
import CeibaMobileCore

class MetasViewController: BaseController {

    @IBOutlet weak var labelProgress: UILabel!
    var currentPage = 0;
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.nextPageWithIndex(currentPage: currentPage)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func irAtras(_ sender: Any) {
        if(self.currentPage > 0){
            let vc =  self.childViewControllers.last as! MetasUIPageViewController;
            vc.goToPreviousPage()
        } else  {
            performSegue(withIdentifier: "PanelMetas", sender: nil)
        }
    }
    
    func nextPageWithIndex(currentPage: Int){
        let i = currentPage + 1
        let max = 2
        
        if i <= max {
            labelProgress.text = "\(i)/\(max)"
            self.currentPage = currentPage
        }
    }
    
    func showMetaCreadaPopup() {
        let popOverVC = UIStoryboard(name: "Metas", bundle: nil).instantiateViewController(withIdentifier: "MetaCreadaPopupViewController") as! MetaCreadaPopupViewController
        self.addChildViewController(popOverVC)
        
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
        
        
        Timer.scheduledTimer(timeInterval: 3, target: BlockOperation(block: {
            popOverVC.closePopUp()
            self.performSegue(withIdentifier: "PanelControl", sender: nil)
        }), selector: #selector(Operation.main), userInfo: nil, repeats: false)
    }
}
