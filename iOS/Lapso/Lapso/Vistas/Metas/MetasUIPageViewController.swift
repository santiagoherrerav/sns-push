import UIKit



class MetasUIPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {
    
    var pageControl = UIPageControl()
    var currentViewController: UIViewController!
    var metaDTO: MetaDTO? = nil


    lazy var orderedViewControllers: [UIViewController] = {
        return [self.newVc(viewController: "crearMetaNombre"),
        self.newVc(viewController: "crearMetaFechaMonto"),
        self.newVc(viewController: "crearMetaPrimerAporte")]
    }()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.dataSource = nil
        self.delegate = self
      
        if let firstViewController = orderedViewControllers.first {
            setViewControllers([firstViewController],
                               direction: .forward,
                               animated: true,
                               completion: nil)
        }
    }
    
    required init?(coder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    
    func newVc(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Metas", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = orderedViewControllers.index(of: pageContentViewController)!
        self.nextPage(currentPage: orderedViewControllers.index(of: pageContentViewController)!)
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard orderedViewControllers.count > previousIndex else {
            return nil
        }
        
        self.currentViewController = pageViewController.viewControllers!.first!
        
        return orderedViewControllers[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count
        
        guard orderedViewControllersCount != nextIndex else {
            return nil
        }
        
        guard orderedViewControllersCount > nextIndex else {
            return nil
        }
        
        self.currentViewController = pageViewController.viewControllers!.first!
        
        return orderedViewControllers[nextIndex]
    }
    
    
    func nextPage(currentPage: Int) {
        let parentViewController = self.parent as! MetasViewController
        parentViewController.nextPageWithIndex(currentPage: currentPage)
    }
    
    func goToNextPage() {
        
        guard let currentViewController = self.viewControllers?.first else { return }
        
        guard let nextViewController = self.pageViewController(self, viewControllerAfter: currentViewController) else { return }
        
        self.nextPage(currentPage: orderedViewControllers.index(of: nextViewController)!)
        
        setViewControllers([nextViewController], direction: .forward, animated: false, completion: nil)
        
    }
    
    func goToPreviousPage() {
        
        guard let currentViewController = self.viewControllers?.first else { return }
        
        guard let previousViewController = self.pageViewController(self, viewControllerBefore: currentViewController) else { return }
        
        self.nextPage(currentPage: orderedViewControllers.index(of: previousViewController)!)
        
        setViewControllers([previousViewController], direction: .reverse, animated: false, completion: nil)
        
    }
    
    func setMetaDTO(metaDTO: MetaDTO) {
        self.metaDTO = metaDTO
    }
    
    func getMetaDTO() -> MetaDTO {
        return self.metaDTO!
    }
    
    func showMetaCreadaPopup() {
        let parentViewController = self.parent as! MetasViewController
        parentViewController.showMetaCreadaPopup()
    }
}
