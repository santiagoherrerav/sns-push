import UIKit

class MetaListaTableViewCell: UITableViewCell{
    
    @IBOutlet weak var nombreMeta: UILabel!
    @IBOutlet weak var fechaFinal: UILabel!
    @IBOutlet weak var ahorro: UILabel!
    @IBOutlet weak var monto: UILabel!
    @IBOutlet weak var imageViewMeta: UIImageView!
    @IBOutlet weak var progressViewAhorro: GradientProgressBar!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        nombreMeta.font = UIFont(name:"HelveticaNeue-Bold", size: 20.0)
        progressViewAhorro.transform = progressViewAhorro.transform.scaledBy(x: 1, y: 4)
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
