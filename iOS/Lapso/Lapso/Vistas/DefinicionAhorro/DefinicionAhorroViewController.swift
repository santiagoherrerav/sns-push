import UIKit

import CeibaMobileCore

import AWSCognitoIdentityProvider

class DefinicionAhorroViewController: BaseController, UIPickerViewDataSource, UIPickerViewDelegate {

    @IBOutlet weak var pikerView: UIPickerView!
    @IBOutlet weak var txtMensajeDefineTuAhorro: UILabel!
    @IBOutlet weak var txtTotalAporte: UILabel!

    var pool = AWSCognitoIdentityUserPool(forKey: AWSCognitoUserPoolsSignInProviderKey)
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var ahorroDominio: AhorroDominio!
    var sesionUsuarioDominio: SesionUsuarioDominio!
    var idEmpresa: String!
    var configuracionAhorroDTO: ConfiguracionAhorroDTO!
    var porcentajeAhorro: Int!
    var valorAhorro: Double!
    var alert = UIAlertController(title: "Por favor espera...", message: "", preferredStyle: .alert)
    
    var porcentajes = [PorcentajeAhorro]()

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.present(alert, animated: true, completion: nil)
        self.inicializarDominio()
        if sesionUsuarioDominio.consultarIdEmpresa() != nil {
            self.idEmpresa = sesionUsuarioDominio.consultarIdEmpresa()
            self.consultarConfiguracionAhorro()
        }
    }

    func inicializarDominio() {

        let ahorroRepositorio = appDelegate.container.resolve(AhorroRepositorioProtocolo.self)!
        let sesionUsuarioRepositorio = appDelegate.container.resolve(SesionUsuarioRepositorioProtocolo.self)!
        do {
            try ahorroDominio = AhorroDominio(ahorroRepositorio: ahorroRepositorio)
            try sesionUsuarioDominio = SesionUsuarioDominio(sesionUsuarioRepositorio: sesionUsuarioRepositorio)
        } catch {
            showGenericDialog(title: "Error", message: "Ocurrió un error iniciando la app", closure: {
                exit(0)
            })
        }
    }

    func numberOfComponents(in pickerView: UIPickerView) -> Int
    {
        return 1
    }

    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String?
    {
        return porcentajes[row].texto
    }

    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int
    {
        return porcentajes.count
    }

    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int)
    {
        if let montoPago = configuracionAhorroDTO.montoPago {
            if let porcentajeAhorro = porcentajes[row].valor {
                let valor = Double(montoPago) * Double(porcentajeAhorro) / 100
                self.porcentajeAhorro = porcentajeAhorro
                self.valorAhorro = valor
                self.txtTotalAporte.text = "Ahorrarás, de cada nómina $ \(valor)"
            }
        }
    }

    private func consultarConfiguracionAhorro() {

        executeEvent(event: createAsyncEvent(closure: {
            try self.ahorroDominio.consultarConfiguracionAhorro( idEmpresa: self.idEmpresa, onFinish: self.procesarConfiguracionAhorro, onError: { (error) in
                self.processError(error: error)
            })
        }))
    }


    private func procesarConfiguracionAhorro(configuracionAhorroDTO: ConfiguracionAhorroDTO) {
        self.configuracionAhorroDTO = configuracionAhorroDTO
        if let montoPago = configuracionAhorroDTO.montoPago {
            self.txtMensajeDefineTuAhorro.text = "Tu nómina es de $ \(montoPago) Selecciona qué porcentaje de cada nómina quieres ahorrar."

            if let porcentajeAhorro = self.configuracionAhorroDTO.porcentajeAhorro {
                let valor = Double(montoPago) * Double(porcentajeAhorro) / 100
                self.porcentajeAhorro = porcentajeAhorro
                self.valorAhorro = valor
                self.txtTotalAporte.text = "Ahorrarás, de cada nómina $ \(valor)"
            }
        }

        self.cargarPorcentajesAhorro()
    }

    func cargarPorcentajesAhorro() {
        let minimoPoncentaje = self.configuracionAhorroDTO.porcentajeMinimo
        let porcentajeMaximo = self.configuracionAhorroDTO.porcentajeMaximo
        //let porcentajeAhorro = self.configuracionAhorroDTO.porcentajeAhorro

        for index in stride(from: minimoPoncentaje!, to: porcentajeMaximo! + 1, by: 1) {
            let porcentaje = PorcentajeAhorro.init(texto: "\(index)%", valor: index)
            self.porcentajes.append(porcentaje)
        }

        self.alert.dismiss(animated: true, completion: {
             self.pikerView.reloadAllComponents()
        })
//        self.pikerView.selectRow(porcentajes.index(of: "\(porcentajeAhorro!)%")!, inComponent: 0, animated: true)
    }

    @IBAction func configurarPorcentajeAhorro(_ sender: Any) {
        if porcentajeAhorro != nil {
            self.present(alert, animated: true, completion: nil)
            self.configurarPorcentajeAhorro()
        }
    }

    func configurarPorcentajeAhorro() {
        
        executeEvent(event: createAsyncEvent(closure: {
            try self.ahorroDominio.configurarAhorro(idEmpresa: self.idEmpresa, porcentajeAhorro: self.porcentajeAhorro, onFinish: {
                self.alert.dismiss(animated: true, completion: {
                    self.performSegue(withIdentifier: "ComoFuncionaTuAhorro", sender: nil)
                })
            }, onError: { (error) in
                self.alert.dismiss(animated: true, completion: nil)
                self.processError(error: error)
            })
        }))
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? ComoFuncionaTuAhorroViewController {
            destinationViewController.valorAhorro = self.valorAhorro
        }
    }

}
