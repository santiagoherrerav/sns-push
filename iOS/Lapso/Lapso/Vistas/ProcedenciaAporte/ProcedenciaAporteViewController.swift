import UIKit

class ProcedenciaAporteViewController: UIViewController{
    
    @IBOutlet weak var viewCLABE: UIView!
    @IBOutlet weak var viewOtraCuenta: UIView!
    @IBOutlet weak var txtCLABE: UILabel!
    @IBOutlet weak var txtOtraCuenta: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.addBorders()
        self.addEvents()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    @objc func tapCLABE(sender:UITapGestureRecognizer){

        if sender.state == .began {
            viewCLABE.backgroundColor = UIColor(hex: 0x161B69)
            txtCLABE.textColor = UIColor.white
        } else if  sender.state == .ended {
            viewCLABE.backgroundColor = UIColor.white
            txtCLABE.textColor = UIColor.black
        }
    }
    
    @objc func tapOtro(sender:UITapGestureRecognizer){
        
        if sender.state == .began {
            viewOtraCuenta.backgroundColor = UIColor(hex: 0x161B69)
            txtOtraCuenta.textColor = UIColor.white

        } else if  sender.state == .ended {
            viewOtraCuenta.backgroundColor = UIColor.white
            txtOtraCuenta.textColor = UIColor.black
        }
    }
    
    private func addBorders(){
        viewCLABE.layer.borderWidth = 1
        viewCLABE.layer.borderColor = UIColor(hex: 0xF9FAFF).cgColor
        
        viewOtraCuenta.layer.borderWidth = 1
        viewOtraCuenta.layer.borderColor = UIColor(hex: 0xF9FAFF).cgColor
    }
    
    private func addEvents(){
        let tapGesture = UILongPressGestureRecognizer(target: self, action: #selector(self.tapCLABE(sender:)))
        tapGesture.minimumPressDuration = 0
        
        viewCLABE.addGestureRecognizer(tapGesture)
        
        let tapGestureOtro = UILongPressGestureRecognizer(target: self, action: #selector(self.tapOtro(sender:)))
        tapGestureOtro.minimumPressDuration = 0
        
        viewOtraCuenta.addGestureRecognizer(tapGestureOtro)
    }
    
}

