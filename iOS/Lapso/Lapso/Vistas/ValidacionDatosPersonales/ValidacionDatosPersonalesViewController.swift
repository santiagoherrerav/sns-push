import UIKit
import MaterialTextField


import CeibaMobileCore


class ValidacionDatosPersonalesViewController: BaseController, UITextFieldDelegate {

    @IBOutlet weak var btnConfirmar: DesignableButtonPrimary!
    @IBOutlet weak var nombre: MFTextField!
    @IBOutlet weak var apellidos: MFTextField!
    @IBOutlet weak var correo: UITextField!
    @IBOutlet weak var textoVerificaDatos: UILabel!

    @IBOutlet weak var emailText: DesignableTextField!

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var registroDominio: RegistroDominio!
    var sesionDomino: SesionUsuarioDominio!

    var alert = UIAlertController(title: "Por favor espera...", message: "", preferredStyle: .alert)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()

        btnConfirmar.isEnabled = false
    }

    override func viewDidAppear(_ animated: Bool) {
        self.present(alert, animated: true, completion: nil)
        self.inicializarDominio()
        self.consultarEmpleado()
    }

    func inicializarDominio() {
        let repository = appDelegate.container.resolve(RegistroRepositorioProtocolo.self)!
        let repositorySesion = appDelegate.container.resolve(SesionUsuarioRepositorioProtocolo.self)!

        do {
            try registroDominio = RegistroDominio(registroRepositorio: repository)
            try sesionDomino = SesionUsuarioDominio(sesionUsuarioRepositorio: repositorySesion)

        } catch {
            showGenericDialog(title: "Error", message: "Ocurrió un error iniciando la app", closure: { exit(0) })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func consultarEmpleado() {
        let parentViewController = self.parent as! RegistroUIPageViewController

        executeEvent(event: createAsyncEvent(closure: {
            try self.registroDominio.consultarInformacionUsuario(codigoPais: parentViewController.getDatosEmpleado().siglaPais, numeroCelular: parentViewController.getDatosEmpleado().celular, onFinish: self.datosEmpleado, onError: self.errorInvitacion)
        }))

    }

    func datosEmpleado(empleadoDTO: EmpleadoDTO) {
        self.alert.dismiss(animated: true, completion: {
            self.nombre.text = "\(empleadoDTO.primerNombre!) \(empleadoDTO.otrosNombres!)"
            self.apellidos.text = "\(empleadoDTO.primerApellido!) \(empleadoDTO.segundoApellido!)"
            self.textoVerificaDatos.text = "La empresa \(empleadoDTO.empresa.nombre!) nos brindó estos datos, Confírma si están correctos:"
            let parentViewController = self.parent as! RegistroUIPageViewController

            let datoEmplado = parentViewController.getDatosEmpleado()
            datoEmplado.idEmpleado = empleadoDTO.idEmpleado!
            datoEmplado.primerNombre = empleadoDTO.primerNombre!
            datoEmplado.otrosNombres = empleadoDTO.otrosNombres!
            datoEmplado.primerApellido = empleadoDTO.primerApellido!
            datoEmplado.segundoApellido = empleadoDTO.segundoApellido!
            datoEmplado.empresa = empleadoDTO.empresa!
            parentViewController.setDatosEmpleado(empleadoDTO: datoEmplado)
            self.sesionDomino.guardarIdEmpresa(idEmpresa: datoEmplado.empresa.uuid)
        })
    }

    func errorInvitacion(error: Error) {
        self.alert.dismiss(animated: true, completion: {
            self.processError(error: error)
        })
    }

    @IBAction func goToNextPage(_ sender: Any) {
        let providedEmailAddress = emailText.text

        let isEmailAddressValid = isValidEmailAddress(emailAddressString: providedEmailAddress!)

        if isEmailAddressValid
            {
            self.present(alert, animated: true, completion: nil)
            self.registrarUsuario()
        } else {
            displayAlertMessage(messageToDisplay: "Email address is not valid")
        }
    }

    func isValidEmailAddress(emailAddressString: String) -> Bool {

        var returnValue = true
        let emailRegEx = "[A-Z0-9a-z.-_]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,3}"

        do {
            let regex = try NSRegularExpression(pattern: emailRegEx)
            let nsString = emailAddressString as NSString
            let results = regex.matches(in: emailAddressString, range: NSRange(location: 0, length: nsString.length))

            if results.count == 0
                {
                returnValue = false
            }

        } catch let error as NSError {
            print("invalid regex: \(error.localizedDescription)")
            returnValue = false
        }

        return returnValue
    }

    func displayAlertMessage(messageToDisplay: String)
    {
        let alertController = UIAlertController(title: "Alert", message: messageToDisplay, preferredStyle: .alert)

        let OKAction = UIAlertAction(title: "OK", style: .default) { (action: UIAlertAction!) in
            print("Ok button tapped");
        }

        alertController.addAction(OKAction)

        self.present(alertController, animated: true, completion: nil)
    }

    func goToNextPage() {
        let parentViewController = self.parent as! RegistroUIPageViewController
        parentViewController.goToNextPage()
    }

    func registrarUsuario() {
        let parentViewController = self.parent as! RegistroUIPageViewController

        executeEvent(event: createAsyncEvent(closure: {
            try self.registroDominio.registrarUsuario(codigoPais: parentViewController.getDatosEmpleado().siglaPais, numeroCelular: parentViewController.getDatosEmpleado().celular, correo: self.correo.text!, clave: parentViewController.getDatosEmpleado().clave, onFinish: {
                    self.alert.dismiss(animated: true, completion: {
                        self.goToNextPage()
                    })
                }, onError: { error in
                    self.alert.dismiss(animated: true, completion: {
                        self.processError(error: error)
                    })
                })
        }))
    }

    @IBAction func aceptarTerminos(_ sender: Any) {
        btnConfirmar.isEnabled = !btnConfirmar.isEnabled
    }


    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.nextField?.becomeFirstResponder()
        return true
    }
}
