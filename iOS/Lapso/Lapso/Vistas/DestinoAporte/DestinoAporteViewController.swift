import UIKit
import MaterialTextField


import CeibaMobileCore

class DestinoAporteViewController: BaseController, UITableViewDelegate, UITableViewDataSource, ExpandableHeaderViewDelegate {

    @IBOutlet weak var tableView: UITableView!
    var alert = UIAlertController(title: "Por favor espera...", message: "", preferredStyle: .alert)

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var metasDominio: MetasDominio!
    var metasUsuario = [MetaDTO]()
    var metas = [String]()
    var uuidMeta: String = ""
    var icons = Icons()
    var section: Int!

    var sections = [
        Section(title: "Metas", childs: [], expanded: false, icon: ""),
        Section(title: "Disponible", childs: [], expanded: false, icon: ""),
        Section(title: "Bloqueado", childs: [], expanded: false, icon: "")
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.present(alert, animated: true, completion: nil)
        self.inicializarDominio()
        self.customTableView()
        self.consultarMetas()
    }


    func inicializarDominio() {
        let repository = appDelegate.container.resolve(MetasRepositorioProtocolo.self)!
        do {
            try metasDominio = MetasDominio(metasRepositorio: repository)
        } catch {
            showGenericDialog(title: "Error", message: "Ocurrió un error iniciando la app", closure: { exit(0) })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func numberOfSections(in tableView: UITableView) -> Int {
        return sections.count
    }

    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return sections[section].childs.count
    }

    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 44
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        if (sections[indexPath.section].expanded) {
            return 44
        } else {
            return 0
        }
    }

    func tableView(_ tableView: UITableView, heightForFooterInSection section: Int) -> CGFloat {
        return 2
    }

    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {

        let header = Bundle.main.loadNibNamed("ExpandableHeaderView", owner: self, options: nil)?.first as! ExpandableHeaderView

        header.customInit(title: sections[section].title, section: section, delegate: self)

        return header
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        let cell = Bundle.main.loadNibNamed("MetasTableViewCell", owner: self, options: nil)?.first as! MetasTableViewCell

        cell.label?.text = sections[indexPath.section].childs[indexPath.row].title
        cell.iconView?.image = UIImage(named: sections[indexPath.section].childs[indexPath.row].icon)
        return cell
    }

    func toggleSection(header: ExpandableHeaderView, section: Int) {
        self.section = section

        sections[section].expanded = !sections[section].expanded
        if sections[section].childs.count > 0 {

            tableView.beginUpdates()
            for i in 0 ..< sections[section].childs.count {
                tableView.reloadRows(at: [IndexPath(row: i, section: section)], with: .automatic)
            }
            tableView.endUpdates()
        } else if section != 0 {
            self.goToIngresarAporte()
        }
    }

    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(sections[indexPath.section].childs[indexPath.row].id)
        self.uuidMeta = sections[indexPath.section].childs[indexPath.row].id
        self.goToIngresarAporte()
    }

    private func goToIngresarAporte() {
        performSegue(withIdentifier: "IngresarAporteMeta", sender: nil)
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? IngresarAporteMetaViewController {
            destinationViewController.destinoAporteMeta = obtenerDestino()
            destinationViewController.uuidMeta = self.uuidMeta
        }
    }

    func obtenerDestino() -> DestinoAporte {
        switch self.section {
        case 0: return DestinoAporte.META
        case 1: return DestinoAporte.DISPONIBLE
        case 2: return DestinoAporte.BLOQUEADO
        default :
            return DestinoAporte.DISPONIBLE
        }
    }

    private func customTableView() {
        tableView.backgroundColor = UIColor.clear
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "cell")
    }

    private func consultarMetas() {

        executeEvent(event: createAsyncEvent(closure: {
            try self.metasDominio.consultarMetas(onFinish: self.obtenerMetasUsuario, onError: self.errorConsultarMetas)
        }))
    }


    private func obtenerMetasUsuario(metasDTO: [MetaDTO]) {
        self.metasUsuario = metasDTO
        cargarMetas()
    }

    private func cargarMetas() {
        for meta in self.metasUsuario {
            print(meta.nombre)
            let child = SectionChilds(id: meta.uuid, title: meta.nombre, icon: icons.getIconById(id: meta.idImagen))
            self.sections[0].childs.append(child)
        }
        tableView.reloadData()
        self.alert.dismiss(animated: true, completion: nil)
    }

    private func errorConsultarMetas(error: Error) {
        self.alert.dismiss(animated: true, completion: {
            self.processError(error: error)
        })
    }
    
    @IBAction func irAtras(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PanelControl", bundle: nil)
        let panelViewController = storyboard.instantiateViewController(withIdentifier: "PanelContainerViewController")
        self.show(panelViewController, sender: nil)
    }

}
