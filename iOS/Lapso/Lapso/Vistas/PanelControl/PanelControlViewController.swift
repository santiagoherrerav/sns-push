import UIKit
import LSDialogViewController
import CeibaMobileCore
import AWSCognitoIdentityProvider
import SlideMenuControllerSwift


class PanelControlViewController: BaseController {

    var alert = UIAlertController(title: "Por favor espera...", message: "", preferredStyle: .alert)
    @IBOutlet weak var lblTotalDinero: UILabel!
    @IBOutlet weak var lblTotalDisponible: UILabel!
    @IBOutlet weak var lblTotalBloqueado: UILabel!
    @IBOutlet weak var lblTotalMetas: UILabel!

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var ahorroDominio: AhorroDominio!
    var pool: AWSCognitoIdentityUserPool? = AWSCognitoIdentityUserPool.init(forKey: AWSCognitoUserPoolsSignInProviderKey)

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.inicializarDominio()
        self.consultarAhorrador()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.cerrarMenu()
    }

    private func inicializarDominio() {
        let ahorroRepositorio = appDelegate.container.resolve(AhorroRepositorioProtocolo.self)!
        do {
            try ahorroDominio = AhorroDominio(ahorroRepositorio: ahorroRepositorio)

        } catch {
            showGenericDialog(title: "Error", message: "Ocurrió un error iniciando la app", closure: {
                exit(0)
            })
        }
    }

    private func consultarAhorrador() {
        self.present(alert, animated: true, completion: nil)
        executeEvent(event: createAsyncEvent(closure: {
            try self.ahorroDominio.consultarAhorrador(onFinish: self.datosAhorrador, onError: self.manejarError)
        }))
    }

    private func datosAhorrador(resumenAhorrador: ResumenAhorradorDTO) {
        self.lblTotalDisponible.text = "\(resumenAhorrador.totalMontoDisponible!)"
        self.lblTotalBloqueado.text = "\(resumenAhorrador.totalMontoBloqueado!)"
        self.lblTotalMetas.text = "\(resumenAhorrador.totalMontoMetas!)"
        let valorTotal = resumenAhorrador.totalMontoDisponible! + resumenAhorrador.totalMontoMetas! + resumenAhorrador.totalMontoBloqueado!
        self.lblTotalDinero.text = "\(valorTotal)"

        self.alert.dismiss(animated: true, completion: nil)
    }

    func manejarError(error: Error) {
        self.alert.dismiss(animated: true, completion: {
            self.processError(error: error)
        })
    }
    
    @IBAction func abrirMenu(_ sender: Any) {
        if let slideMenuController = self.slideMenuController(){
            slideMenuController.openLeft()
        }
    }
    
    private func cerrarMenu(){
        if let slideMenuController = self.slideMenuController(){
            slideMenuController.closeLeft()
        }
    }
    
    @IBAction func gestionAhorroDisponible(_ sender: Any) {

        executeEvent(event: createAsyncEvent(closure: {
            try self.ahorroDominio.consultarCuentaCLABE(onFinish: {
                
            }, onError: {
                error in print(error)
            })
        }))
    }
    
}
