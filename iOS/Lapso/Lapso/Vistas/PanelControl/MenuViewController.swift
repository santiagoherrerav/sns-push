import UIKit
import CeibaMobileCore
import SlideMenuControllerSwift

class MenuViewController: BaseController {

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var sesionUsuarioDominio: SesionUsuarioDominio!

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func viewDidAppear(_ animated: Bool) {
        inicializarDominio()
    }
    private func inicializarDominio() {

        let sesionUsuarioRepositorio = appDelegate.container.resolve(SesionUsuarioRepositorioProtocolo.self)!

        do {
            try sesionUsuarioDominio = SesionUsuarioDominio(sesionUsuarioRepositorio: sesionUsuarioRepositorio)

        } catch {
            showGenericDialog(title: "Error", message: "Ocurrió un error iniciando la app", closure: {
                exit(0)
            })
        }
    }

    @IBAction func cerrarMenu(_ sender: Any) {
        if let slideMenuController = self.slideMenuController() {
            slideMenuController.closeLeft()
        }
    }

    @IBAction func cerrarSesion(_ sender: Any) {
        sesionUsuarioDominio.cerrarSesion(onFinish: {
            let storyboard = UIStoryboard(name: "IniciarSesion", bundle: nil)
            let panelViewController = storyboard.instantiateViewController(withIdentifier: "IniciarSesionViewController")
            self.show(panelViewController, sender: nil)
        })
    }

    
    @IBAction func irAServicioCliente(_ sender: Any) {
        let storyboard = UIStoryboard(name: "ServicioCliente", bundle: nil)
        
        let servicioClienteViewController = storyboard.instantiateViewController(withIdentifier: "ServicioClienteViewController")
        self.show(servicioClienteViewController, sender: sender)
    }
}
