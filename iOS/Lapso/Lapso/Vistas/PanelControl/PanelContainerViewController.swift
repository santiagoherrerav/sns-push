 import UIKit
import SlideMenuControllerSwift

class PanelContainerViewController: SlideMenuController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    override func awakeFromNib() {

        if let menuController = self.storyboard?.instantiateViewController(withIdentifier: "PanelControlViewController") {
            self.mainViewController = menuController

        }
        if let controller = self.storyboard?.instantiateViewController(withIdentifier: "MenuViewController") {
            self.leftViewController = controller
        }
        super.awakeFromNib()
    }
}
