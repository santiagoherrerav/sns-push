import UIKit
import CeibaMobileCore

import AWSCognitoIdentityProvider
import MaterialTextField

protocol ConfirmarAporteDelegate {
    func confirmarAporteExtra()
}

class IngresarAporteMetaViewController: BaseController, ConfirmarAporteDelegate, SuiteControllerDelegate {

    @IBOutlet weak var txtMonto: DesignableTextField!
    @IBOutlet weak var btnConfirmar: DesignableButtonPrimary!

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var aportesDominio: AportesDominio!
    var sesionDomino: SesionUsuarioDominio!
    var movimientoAporte: MovimientoDTO?
    var uuidMeta: String = ""
    var destinoAporteMeta =  DestinoAporte.META

    @IBOutlet weak var labelMonto: UILabel!
    @IBOutlet weak var txtTarjeta: MFTextField!

    let environment: Environment = EnvironmentSANDBOX

    let referenceSANDBOX = "REFERENCIA SNDBX001"
    var token: String? = nil

    var suiteController: SuiteController?
    var bean3DS: Bean3DS?
    var beanTokenization: BeanTokenization?


    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        self.inicializarDominio()

        btnConfirmar.isEnabled = false
        suiteController = getSuiteControllerOnVC()
        bean3DS = getBean3DS()
        beanTokenization = getBeanTokenization()

        if sesionDomino.consultarTokenPagos() != nil {
            token = sesionDomino.consultarTokenPagos()
            self.tokenRegex()
        }
    }

    private func tokenRegex() {
        let regex = try! NSRegularExpression(pattern: "\\d(?=\\d{4})")
        let modifiedString: String = regex.stringByReplacingMatches(in: token!, options: [], range: NSRange(location: 0, length: token!.count), withTemplate: "*")
        txtTarjeta.text = modifiedString
    }

    func inicializarDominio() {
        let repository = appDelegate.container.resolve(AportesRepositorioProtocolo.self)!
        let repositorySesion = appDelegate.container.resolve(SesionUsuarioRepositorioProtocolo.self)!
        do {
            try aportesDominio = AportesDominio(aportesRepositorio: repository)
            try sesionDomino = SesionUsuarioDominio(sesionUsuarioRepositorio: repositorySesion)
        } catch {
            showGenericDialog(title: "Error", message: "Ocurrió un error iniciando la app", closure: { exit(0) })
        }
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? ResumenAportePopUpViewController {
            destinationViewController.delegate = self
            if let monto = txtMonto.text {
                destinationViewController.monto = monto
            }
            destinationViewController.destino = self.destinoAporteMeta.rawValue
            destinationViewController.procedencia = token
        }
    }

    @IBAction func textChanged(_ sender: UITextField) {
        if sender == txtMonto {
            btnConfirmar.isEnabled = false
            if sender.hasText {
                if txtMonto.text!.count >= 1 {
                    if let aporte = Int(txtMonto.text!) {
                        if(aporte > 0) {
                            btnConfirmar.isEnabled = true
                        }
                    }
                }
            }
        }
    }

    @IBAction func confirmarAporte(_ sender: Any) {
        self.empezarAporte()
    }

    private func autenticarPago() {
        suiteController?.authenticate(with: beanTokenization, bean3DS: bean3DS)
    }

    func empezarAporte() {
        if token == nil {
            self.autenticarPago()
        } else {
            self.iniciarAporte()
        }
    }

    func iniciarAporte() {
        if let montoObjetivoString = txtMonto.text {
            if let montoObjetivo = Double(montoObjetivoString) {
                self.obtenerReferenciaAntesIniciarAporte(uuidMeta: uuidMeta, monto: montoObjetivo)
            }
        }
    }

    func payOneClick() {
        beanTokenization?.amount = txtMonto.text
        if let referenciaAporte = movimientoAporte?.referencia {
            bean3DS?.reference = "\(referenciaAporte)"
            beanTokenization?.reference = "\(referenciaAporte)"
        } else {
            bean3DS?.reference = referenceSANDBOX
            beanTokenization?.reference = referenceSANDBOX
        }

        beanTokenization?.token = token

        suiteController?.sndPayWithToken(with: beanTokenization, bean3DS: bean3DS)
    }

    func cambiarColorTextField(color: Int) {
        txtMonto.textColor = UIColor(hex: color)
        txtMonto.layer.borderColor = UIColor(hex: color).cgColor
    }

    func getSuiteControllerOnVC() -> SuiteController {
        return SuiteController.init(on:
                environment, currentViewController: self, delegate: self)
    }

    func getBeanTokenization() -> BeanTokenization {
        let beanTokenization = BeanTokenization()

        beanTokenization.branch = "0001"
        beanTokenization.company = "SX001"
        beanTokenization.country = "MEX"
        beanTokenization.user = "SNDBXUS3R"
        beanTokenization.password = "SNDBXP44S"
        beanTokenization.merchant = "123456"
        beanTokenization.currency = CurrencyMXN
        beanTokenization.operationType = "6"

        return beanTokenization
    }

    func getBean3DS() -> Bean3DS {
        let bean3DS = Bean3DS()

        bean3DS.branch = "0001"
        bean3DS.company = "SX001"
        bean3DS.country = "MEX"
        bean3DS.user = "SNDBXUS3R"
        bean3DS.password = "SNDBXP44S"
        bean3DS.merchant = "123456"
        bean3DS.currency = CurrencyMXN
        bean3DS.authKey = "516883575148515057485348"


        return bean3DS
    }

    func didFinishPayProcess(_ response: String!, error: SuiteError!) {

    }

    func didFinishAuthenticationProcess(_ tokenizeResponse: BeanTokenizeResponse!, error: SuiteError!) {

        if tokenizeResponse != nil {
            token = tokenizeResponse.token
            self.tokenRegex()
            sesionDomino.guardarTokenPagos(token: token!)
            self.iniciarAporte()
        }
        else {
            print("""
                Error
                Code: \(error.getCode())
                Desc: \(error.getDescription())
                """)
            showAlert(error.getDescription())
        }
    }

    func didFinishTokenizeTransantion(_ beanPaymentWithToken: BeanPaymentWithToken, error: SuiteError) {

        if beanPaymentWithToken.response != nil {
            movimientoAporte?.idTransaccion = beanPaymentWithToken.folio
            if let estadoAporte: String = beanPaymentWithToken.response {
                if estadoAporte == EstadoMovimiento.CONFIRMADO.rawValue{
                    movimientoAporte?.estado =  "\(EstadoMovimiento.ACREDITADO)"
                }else{
                    let estadoMovimiento = EstadoMovimiento(rawValue: estadoAporte)
                    movimientoAporte?.estado = "\(estadoMovimiento)"
                }
            }
            movimientoAporte?.mensaje = nil
            movimientoAporte?.monto =  Double(beanPaymentWithToken.amount) ?? 0.0
        }
        else {
            movimientoAporte?.estado = EstadoMovimiento.ERROR.rawValue
            movimientoAporte?.mensaje = EstadoMovimiento(rawValue: error.getCode())?.rawValue
        }
        self.actualizarAporte()
    }

    func operationCanceledByUser() {
        print("Operation Canceled By User")
    }

    func showAporteHechoPopup() {
        let popOverVC = UIStoryboard(name: "IngresarAporteMeta", bundle: nil).instantiateViewController(withIdentifier: "AporteHechoPopup") as! AporteHechoPopupViewController
        self.addChildViewController(popOverVC)

        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)


        Timer.scheduledTimer(timeInterval: 3, target: BlockOperation(block: {
            popOverVC.closePopUp()
            let storyboard = UIStoryboard(name: "PanelControl", bundle: nil)
            _ = storyboard.instantiateViewController(withIdentifier: "PanelContainerViewController")
        }), selector: #selector(Operation.main), userInfo: nil, repeats: false)
    }

    private func obtenerReferenciaAntesIniciarAporte(uuidMeta: String, monto: Double) {
        executeEvent(event: createAsyncEvent(closure: {
            try self.aportesDominio.iniciarAporteMeta(uuidMeta: uuidMeta, monto: monto, destinoAporteMeta: self.destinoAporteMeta.rawValue, onFinish: self.finalizoEventoInicioAporte, onError: self.errorCrearMeta)
        }))
    }

    private func actualizarAporte() {
        executeEvent(event: createAsyncEvent(closure: {
            try self.aportesDominio.actualizarAporte(movimientoDTO: self.movimientoAporte!, onFinish: {
                self.showAporteHechoPopup()
            }, onError: {
                error in self.processError(error: error)
            })
        }))
    }

    private func finalizoEventoInicioAporte(movimientoDTO: MovimientoDTO) {
        self.movimientoAporte = movimientoDTO
        performSegue(withIdentifier: "ResumenAportePopup", sender: nil)
    }

    private func errorCrearMeta(error: Error) {
        processError(error: error)
    }

    func confirmarAporteExtra() {
        self.payOneClick()
    }

    func showAlert(_ message: String) {
        let alert = UIAlertController(title: "Lapso", message: message, preferredStyle: .alert)
        let okAction = UIAlertAction(title: "OK", style: .default, handler: { (_ action: UIAlertAction) -> Void in
            alert.dismiss(animated: true) { () -> Void in }
        })
        alert.addAction(okAction)
        self.present(alert, animated: true, completion: nil)
    }

    @IBAction func utilizarOtraTarjeta(_ sender: Any) {
        self.autenticarPago()
    }

}
