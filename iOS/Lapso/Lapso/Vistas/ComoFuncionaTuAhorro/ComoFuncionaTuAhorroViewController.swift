import UIKit
import Charts

class ComoFuncionaTuAhorroViewController: UIViewController {
    @IBOutlet weak var pieChartView: PieChartView!
    
    @IBOutlet weak var txtComoFuncionaAhorro: UILabel!
    @IBOutlet weak var txtValorBloqueado: UILabel!
    @IBOutlet weak var txtValorDisponible: UILabel!
    @IBOutlet weak var txtPorcentajeBloqueado: UILabel!
    @IBOutlet weak var txtPorcentajeDisponible: UILabel!
    
    let porcentajeBloqueado = 50
    let porcentajeDisponible = 50
    var valorAhorro: Double!
  
    override func viewDidLoad() {
        super.viewDidLoad()
        inicializarGrafica()
    }
    
    private func inicializarGrafica() {
        txtPorcentajeBloqueado.text = "\(porcentajeBloqueado)%"
        txtPorcentajeDisponible.text = "\(porcentajeDisponible)%"
        
        if let valor = valorAhorro {
            let valorBloqueado = Double(valor) * Double(porcentajeBloqueado) / 100
            let valorDisponible = Double(valor) * Double(porcentajeDisponible) / 100
            
            txtComoFuncionaAhorro.text = "Tu ahorro periódico es de $ \(valor) Para ayudarte a ahorrar, Lapso lo distribuirá de la siguiente manera:"
            
            txtValorBloqueado.text = "$\(valorBloqueado)"
            txtValorDisponible.text = "$\(valorDisponible)"
        }
    
        let dataPoints = ["Disponible", "Bloqueado"]
        let unitsSold = [Double(porcentajeBloqueado), Double(porcentajeDisponible)]
        
        setChart(dataPoints: dataPoints, values: unitsSold)
    }
   
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func setChart(dataPoints: [String], values: [Double]) {
        
        var dataEntries: [ChartDataEntry] = []
        
        for i in 0..<dataPoints.count {
            let dataEntry1 = PieChartDataEntry(value: values[i], label: nil)
            dataEntries.append(dataEntry1)
        }
        
        let pieChartDataSet = PieChartDataSet(values: dataEntries, label: nil)
        let pieChartData = PieChartData(dataSet: pieChartDataSet)
        pieChartView.data = pieChartData
        pieChartView.rotationEnabled = true
        pieChartView.drawEntryLabelsEnabled = false
        pieChartView.legend.enabled = false
        pieChartView.chartDescription?.enabled = false
        pieChartView.drawCenterTextEnabled = false
        pieChartView.holeColor = UIColor.clear
        pieChartView.rotationAngle = 180.0
        pieChartView.holeRadiusPercent = 0.7
        pieChartView.entryLabelColor = UIColor.clear
        pieChartView.data?.setValueTextColor(UIColor.clear)
        
        let colorBloqueado : UIColor = UIColor(hex: 0x3F6FF2);
        let colorDisponible : UIColor = UIColor(hex: 0x00BF93);
        
        var colors: [UIColor] = []
        colors.append(colorBloqueado)
        colors.append(colorDisponible)
        pieChartDataSet.colors = colors
    }
    
    
    @IBAction func irSiguienteVentana(_ sender: Any) {
        let storyboard = UIStoryboard(name: "PanelControl", bundle: nil)
        let panelViewController = storyboard.instantiateViewController(withIdentifier: "PanelContainerViewController")
        self.show(panelViewController, sender: nil)
    }
    

}
