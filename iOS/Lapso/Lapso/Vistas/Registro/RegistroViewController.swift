import UIKit

class RegistroViewController: UIViewController {

    @IBOutlet weak var headerView: HeaderViewProgress!
    @IBOutlet weak var labelProgress: UILabel!
    @IBOutlet weak var btnAtras: UIButton!

    var currentPage = 0;
    var registroUIPageViewController: RegistroUIPageViewController!

    override func viewDidLoad() {
        super.viewDidLoad()
        registroUIPageViewController = self.childViewControllers.last as! RegistroUIPageViewController;
        registroUIPageViewController.setPageManual(nuevaPagina: currentPage)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


    @IBAction func irAtras(_ sender: Any) {
        if(self.currentPage > 0) {
            registroUIPageViewController.goToPreviousPage()
        } else {
            performSegue(withIdentifier: "Landing", sender: nil)
        }
    }

    func nextPageWithIndex(currentPage: Int) {
        let i = currentPage + 1
        let max = 6

        if i <= max {
            let ratio = Float(i) / Float(max)
            self.headerView.set(progress: Float(ratio))
            labelProgress.text = "\(i)/\(max)"
            self.currentPage = currentPage
        } else {
            self.ocultarPageViewController()
        }
    }

    func openPopupDNI(image: UIImage) {
        let popOverVC = UIStoryboard(name: "FotoDNI", bundle: nil).instantiateViewController(withIdentifier: "fotoDNIPopup") as! FotoDNIPopupViewController
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        popOverVC.imagePiked.image = image
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }

    func showCodigoCorrectoPopup() {
        let popOverVC = UIStoryboard(name: "Registro", bundle: nil).instantiateViewController(withIdentifier: "CodigoCorrectoViewController") as! CodigoCorrectoViewController
        self.addChildViewController(popOverVC)

        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)

        Timer.scheduledTimer(timeInterval: 3, target: BlockOperation(block: {
            popOverVC.closePopUp()
            self.goToNextPage()
        }), selector: #selector(Operation.main), userInfo: nil, repeats: false)
    }

    func goToNextPage() {
        registroUIPageViewController.goToNextPage()
    }

    func ocultarPageViewController() {
        self.labelProgress.isHidden = true
        self.btnAtras.isHidden = true
    }

}
