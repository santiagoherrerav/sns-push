import UIKit
import LSDialogViewController

protocol SeleccionarPaisDelegate {
    func dismissDialogPais(pais: PaisDTO)
}

class RegistroUIPageViewController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource, SeleccionarPaisDelegate {

    var pageControl = UIPageControl()
    var numeroCelular = "";
    var codigoPais = "";
    var empleadoDTO: EmpleadoDTO? = nil
    var idPreregistro: String = ""

    var currentViewController: UIViewController!

    lazy var orderedViewControllers: [UIViewController] = {
        return [
            self.newVc(viewController: "pageRegistro1"),
            self.newVc(viewController: "pageRegistro2"),
            self.newVc(viewController: "pageRegistro3"),
            self.newVc(viewController: "pageRegistro4"),
            self.newVc(viewController: "pageRegistro5"),
            self.newVc(viewController: "pageRegistro6")]
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        self.dataSource = nil
        self.delegate = self
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    required init?(coder: NSCoder) {
        super.init(transitionStyle: .scroll, navigationOrientation: .horizontal, options: nil)
    }

    func newVc(viewController: String) -> UIViewController {
        return UIStoryboard(name: "Registro", bundle: nil).instantiateViewController(withIdentifier: viewController)
    }

    func pageViewController(_ pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        let pageContentViewController = pageViewController.viewControllers![0]
        self.pageControl.currentPage = orderedViewControllers.index(of: pageContentViewController)!
        self.nextPage(currentPage: orderedViewControllers.index(of: pageContentViewController)!)
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }

        let previousIndex = viewControllerIndex - 1

        guard previousIndex >= 0 else {
            return nil
        }

        guard orderedViewControllers.count > previousIndex else {
            return nil
        }

        self.currentViewController = pageViewController.viewControllers!.first!

        return orderedViewControllers[previousIndex]
    }

    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let viewControllerIndex = orderedViewControllers.index(of: viewController) else {
            return nil
        }

        let nextIndex = viewControllerIndex + 1
        let orderedViewControllersCount = orderedViewControllers.count

        guard orderedViewControllersCount != nextIndex else {
            return nil
        }

        guard orderedViewControllersCount > nextIndex else {
            return nil
        }

        self.currentViewController = pageViewController.viewControllers!.first!

        return orderedViewControllers[nextIndex]
    }


    func nextPage(currentPage: Int) {
        let parentViewController = self.parent as! RegistroViewController
        parentViewController.nextPageWithIndex(currentPage: currentPage)
    }

    func goToNextPage() {

        guard let currentViewController = self.viewControllers?.first else { return }

        guard let nextViewController = self.pageViewController(self, viewControllerAfter: currentViewController) else { return }

        self.nextPage(currentPage: orderedViewControllers.index(of: nextViewController)!)

        setViewControllers([nextViewController], direction: .forward, animated: false, completion: nil)

    }

    func goToPreviousPage() {

        guard let currentViewController = self.viewControllers?.first else { return }

        guard let previousViewController = self.pageViewController(self, viewControllerBefore: currentViewController) else { return }

        self.nextPage(currentPage: orderedViewControllers.index(of: previousViewController)!)

        setViewControllers([previousViewController], direction: .reverse, animated: false, completion: nil)
    }


    func setDatos(numeroCelular: String, codigoPais: String) {
        self.numeroCelular = numeroCelular;
        self.codigoPais = codigoPais;
    }

    func showDialog(_ animationPattern: LSAnimationPattern) {
        let dialogViewController = SeleccionarPais(nibName: "SeleccionarPais", bundle: nil)
        dialogViewController.delegate = self
        self.presentDialogViewController(dialogViewController, animationPattern: animationPattern)
    }
    
    func dismissDialogPais(pais: PaisDTO) {
        self.dismissDialogViewController(LSAnimationPattern.fadeInOut)
        let vc = self.childViewControllers.last as! ValidacionInvitacionViewController;
        vc.setPais(pais: pais)
    }

    func setDatosEmpleado(empleadoDTO: EmpleadoDTO) {
        self.empleadoDTO = empleadoDTO
    }

    func getDatosEmpleado() -> EmpleadoDTO {
        return self.empleadoDTO!
    }

    func dismissDialog(pais: PaisDTO) {
        self.dismissDialogViewController(LSAnimationPattern.fadeInOut)
        if(self.childViewControllers.last is ValidacionInvitacionViewController) {
            let vc = self.childViewControllers.last as! ValidacionInvitacionViewController;
            vc.setPais(pais: pais)
        }
    }


    func openPopupDNI(image: UIImage) {
        let parentViewController = self.parent as! RegistroViewController
        parentViewController.openPopupDNI(image: image)
    }

    func showCodigoCorrectoPopup() {
        let parentViewController = self.parent as! RegistroViewController
        parentViewController.showCodigoCorrectoPopup()
    }

    func setPageManual(nuevaPagina: Int) {
        self.nextPage(currentPage: nuevaPagina)
        setViewControllers([orderedViewControllers[nuevaPagina]], direction: .forward, animated: false, completion: nil)
    }

}

