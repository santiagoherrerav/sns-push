import UIKit

class ResumenAportePopUpViewController: UIViewController {

    @IBOutlet weak var lblMonto: UILabel!
    @IBOutlet weak var lblDestino: UILabel!
    @IBOutlet weak var lblProcedencia: UILabel!

    var delegate: ConfirmarAporteDelegate?
    var monto: String!
    var destino: String!
    var procedencia: String!

    override func viewDidLoad() {
        super.viewDidLoad()
        lblMonto.text = monto
        lblDestino.text = destino
        let regex = try! NSRegularExpression(pattern: "\\d(?=\\d{4})")
        let modifiedString: String = regex.stringByReplacingMatches(in: procedencia, options: [], range: NSRange(location: 0, length: procedencia.count), withTemplate: "*")
        lblProcedencia.text = modifiedString
    }

    @IBAction func confirmarAporte(_ sender: Any) {
        delegate?.confirmarAporteExtra()
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func dismiss(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
}


