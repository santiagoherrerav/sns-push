import UIKit
import LSDialogViewController
import Alamofire
import CeibaMobileCore
import PhoneNumberKit

class IniciarSesionViewController: BaseController, UITextFieldDelegate, SeleccionarPaisDelegate {

    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var textField3: UITextField!
    @IBOutlet weak var textField4: UITextField!
    @IBOutlet weak var txtNumeroCelular: DesignableTextField!

    var text1: String = ""
    var text2: String = ""
    var text3: String = ""
    var text4: String = ""


    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var labelPais: UILabel!
    @IBOutlet weak var imagenFlecha: UIImageView!
    @IBOutlet weak var lblIndicativo: UILabel!
    @IBOutlet weak var myNumericTextField: DesignableTextField!
    @IBOutlet weak var btnConfirmar: DesignableButtonPrimary!

    var prefijoPais: String = "Mx"

    var alert = UIAlertController(title: "Por favor espera...", message: "", preferredStyle: .alert)

    var paises: [PaisDTO] = []

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var sesionUsuarioDominio: SesionUsuarioDominio!
    var registroDominio: RegistroDominio!
    var ahorroDominio: AhorroDominio!

    override func viewDidLoad() {
        super.viewDidLoad()
        self.hideKeyboardWhenTappedAround()
        inicializarComponentesVisuales()

        btnConfirmar.isEnabled = false
        customLabelPais()
        self.inicializarDominio()
        self.eliminarDatosSesion()
    }

    @IBAction func textChanged(_ sender: UITextField) {
        if sender.hasText {
            switch sender {
            case textField1:
                text1 = textField1.text!
                textField1.text = "*"
                textField2.text = ""
                text2 = ""
                textField2.becomeFirstResponder()
            case textField2:
                text2 = textField2.text!
                textField2.text = "*"
                textField3.text = ""
                text3 = ""
                textField3.becomeFirstResponder()
            case textField3:
                text3 = textField3.text!
                textField3.text = "*"
                textField4.text = ""
                text4 = ""
                textField4.becomeFirstResponder()
            case textField4:
                text4 = textField4.text!
                textField4.text = "*"
            default:
                sender.resignFirstResponder()
            }
        }
    }

    @IBAction func antesCambiarTexto(_ sender: UITextField) {
        switch sender {
        case textField1:
            textField1.text = ""
            text1 = ""
        case textField2:
            textField2.text = ""
            text2 = ""
        case textField3:
            textField3.text = ""
            text3 = ""
        case textField4:
            textField4.text = ""
            text4 = ""
        default:
            sender.resignFirstResponder()
        }
    }

    private func inicializarDominio() {

        let repository = appDelegate.container.resolve(RegistroRepositorioProtocolo.self)!
        let sesionUsuarioRepositorio = appDelegate.container.resolve(SesionUsuarioRepositorioProtocolo.self)!
        let ahorroRepositorio = appDelegate.container.resolve(AhorroRepositorioProtocolo.self)!


        do {
            try registroDominio = RegistroDominio(registroRepositorio: repository)
            try sesionUsuarioDominio = SesionUsuarioDominio(sesionUsuarioRepositorio: sesionUsuarioRepositorio)
            try ahorroDominio = AhorroDominio(ahorroRepositorio: ahorroRepositorio)

        } catch {
            showGenericDialog(title: "Error", message: "Ocurrió un error iniciando la app", closure: {
                exit(0)
            })
        }
    }

    private func inicializarComponentesVisuales() {
        //TODO: falta obtener el pais desde el link de la invitación
        let preferences = UserDefaults.standard
        if preferences.object(forKey: "pais") != nil && preferences.object(forKey: "codigo") != nil {
            labelPais.text = preferences.string(forKey: "pais")
            prefijoPais = preferences.string(forKey: "codigo")!
            let phoneNumberKit = PhoneNumberKit()
            let countryCode: UInt64? = phoneNumberKit.countryCode(for: prefijoPais)
            self.lblIndicativo.text = "+\(String(describing: countryCode!))"
        }
    }

    func limpiarTextos() {
        text1 = ""
        text2 = ""
        text3 = ""
        text4 = ""
        textField1.text = ""
        textField2.text = ""
        textField3.text = ""
        textField4.text = ""
    }

    func customLabelPais() {
        labelPais.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userDidTapLabel(tapGestureRecognizer:)))
        labelPais.addGestureRecognizer(tapGesture)

        imagenFlecha.isUserInteractionEnabled = true
        let tapGestureFlecha = UITapGestureRecognizer(target: self, action: #selector(userDidTapFlecha(tapGestureRecognizer:)))
        imagenFlecha.addGestureRecognizer(tapGestureFlecha)
    }

    @objc func userDidTapLabel(tapGestureRecognizer: UITapGestureRecognizer) {
        self.showDialog(.slideBottomBottom)
    }

    func showDialog(_ animationPattern: LSAnimationPattern) {
        let dialogViewController = SeleccionarPais(nibName: "SeleccionarPais", bundle: nil)
        dialogViewController.delegate = self
        self.presentDialogViewController(dialogViewController, animationPattern: animationPattern)
    }

    @objc func userDidTapFlecha(tapGestureRecognizer: UITapGestureRecognizer) {
        self.showDialog(.slideBottomBottom)
    }

    func dismissDialogPais(pais: PaisDTO) {
        self.dismissDialogViewController(LSAnimationPattern.fadeInOut)
        self.setPais(pais: pais)
    }

    @IBAction func btnIniciarSesion(_ sender: Any) {

        let clave: String = text1 + text2 + text3 + text4
        let numeroCelular = self.txtNumeroCelular.text
        if self.estanDatosLLenos(celular: numeroCelular!, clave: clave) {
            self.present(alert, animated: true, completion: nil)
            iniciarSesion(numeroCelular: numeroCelular!, clave: clave)
        } else {
            self.showGenericDialog(title: "Error", message: "Debes ingresar la contraseña y  un número de célular valido", closure: {

            })
        }
    }

    private func eliminarDatosSesion() {
        self.sesionUsuarioDominio.eliminarTokenPagos()
        self.sesionUsuarioDominio.eliminarIdEmpresa()
    }

    private func iniciarSesion(numeroCelular: String, clave: String) {
        executeEvent(event: createAsyncEvent(closure: {
            try self.sesionUsuarioDominio.iniciarSesion(codigoPais: self.prefijoPais, numeroCelular: numeroCelular, clave: clave, onFinish: {
                self.consultarAhorrador()
            }, onError: {
                error in  self.alert.dismiss(animated: true, completion: {
                    self.manejarError(error: error)
                })
                })
        }))
    }

    func manejarError(error: Error) {
        processError(error: error)
    }

    private func consultarAhorrador() {
        executeEvent(event: createAsyncEvent(closure: {
            try self.ahorroDominio.consultarAhorrador(onFinish: { resumenAhorrador in
                self.alert.dismiss(animated: true, completion: {
                    self.datosAhorrador(resumenAhorrador: resumenAhorrador)
                })
            }, onError: { error in
                    self.alert.dismiss(animated: true, completion: {
                        self.manejarError(error: error)
                    })
                })
        }))
    }


    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if let destinationViewController = segue.destination as? RegistroViewController {
            destinationViewController.currentPage = 5
        }
    }

    private func datosAhorrador(resumenAhorrador: ResumenAhorradorDTO) {
        if !resumenAhorrador.tieneDocumentoIdentidad {
            self.performSegue(withIdentifier: "Registro", sender: nil)
        } else {
            if resumenAhorrador.porcentajeAhorro.count >= 1 {
                let porcentajeEmpresa = resumenAhorrador.porcentajeAhorro[0]
                sesionUsuarioDominio.guardarIdEmpresa(idEmpresa: porcentajeEmpresa.uuidEmpresa)
                if porcentajeEmpresa.tienePorcentajeAhorro {
                    let storyboard = UIStoryboard(name: "PanelControl", bundle: nil)
                    let panelViewController = storyboard.instantiateViewController(withIdentifier: "PanelContainerViewController")
                    self.show(panelViewController, sender: nil)
                } else {
                    self.performSegue(withIdentifier: "DefinicionAhorroViewController", sender: nil)
                }
            }
        }
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        textField.resignFirstResponder()

        return true;
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 200), animated: true)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        let clave: String = text1 + text2 + text3 + text4
        let numeroCelular = self.txtNumeroCelular.text
        if self.estanDatosLLenos(celular: numeroCelular!, clave: clave) {
            self.btnConfirmar.isEnabled = true
        } else {
            self.btnConfirmar.isEnabled = false
        }
    }

    func setPais(pais: PaisDTO) {
        self.lblIndicativo.text = pais.indicativo
        self.labelPais.text = pais.nombre
        self.prefijoPais = pais.sigla
    }

    private func estanDatosLLenos(celular: String, clave: String) -> Bool {
        if(clave.count == 4 && celular.count > 5) {
            return true
        }
        return false
    }
}
