import UIKit

class IngresoClaveViewController: UIViewController {
    
    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var textField3: UITextField!
    @IBOutlet weak var textField4: UITextField!
    var text1: String = ""
    var text2: String = ""
    var text3: String = ""
    var text4: String = ""
    
    @IBAction func textChanged(_ sender: UITextField) {
        if sender.hasText {
            switch sender {
            case textField1:
                text1 = textField1.text!
                textField1.text = "*"
                textField2.becomeFirstResponder()
            case textField2:
                text2 = textField2.text!
                textField2.text = "*"
                textField3.becomeFirstResponder()
            case textField3:
                text3 = textField3.text!
                textField3.text = "*"
                textField4.becomeFirstResponder()
            default:
                sender.resignFirstResponder()
            }
        }
        validarClaveCompleta()
    }

    @IBAction func back(_ sender: Any) {
        text4 = textField4.text!
        textField4.text = "*"
        validarClaveCompleta()
        
        let storyBoard: UIStoryboard = UIStoryboard(name: "Bienvenida", bundle: nil)
        let newViewController = storyBoard.instantiateViewController(withIdentifier: "GestionAhorroDisponibleViewController") as! GestionAhorroDisponibleViewController
        self.present(newViewController, animated: true, completion: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //ocultar teclado
        self.hideKeyboardWhenTappedAround()
        
        self.textField1.becomeFirstResponder()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cambiarColorTextFields( color : Int){
        textField1.textColor = UIColor(hex: color)
        textField2.textColor = UIColor(hex: color)
        textField3.textColor = UIColor(hex: color)
        textField4.textColor = UIColor(hex: color)
        textField1.layer.borderColor = UIColor(hex: color).cgColor
        textField2.layer.borderColor = UIColor(hex: color).cgColor
        textField3.layer.borderColor = UIColor(hex: color).cgColor
        textField4.layer.borderColor = UIColor(hex: color).cgColor
        textField1.text = textField1.text
        textField2.text = textField2.text
        textField3.text = textField3.text
        textField4.text = textField4.text
    }
    
    func validarClaveCompleta(){
        let estaCompleta : Bool = !(textField1.text?.isEmpty ?? true) && !(textField2.text?.isEmpty ?? true) && !(textField3.text?.isEmpty ?? true) && !(textField4.text?.isEmpty ?? true)
        if estaCompleta{
            cambiarColorTextFields(color: 0x00BF93)
            
        }else{
            cambiarColorTextFields(color: 0xC3C8D1)
        }
    }
}
