import UIKit

import CeibaMobileCore

import AWSCognitoIdentityProvider

class FotoDNIPopupViewController: BaseController {

    @IBOutlet weak var imagePiked: UIImageView!
    var empleadoDTO: EmpleadoDTO? = nil
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var registroDominio: RegistroDominio!

    var pool: AWSCognitoIdentityUserPool? = AWSCognitoIdentityUserPool.init(forKey: AWSCognitoUserPoolsSignInProviderKey)
    
    var alert = UIAlertController(title: "Por favor espera...", message: "", preferredStyle: .alert)

    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = UIColor(red: 0 / 255, green: 200 / 255, blue: 152 / 255, alpha: 0.9)
        self.showAnimate()

        inicializarDominio()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func inicializarDominio() {
        let registroRepositorio = appDelegate.container.resolve(RegistroRepositorioProtocolo.self)!
        do {
            try registroDominio = RegistroDominio(registroRepositorio: registroRepositorio)
        } catch {
            showGenericDialog(title: "Error", message: "Ocurrió un error iniciando la app", closure: {
                exit(0)
            })
        }
    }

    @IBAction func closePopUp(_ sender: AnyObject) {
        cargarFotoDNI()
    }

    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }

    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.3, y: 1.3)
            self.view.alpha = 0.0;
        }, completion: { (finished: Bool) in
                if (finished)
                    {
                    self.view.removeFromSuperview()
                }
            });
    }

    func cargarFotoDNI() {
         self.present(alert, animated: true, completion: nil)
        let uuidUsuario = self.pool?.currentUser()?.username
        let image = self.imagePiked.image?.compressImage()

        self.pool?.currentUser()?.getSession().continueOnSuccessWith { (task) -> AnyObject? in
            DispatchQueue.main.async(execute: {
                let result = task.result
                
                self.eventoCargarFotoDNI(uuidUsuario: uuidUsuario!, image: image!, token: (result?.accessToken?.tokenString)!)

            })
            return nil
        }
        
    }
    
    func eventoCargarFotoDNI(uuidUsuario: String, image: UIImage, token: String){
        executeEvent(event: createAsyncEvent(closure: {
            try self.registroDominio.cargarFotoDNI(uuidUsuario: uuidUsuario, fotoDNI: image, tokenAutorizacion: token, onFinish: {
                self.alert.dismiss(animated: true, completion: {
                    self.removeAnimate()
                    self.performSegue(withIdentifier: "Bienvenida", sender: nil)
                })
            }, onError: { error in
                self.alert.dismiss(animated: true, completion: nil)
                self.processError(error: error)
            })
        }))
    }
    
}
