import UIKit
import Alamofire
import SwiftyJSON


class FotoDNIViewController: UIViewController, UIImagePickerControllerDelegate,
    UINavigationControllerDelegate {

        @IBOutlet weak var imagePicked: UIImageView!
        var imagePickerController: UIImagePickerController!

        override func viewDidLoad() {
            super.viewDidLoad()
        }

        override func didReceiveMemoryWarning() {
            super.didReceiveMemoryWarning()
        }

        @IBAction func openPopup(_ sender: Any) {
            if UIImagePickerController.isSourceTypeAvailable(.camera) {
                imagePickerController = UIImagePickerController()
                imagePickerController.delegate = self
                imagePickerController.sourceType = .camera;
                imagePickerController.allowsEditing = false
                self.present(imagePickerController, animated: true, completion: nil)
            }
        }

        func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
            imagePickerController.dismiss(animated: true, completion: nil)
            let parentViewController = self.parent as! RegistroUIPageViewController
            parentViewController.openPopupDNI(image: (info[UIImagePickerControllerOriginalImage] as? UIImage)!)
        }
}

