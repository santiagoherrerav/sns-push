import UIKit
import WebKit
import CeibaMobileCore


class ServicioClienteViewController:  BaseController {

    @IBOutlet weak var myWebView: UIWebView!
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var sesionUsuarioDominio: SesionUsuarioDominio!
    var idUsuario : String = ""
    var alert = UIAlertController(title: "Por favor espera...", message: "", preferredStyle: .alert)


    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.present(alert, animated: true, completion: nil)
        self.inicializarDominio()
        self.inicializarChat()
    }
    
    private func inicializarDominio() {
        
        let sesionUsuarioRepositorio = appDelegate.container.resolve(SesionUsuarioRepositorioProtocolo.self)!
        
        do {
            try sesionUsuarioDominio = SesionUsuarioDominio(sesionUsuarioRepositorio: sesionUsuarioRepositorio)
        } catch {
            showGenericDialog(title: "Error", message: "Ocurrió un error iniciando la app", closure: {
                exit(0)
            })
        }
    }
    
    private func inicializarChat(){
        self.consultarUsuario()
    }
    
    private func consultarUsuario() {
        executeEvent(event: createAsyncEvent(closure: {
            try self.sesionUsuarioDominio.obtenerSesionActual(onFinish: self.datosUsuario, onError: self.manejarError)
        }))

    }
    
    private func manejarError(error: Error) {
        self.dismiss(animated: true, completion: {
            self.processError(error: error)
        })
    }
    
    private func datosUsuario(usuario: UsuarioDTO) {
        idUsuario = usuario.id
        
        let url = URL (string: "https://pratech-chatbot-view-haceb.mybluemix.net/haceb/demo/mobile/fullwindow/?k=1001");
        if let unwrapper = url{
            let requestObj = URLRequest(url: unwrapper);
            myWebView.loadRequest(requestObj);
        }
    }
    
    @IBAction func irAtras(_ sender: Any) {
        self.dismiss(animated: true, completion: nil)
    }
}
