import UIKit
import LSDialogViewController


import CeibaMobileCore

class ConfirmacionClaveViewController: BaseController {

    @IBOutlet weak var textField1: UITextField!
    @IBOutlet weak var textField2: UITextField!
    @IBOutlet weak var textField3: UITextField!
    @IBOutlet weak var textField4: UITextField!
    
    @IBOutlet weak var viewError: UIView!
    
    var text1: String = ""
    var text2: String = ""
    var text3: String = ""
    var text4: String = ""
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var registroDominio: RegistroDominio!
    
    @IBAction func textChanged(_ sender: UITextField) {
        if sender.hasText {
            switch sender {
            case textField1:
                text1 = textField1.text!
                textField1.text = "*"
                textField2.becomeFirstResponder()
            case textField2:
                text2 = textField2.text!
                textField2.text = "*"
                textField3.becomeFirstResponder()
            case textField3:
                text3 = textField3.text!
                textField3.text = "*"
                textField4.becomeFirstResponder()
            case textField4:
                text4 = textField4.text!
                textField4.text = "*"
            default:
                sender.resignFirstResponder()
            }
        }
        
        if validarClaveCompleta(){
            let claveRepetida = text1 + text2 + text3 + text4
            validarClave(claveConfirmada: claveRepetida, sender: sender)
        }else{
            cambiarColorTextFields(color: 0xC3C8D1)
        }
    }
    
    @IBAction func antesCambiarTexto(_ sender: UITextField) {
        switch sender {
        case textField1:
            textField1.text = ""
            text1 = ""
        case textField2:
            textField2.text = ""
            text2 = ""
        case textField3:
            textField3.text = ""
            text3 = ""
        case textField4:
            textField4.text = ""
            text4 = ""
        default:
            sender.resignFirstResponder()
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        //ocultar teclado
        self.hideKeyboardWhenTappedAround()
        self.textField1.becomeFirstResponder()
        self.viewError.isHidden = true

        
        let repository = appDelegate.container.resolve(RegistroRepositorioProtocolo.self)!
        do {
            try registroDominio = RegistroDominio(registroRepositorio: repository)
        } catch {
            showGenericDialog(title: "Error", message: "Ocurrió un error iniciando la app", closure: { exit(0) })
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    func cambiarColorTextFields( color : Int){
        textField1.textColor = UIColor(hex: color)
        textField2.textColor = UIColor(hex: color)
        textField3.textColor = UIColor(hex: color)
        textField4.textColor = UIColor(hex: color)
        textField1.layer.borderColor = UIColor(hex: color).cgColor
        textField2.layer.borderColor = UIColor(hex: color).cgColor
        textField3.layer.borderColor = UIColor(hex: color).cgColor
        textField4.layer.borderColor = UIColor(hex: color).cgColor
        textField1.text = textField1.text
        textField2.text = textField2.text
        textField3.text = textField3.text
        textField4.text = textField4.text
    }
    
    func validarClaveCompleta() -> Bool {
        return !(textField1.text?.isEmpty ?? true) && !(textField2.text?.isEmpty ?? true) && !(textField3.text?.isEmpty ?? true) && !(textField4.text?.isEmpty ?? true)
    }
    
    func validarClave(claveConfirmada : String, sender: UITextField)  {
        let parentViewController = self.parent as! RegistroUIPageViewController
        let empleadoDTO = parentViewController.getDatosEmpleado()
        let clave = empleadoDTO.clave
        
        let isRepetida=self.registroDominio.validarConfirmacionlave( clave : clave!, claveConfirmacion : claveConfirmada);
        
        if isRepetida{
            UIView.animate(withDuration: 0.5, animations: {
                self.viewError.isHidden = true
            }, completion: nil)
            cambiarColorTextFields(color: 0x00BF93)
            
            if sender == textField4{
                parentViewController.goToNextPage()
            }
        }else{
            UIView.animate(withDuration: 0.5, animations: {
                self.viewError.isHidden = false
            }, completion: nil)
            
            cambiarColorTextFields(color: 0xE62C17)
        }
    }
}
