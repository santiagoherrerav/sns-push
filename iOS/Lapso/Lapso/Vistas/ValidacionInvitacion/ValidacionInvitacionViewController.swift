import UIKit
import LSDialogViewController
import Alamofire

import CeibaMobileCore

import PhoneNumberKit

class ValidacionInvitacionViewController: BaseController, UITextFieldDelegate {

    @IBOutlet weak var labelPais: UILabel!
    @IBOutlet weak var labelIndicativo: UILabel!
    @IBOutlet weak var imagenFlecha: UIImageView!

    @IBOutlet weak var scrollView: UIScrollView!
    @IBOutlet weak var myNumericTextField: UITextField!

    @IBOutlet weak var btnConfirmar: DesignableButtonPrimary!
    var prefijoPais: String = "Mx"

    var alert = UIAlertController(title: "Por favor espera...", message: "", preferredStyle: .alert)

    var paises: [PaisDTO] = []

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var registroDominio: RegistroDominio!
    var sesionUsuarioDominio: SesionUsuarioDominio!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        inicializarComponentesVisuales()

        self.hideKeyboardWhenTappedAround()
        btnConfirmar.isEnabled = false
        customLabelPais()
    }

    override func viewDidAppear(_ animated: Bool) {
        self.inicializarDominio()
        self.eliminarDatosSesion()
    }

    private func eliminarDatosSesion() {
        self.sesionUsuarioDominio.eliminarTokenPagos()
        self.sesionUsuarioDominio.eliminarIdEmpresa()
    }

    func customLabelPais() {
        labelPais.isUserInteractionEnabled = true
        let tapGesture = UITapGestureRecognizer(target: self, action: #selector(userDidTapLabel(tapGestureRecognizer:)))
        labelPais.addGestureRecognizer(tapGesture)

        imagenFlecha.isUserInteractionEnabled = true
        let tapGestureFlecha = UITapGestureRecognizer(target: self, action: #selector(userDidTapFlecha(tapGestureRecognizer:)))
        imagenFlecha.addGestureRecognizer(tapGestureFlecha)
    }

    func inicializarDominio() {
        let repository = appDelegate.container.resolve(RegistroRepositorioProtocolo.self)!
        let sesionUsuarioRepositorio = appDelegate.container.resolve(SesionUsuarioRepositorioProtocolo.self)!
        do {
            try registroDominio = RegistroDominio(registroRepositorio: repository)
            try sesionUsuarioDominio = SesionUsuarioDominio(sesionUsuarioRepositorio: sesionUsuarioRepositorio)
        } catch {
            showGenericDialog(title: "Error", message: "Ocurrió un error iniciando la app", closure: { exit(0) })
        }
    }
    
    private func inicializarComponentesVisuales() {
        let preferences = UserDefaults.standard
        if preferences.object(forKey: "pais") != nil && preferences.object(forKey: "codigo") != nil {
            labelPais.text = preferences.string(forKey: "pais")
            prefijoPais = preferences.string(forKey: "codigo")!
            let phoneNumberKit = PhoneNumberKit()
            let countryCode: UInt64? = phoneNumberKit.countryCode(for: prefijoPais)
            self.labelIndicativo.text = "+\(String(describing: countryCode!))"
        }
    }

    @objc func userDidTapLabel(tapGestureRecognizer: UITapGestureRecognizer) {
        let parentViewController = self.parent as! RegistroUIPageViewController
        parentViewController.showDialog(.slideBottomBottom)
    }

    @objc func userDidTapFlecha(tapGestureRecognizer: UITapGestureRecognizer) {
        let parentViewController = self.parent as! RegistroUIPageViewController
        parentViewController.showDialog(.slideBottomBottom)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    @IBAction func goToNextPage(_ sender: Any) {
        self.present(alert, animated: true, completion: nil)
        executeEvent(event: createAsyncEvent(closure: {
            try self.registroDominio.validarInvitacionUsuario(codigoPais: self.prefijoPais, numeroCelular: self.myNumericTextField.text!, onFinish: self.invitacionValidada, onError: self.errorInvitacion)
        }))
    }

    func invitacionValidada() {
        self.dismiss(animated: true, completion: {
            let parentViewController = self.parent as! RegistroUIPageViewController
            let datosEmpleado = EmpleadoDTO()
            datosEmpleado.siglaPais = self.prefijoPais
            datosEmpleado.celular = self.myNumericTextField.text!
            parentViewController.setDatosEmpleado(empleadoDTO: datosEmpleado)
            parentViewController.setDatos(numeroCelular: self.myNumericTextField.text!, codigoPais: self.prefijoPais)
            self.proximaPagina()
        })
    }

    func errorInvitacion(error: Error) {
        self.dismiss(animated: true, completion: {
            self.processError(error: error)
        })
    }

    func proximaPagina() {
        let parentViewController = self.parent as! RegistroUIPageViewController
        parentViewController.goToNextPage()
    }

    func textFieldShouldReturn(_ textField: UITextField) -> Bool {

        textField.resignFirstResponder()

        return true;
    }

    func textFieldDidBeginEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 250), animated: true)
    }

    func textFieldDidEndEditing(_ textField: UITextField) {
        scrollView.setContentOffset(CGPoint(x: 0, y: 0), animated: true)
        btnConfirmar.isEnabled = true
    }

    func setPais(pais: PaisDTO) {
        self.labelIndicativo.text = pais.indicativo
        self.labelPais.text = pais.nombre
        self.prefijoPais = pais.sigla
    }

}
