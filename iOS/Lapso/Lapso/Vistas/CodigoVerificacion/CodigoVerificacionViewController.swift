import UIKit

import CeibaMobileCore


class CodigoVerificacionViewController: BaseController {

    @IBOutlet weak
    var textField1: UITextField!
    @IBOutlet weak
    var textField2: UITextField!
    @IBOutlet weak
    var textField3: UITextField!
    @IBOutlet weak
    var textField4: UITextField!
    @IBOutlet weak
    var textField5: UITextField!
    @IBOutlet weak
    var textField6: UITextField!

    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var registroDominio: RegistroDominio!
    var sesionUsuarioDominio: SesionUsuarioDominio!
    var ahorroDominio: AhorroDominio!
    var empleadoDTO: EmpleadoDTO!
    var alert = UIAlertController(title: "Por favor espera...", message: "", preferredStyle: .alert)


    override func viewDidLoad() {
        super.viewDidLoad()
        self.textField1.becomeFirstResponder()

        self.hideKeyboardWhenTappedAround()
        obtenerDatosEmpleado()
        inicializarDominio()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }

    func inicializarDominio() {
        let registroRepositorio = appDelegate.container.resolve(RegistroRepositorioProtocolo.self)!
        let sesionUsuarioRepositorio = appDelegate.container.resolve(SesionUsuarioRepositorioProtocolo.self)!
        let ahorroRepositorio = appDelegate.container.resolve(AhorroRepositorioProtocolo.self)!
        do {
            try registroDominio = RegistroDominio(registroRepositorio: registroRepositorio)
            try sesionUsuarioDominio = SesionUsuarioDominio(sesionUsuarioRepositorio: sesionUsuarioRepositorio)
            try ahorroDominio = AhorroDominio(ahorroRepositorio: ahorroRepositorio)
        } catch {
            showGenericDialog(title: "Error", message: "Ocurrió un error iniciando la app", closure: {
                exit(0)
            })
        }
    }

    @IBAction func textChanged(_ sender: UITextField) {
        if sender.hasText {
            switch sender {
            case textField1:
                textField2.becomeFirstResponder()
            case textField2:
                textField3.becomeFirstResponder()
            case textField3:
                textField4.becomeFirstResponder()
            case textField4:
                textField5.becomeFirstResponder()
            case textField5:
                textField6.becomeFirstResponder()
            default:
                sender.resignFirstResponder()
            }
        }
    }

    @IBAction func reenviarCodigoVerificacion(_ sender: Any) {
        self.textField1.text = ""
        self.textField2.text = ""
        self.textField3.text = ""
        self.textField4.text = ""
        self.textField5.text = ""
        self.textField6.text = ""
        self.textField1.becomeFirstResponder()
        self.reenviarCodigoVerificacion()
    }

    func obtenerDatosEmpleado() {
        let parentViewController = self.parent as! RegistroUIPageViewController
        empleadoDTO = parentViewController.getDatosEmpleado()
    }

    func reenviarCodigoVerificacion() {
        self.present(alert, animated: true, completion: nil)
        executeEvent(event: createAsyncEvent(closure: {
            try self.registroDominio.reenviarCodigoVerificacion(codigoPais: self.empleadoDTO.siglaPais, numeroCelular: self.empleadoDTO.celular, onFinish: {
                self.alert.dismiss(animated: true, completion: nil)
            }, onError: {
                    error in
                    self.alert.dismiss(animated: true, completion: {
                        self.processError(error: error)
                    })
                })
        }))
    }

    @IBAction func validarCodigoVerificacion(_ sender: Any) {
        self.present(alert, animated: true, completion: nil)
        let codigoVerificacion = "\(self.textField1.text!)\(self.textField2.text!)\(self.textField3.text!)\(self.textField4.text!)\(self.textField5.text!)\(self.textField6.text!)"

        executeEvent(event: createAsyncEvent(closure: {
            try self.registroDominio.validarCodigoVerificacion(codigoPais: self.empleadoDTO.siglaPais, numeroCelular: self.empleadoDTO.celular, codigoVerificacion: codigoVerificacion, onFinish: {
                self.iniciarSesion()
            }, onError: {
                    error in
                    self.alert.dismiss(animated: true, completion: {
                        self.processError(error: error)
                    })
                })
        }))
    }

    func iniciarSesion() {
        executeEvent(event: createAsyncEvent(closure: {
            try self.sesionUsuarioDominio.iniciarSesion(codigoPais: self.empleadoDTO.siglaPais, numeroCelular: self.empleadoDTO.celular, clave: self.empleadoDTO.clave, onFinish: { self.crearAhorrador()
            }, onError: {
                    error in
                    self.alert.dismiss(animated: true, completion: {
                        self.processError(error: error)
                    })
                })
        }))
    }

    func crearAhorrador() {
        executeEvent(event: createAsyncEvent(closure: {
            try self.ahorroDominio.crearAhorrador(codigoEmpleado: self.empleadoDTO.idEmpleado, idEmpresa: self.empleadoDTO.empresa.uuid, onFinish: {
                let parentViewController = self.parent as! RegistroUIPageViewController
                self.alert.dismiss(animated: true, completion: {
                    self.textField6.resignFirstResponder()
                    parentViewController.showCodigoCorrectoPopup()
                })

            }, onError: {
                    error in
                    self.alert.dismiss(animated: true, completion: {
                        self.processError(error: error)
                    })
                })
        }))
    }

}
