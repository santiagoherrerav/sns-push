/*
 Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */
 

import Foundation
import AWSCore

@objcMembers
public class LAPSORegistrarUsuariorequest : AWSModel {
    
    /**  */
    var usuario: String!
    /**  */
    var contrasena: String!
    /**  */
    var nombres: String!
    /**  */
    var apellidos: String!
    /**  */
    var celular: String!
    /**  */
    var correo: String!
    /**  */
    var dni: String!
    
   	public override static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]!{
		var params:[AnyHashable : Any] = [:]
		params["usuario"] = "usuario"
		params["contrasena"] = "contrasena"
		params["nombres"] = "nombres"
		params["apellidos"] = "apellidos"
		params["celular"] = "celular"
		params["correo"] = "correo"
		params["dni"] = "dni"
		
        return params
	}
}
