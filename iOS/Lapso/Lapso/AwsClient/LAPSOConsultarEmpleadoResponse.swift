/*
 Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */
 

import Foundation
import AWSCore

@objcMembers
public class LAPSOConsultarEmpleadoResponse : AWSModel {
    
    /**  */
    var idEmpleado: String!
    /**  */
    var primerNombre: String!
    /**  */
    var otrosNombres: String!
    /**  */
    var primerApellido: String!
    /**  */
    var segundoApellido: String!
    /**  */
    var correo: String!
    var empresa: LAPSOConsultarEmpleadoResponse_empresa!
    
   	public override static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]!{
		var params:[AnyHashable : Any] = [:]
		params["idEmpleado"] = "idEmpleado"
		params["primerNombre"] = "primerNombre"
		params["otrosNombres"] = "otrosNombres"
		params["primerApellido"] = "primerApellido"
		params["segundoApellido"] = "segundoApellido"
		params["correo"] = "correo"
		params["empresa"] = "empresa"
		
        return params
	}
	class func empresaJSONTransformer() -> ValueTransformer{
	    return ValueTransformer.awsmtl_JSONDictionaryTransformer(withModelClass: LAPSOConsultarEmpleadoResponse_empresa.self);
	}
}
