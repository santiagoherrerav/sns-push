/*
 Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */
 

import Foundation
import AWSCore

@objcMembers
public class LAPSOCrearMetaResponse : AWSModel {
    
    /**  */
    var idAhorrador: String!
    /**  */
    var uuid: String!
    /**  */
    var nombre: String!
    /**  */
    var monto: NSNumber!
    /**  */
    var fechaFinal: String!
    /**  */
    var montoProgramado: NSNumber!
    /**  */
    var bloqueada: NSNumber!
    /**  */
    var idImagen: NSNumber!
    /**  */
    var hitosAporte: [LAPSOHitosAporte]!
    
   	public override static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]!{
		var params:[AnyHashable : Any] = [:]
		params["idAhorrador"] = "idAhorrador"
		params["uuid"] = "uuid"
		params["nombre"] = "nombre"
		params["monto"] = "monto"
		params["fechaFinal"] = "fechaFinal"
		params["montoProgramado"] = "montoProgramado"
		params["bloqueada"] = "bloqueada"
		params["idImagen"] = "idImagen"
		params["hitosAporte"] = "hitosAporte"
		
        return params
	}
	class func hitosAporteJSONTransformer() -> ValueTransformer{
		return  ValueTransformer.awsmtl_JSONArrayTransformer(withModelClass: LAPSOHitosAporte.self);
	}
}
