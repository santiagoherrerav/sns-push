/*
 Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */
 

import Foundation
import AWSCore

@objcMembers
public class LAPSOConsultarAhorradorResponse : AWSModel {
    
    /**  */
    var id: String!
    /**  */
    var primerNombre: String!
    /**  */
    var otrosNombres: String!
    /**  */
    var primerApellido: String!
    /**  */
    var segundoApellido: String!
    /**  */
    var totalMontoBloqueado: NSNumber!
    /**  */
    var totalMontoDisponible: NSNumber!
    /**  */
    var totalMontoMetas: NSNumber!
    /**  */
    var tieneDocumentoIdentidad: NSNumber!
    /**  */
    var porcentajeAhorro: [LAPSOConsultarSiTienePorcentajesResponse]!
    
   	public override static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]!{
		var params:[AnyHashable : Any] = [:]
		params["id"] = "id"
		params["primerNombre"] = "primerNombre"
		params["otrosNombres"] = "otrosNombres"
		params["primerApellido"] = "primerApellido"
		params["segundoApellido"] = "segundoApellido"
		params["totalMontoBloqueado"] = "totalMontoBloqueado"
		params["totalMontoDisponible"] = "totalMontoDisponible"
		params["totalMontoMetas"] = "totalMontoMetas"
		params["tieneDocumentoIdentidad"] = "tieneDocumentoIdentidad"
		params["porcentajeAhorro"] = "porcentajeAhorro"
		
        return params
	}
	class func porcentajeAhorroJSONTransformer() -> ValueTransformer{
		return  ValueTransformer.awsmtl_JSONArrayTransformer(withModelClass: LAPSOConsultarSiTienePorcentajesResponse.self);
	}
}
