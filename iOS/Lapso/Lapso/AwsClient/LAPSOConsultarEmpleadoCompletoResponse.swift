/*
 Copyright 2010-2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.

 Licensed under the Apache License, Version 2.0 (the "License").
 You may not use this file except in compliance with the License.
 A copy of the License is located at

 http://aws.amazon.com/apache2.0

 or in the "license" file accompanying this file. This file is distributed
 on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 express or implied. See the License for the specific language governing
 permissions and limitations under the License.
 */
 

import Foundation
import AWSCore

@objcMembers
public class LAPSOConsultarEmpleadoCompletoResponse : AWSModel {
    
    var cargo: String!
    var celular: LAPSOConsultarEmpleadoCompletoResponse_celular?
    var correo: String?
    var cuenta: String?
    var estado: String?
    var fechaIngreso: String?
    var frecuenciaPago: String?
    var idEmpleado: String?
    var idUsuario: String?
    var montoDeduccion: NSNumber?
    var montoPago: String?
    var otrosNombres: String?
    var porcentajeAhorro: String?
    var primerApellido: String?
    var primerNombre: String?
    var segundoApellido: String?
    
   	public override static func jsonKeyPathsByPropertyKey() -> [AnyHashable : Any]!{
		var params:[AnyHashable : Any] = [:]
		params["cargo"] = "cargo"
		params["celular"] = "celular"
		params["correo"] = "correo"
		params["cuenta"] = "cuenta"
		params["estado"] = "estado"
		params["fechaIngreso"] = "fechaIngreso"
		params["frecuenciaPago"] = "frecuenciaPago"
		params["idEmpleado"] = "idEmpleado"
		params["idUsuario"] = "idUsuario"
		params["montoDeduccion"] = "montoDeduccion"
		params["montoPago"] = "montoPago"
		params["otrosNombres"] = "otrosNombres"
		params["porcentajeAhorro"] = "porcentajeAhorro"
		params["primerApellido"] = "primerApellido"
		params["primerNombre"] = "primerNombre"
		params["segundoApellido"] = "segundoApellido"
		
        return params
	}
	class func celularJSONTransformer() -> ValueTransformer{
	    return ValueTransformer.awsmtl_JSONDictionaryTransformer(withModelClass: LAPSOConsultarEmpleadoCompletoResponse_celular.self);
	}
}
