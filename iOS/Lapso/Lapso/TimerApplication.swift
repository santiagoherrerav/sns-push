import UIKit

extension Notification.Name {
    static let appTimeout = Notification.Name("appTimeout")
}

class TimerApplication: UIApplication {

    private var timeoutInSeconds: TimeInterval {
        return 5 * 60
    }

    private var idleTimer: Timer?

    private func resetIdleTimer() {
        if let idleTimer = idleTimer {
            idleTimer.invalidate()
        }

        idleTimer = Timer.scheduledTimer(timeInterval: timeoutInSeconds,
            target: self,
            selector: #selector(TimerApplication.timeHasExceeded),
            userInfo: nil,
            repeats: false
        )
    }

    @objc private func timeHasExceeded() {
        NotificationCenter.default.post(name: .appTimeout, object: nil)
    }

    override func sendEvent(_ event: UIEvent) {

        super.sendEvent(event)

        if idleTimer != nil {
            self.resetIdleTimer()
        }

        if let touches = event.allTouches {
            for touch in touches where touch.phase == UITouchPhase.began {
                self.resetIdleTimer()
            }
        }
    }
}
