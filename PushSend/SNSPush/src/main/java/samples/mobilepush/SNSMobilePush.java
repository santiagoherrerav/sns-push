package samples.mobilepush;

/*
 * Copyright 2014 Amazon.com, Inc. or its affiliates. All Rights Reserved.
 *
 * Licensed under the Apache License, Version 2.0 (the "License").
 * You may not use this file except in compliance with the License.
 * A copy of the License is located at
 *
 * http://aws.amazon.com/apache2.0
 *
 * or in the "license" file accompanying this file. This file is distributed
 * on an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either
 * express or implied. See the License for the specific language governing
 * permissions and limitations under the License.
 */

import com.amazonaws.AmazonClientException;
import com.amazonaws.AmazonServiceException;
import com.amazonaws.auth.PropertiesCredentials;
import com.amazonaws.services.sns.AmazonSNS;
import com.amazonaws.services.sns.AmazonSNSClient;
import com.amazonaws.services.sns.model.MessageAttributeValue;
import samples.tools.AmazonSNSClientWrapper;
import samples.tools.SampleMessageGenerator;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class SNSMobilePush {

	private AmazonSNSClientWrapper snsClientWrapper;

	public SNSMobilePush(AmazonSNS snsClient) {
		this.snsClientWrapper = new AmazonSNSClientWrapper(snsClient);
	}

	public static final Map<SampleMessageGenerator.Platform, Map<String, MessageAttributeValue>> attributesMap = new HashMap<SampleMessageGenerator.Platform, Map<String, MessageAttributeValue>>();
	static {
		attributesMap.put(SampleMessageGenerator.Platform.ADM, null);
		attributesMap.put(SampleMessageGenerator.Platform.GCM, null);
		attributesMap.put(SampleMessageGenerator.Platform.APNS, null);
		attributesMap.put(SampleMessageGenerator.Platform.APNS_SANDBOX, null);
		attributesMap.put(SampleMessageGenerator.Platform.BAIDU, addBaiduNotificationAttributes());
		attributesMap.put(SampleMessageGenerator.Platform.WNS, addWNSNotificationAttributes());
		attributesMap.put(SampleMessageGenerator.Platform.MPNS, addMPNSNotificationAttributes());
	}

	public static void main(String[] args) throws IOException {
		/*
		 * TODO: Be sure to fill in your AWS access credentials in the
		 * AwsCredentials.properties file before you try to run this sample.
		 * http://aws.amazon.com/security-credentials
		 */
		AmazonSNS sns = new AmazonSNSClient(new PropertiesCredentials(
				SNSMobilePush.class
						.getResourceAsStream("AwsCredentials.properties")));

		sns.setEndpoint("https://sns.us-west-1.amazonaws.com");
		System.out.println("===========================================\n");
		System.out.println("Getting Started with Amazon SNS");
		System.out.println("===========================================\n");
		try {
			SNSMobilePush sample = new SNSMobilePush(sns);
			/* TODO: Uncomment the services you wish to use. */
			sample.demoAndroidAppNotification();
			// sample.demoKindleAppNotification();
			// sample.demoAppleAppNotification();
			// sample.demoAppleSandboxAppNotification();
			// sample.demoBaiduAppNotification();
			// sample.demoWNSAppNotification();
			// sample.demoMPNSAppNotification();
		} catch (AmazonServiceException ase) {
			System.out
					.println("Caught an AmazonServiceException, which means your request made it "
							+ "to Amazon SNS, but was rejected with an error response for some reason.");
			System.out.println("Error Message:    " + ase.getMessage());
			System.out.println("HTTP Status Code: " + ase.getStatusCode());
			System.out.println("AWS Error Code:   " + ase.getErrorCode());
			System.out.println("Error Type:       " + ase.getErrorType());
			System.out.println("Request ID:       " + ase.getRequestId());
		} catch (AmazonClientException ace) {
			System.out
					.println("Caught an AmazonClientException, which means the client encountered "
							+ "a serious internal problem while trying to communicate with SNS, such as not "
							+ "being able to access the network.");
			System.out.println("Error Message: " + ace.getMessage());
		}
	}

	public void demoAndroidAppNotification() {
		String serverAPIKey = "AAAAS-MJ2vc:APA91bHxNj3w8WAz2yLNngtsu1G_Tw5eP1Q98Gs90xzEJZKFu1DegcCAOcYXp2uueUCyy-jYGEqlzmESkxLqGcVvr7HBU9vcst8PGpNtCZMDyXvQ61e130getri5gC3Rzc7eIHZbq7Xl";
		String applicationName = "Lapso-dev";
		String registrationId = "APA91bEjCRc4mrntx7-73Spxfq_6lX8mMGidfJLYgqN7JRo0qL90k7smPjtamra7W3bf8RZMMPAcrhX67bPPNe_GImpdeTwhjrp3TALPA0a-wg-rdc1NSyALrEkHYAb-_huvjs3TXKjA";
		snsClientWrapper.demoNotification(SampleMessageGenerator.Platform.GCM, "", serverAPIKey,
				registrationId, applicationName, attributesMap);
	}

	public void demoKindleAppNotification() {
		// TODO: Please fill in following values for your application. You can
		// also change the notification payload as per your preferences using
		// the method
		// com.amazonaws.sns.samples.tools.SampleMessageGenerator.getSampleKindleMessage()
		String clientId = "";
		String clientSecret = "";
		String applicationName = "";

		String registrationId = "";
		snsClientWrapper.demoNotification(SampleMessageGenerator.Platform.ADM, clientId, clientSecret,
				registrationId, applicationName, attributesMap);
	}

	public void demoAppleAppNotification() {
		// TODO: Please fill in following values for your application. You can
		// also change the notification payload as per your preferences using
		// the method
		// com.amazonaws.sns.samples.tools.SampleMessageGenerator.getSampleAppleMessage()
		String certificate = "MIIGKjCCBRKgAwIBAgIITw0H584atbYwDQYJKoZIhvcNAQELBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTgwMjE2MDIzMzU1WhcNMTkwMzE4MDIzMzU1WjCBqDEgMB4GCgmSJomT8ixkAQEMEGNvbS5zdXJhYW0uTGFwc28xLjAsBgNVBAMMJUFwcGxlIFB1c2ggU2VydmljZXM6IGNvbS5zdXJhYW0uTGFwc28xEzARBgNVBAsMCjg2WTZOVE4yWjkxMjAwBgNVBAoMKUlOVkVSU0lPTkVTIElOVEVSTkFDSU9OQUxFUyBHUlVQT1NVUkEgUyBBMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJk373J2ZMqL4Aj8VPn8oDRWpikDkPG0DXTTNZSHs49GvslSDsAUpOrRJ4kWKptkiBFhXUiNy5Pp/lxZXcj/qravl8Kdj4N/HjmBIQsrQw656y+sOTVDFlsf4lOmAgWBJ3eCyverIUlqcvr4+VRr6WSbmPz7qiiZTMVVhUXEmXiX4O/+Ox+qz34YQW5W54lqkOruGoPRqgGKyjgncWZgd3RHcP54OYuMSe/+UbWXjMBfip+6NcShCaF77L+qvBm08DJtmSMeszKYCr81Xq1tOOUiDRHGsvZnPhyAcFnh0myCVjYrc+Imbm7O+oPT5ftzmI5Qw0JrqEIDi0/cb9MEKPMCAwEAAaOCAmYwggJiMB0GA1UdDgQWBBQz9UYHdVb+JaCjLmcGVSQ3ayuqxDAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFIgnFwmpthhgi+zruvZHWcVSVKO3MIIBHAYDVR0gBIIBEzCCAQ8wggELBgkqhkiG92NkBQEwgf0wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNQYIKwYBBQUHAgEWKWh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5MDAGA1UdHwQpMCcwJaAjoCGGH2h0dHA6Ly9jcmwuYXBwbGUuY29tL3d3ZHJjYS5jcmwwDgYDVR0PAQH/BAQDAgeAMBMGA1UdJQQMMAoGCCsGAQUFBwMCMBAGCiqGSIb3Y2QGAwEEAgUAMBAGCiqGSIb3Y2QGAwIEAgUAMHcGCiqGSIb3Y2QGAwYEaTBnDBBjb20uc3VyYWFtLkxhcHNvMAUMA2FwcAwVY29tLnN1cmFhbS5MYXBzby52b2lwMAYMBHZvaXAMHWNvbS5zdXJhYW0uTGFwc28uY29tcGxpY2F0aW9uMA4MDGNvbXBsaWNhdGlvbjANBgkqhkiG9w0BAQsFAAOCAQEAJeF8YUyEQvAOO4su7Hc/dM+FTVHZ/Kk15EySM5bxZjoSq/LhyjP2ic/pJxcFNFTaGyuehjoQqaOH/z2encOHnZLV8LhIvDejZ1HmgFHLV5adPtQ8nZNLg+GYZYmdDA3zHe1HJci/B0UC+YUrNABxE7QYxA81bBce8aVkIr2CYTJqwZgZsui9tANtA5lGzKnAsZL5g1QC3ijDo65ZgvkRUhrhBGKJe3WQB3DQ825k1z9OFs30AcqdHOSUtzT5Az8etswa79ezkxIDj15cEKIGkUYcbXQkR3tMR3R6daqbzx9/2T4/gKhA+nOkPz0PEbSGpK8jbPkqF4pAnVWJduHxrg=="; // This should be in pem format with \n at the
		// end of each line.
		String privateKey = "MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCZN+9ydmTKi+AI/FT5/KA0VqYpA5DxtA100zWUh7OPRr7JUg7AFKTq0SeJFiqbZIgRYV1IjcuT6f5cWV3I/6q2r5fCnY+Dfx45gSELK0MOuesvrDk1QxZbH+JTpgIFgSd3gsr3qyFJanL6+PlUa+lkm5j8+6oomUzFVYVFxJl4l+Dv/jsfqs9+GEFuVueJapDq7hqD0aoBiso4J3FmYHd0R3D+eDmLjEnv/lG1l4zAX4qfujXEoQmhe+y/qrwZtPAybZkjHrMymAq/NV6tbTjlIg0RxrL2Zz4cgHBZ4dJsglY2K3PiJm5uzvqD0+X7c5iOUMNCa6hCA4tP3G/TBCjzAgMBAAECggEBAII34t6iNslaFd8tLb+E8FdrKS8EY+S4hRM8Cnag2n+zvaR2hCjFouAbQIGnADEvQOGcoV5vfIzKuy6bmTSh7eHz3IQ/yuAUYoi28XF/pC9Y3cSXl8tnqBFsSLuVBHTPLs7Ir13M6K5vtScOAUG5sROblmu0LArzNycjpvADxfeRtIickFNdn+9Yj9ImmBwgnp3LKtMeCGi/ZJPHsmHFXsdRMOHYI22UtgZNUiZu4mWSzDiO3yg3F4iD73mi533Ul2laiJRz7tN2MeWPK22yrfia5MpbS7H9haGeCA9Da9GzcDjW6sMy78cDl4A9cIcNecjTLduezRWOYVWzQmEJxzkCgYEAy7DdEGOiLojAACP8R498Hjdh13vXsKI1XZf72o3TiO31Ooya/1o4qwCU4QJgg5HPUAIuL/VchMN043o3CWnYMTY4PARo7azp8tF51cu7FvUaBry8+095rLOP+G/dRd5XNwniC/PqcvZNs2vMBQQuy8VVHZQBYb3D0qQOCOCXP40CgYEAwJDk8bAIjvSZD0uHWULwChwf7G30w6df155FU6vYVoSqqz8DMKu4lNCAVNzHFFbVdPUD+n0sy4ChxQjTxAngjhwzz4EE2x9IX/OAGib2sdF452iC6Z6yAP1WEm9Mfxxwx/b2NpTWGGh/4ybYKayo4tYBBrAufrK1hn170+Doqn8CgYEAqhcIopTwazdaTUO/eOpCVOEeXNhXZnItafhuITTpfaBndPrQu4bVZ0ZXDAX2Hif2G/OWKgoTtfGZfBMkPkIafA2wq71q+X245kEyIeu8URFfKF0W2iyliCdxg522ApAF+DnSfvSxxEoU3EyZ016IzTP9PXPIK5xRF2ZTKeqRokkCgYBisaB1cqhgRGKyIR7Ek/cc519BOPXK1Vzc0MRtZtThOuuSCyCicFCRDO/JDsKF3R9X6z/XwRIVVt8Scjy+6+UIUNIJFvIbMERS0SUlwjSL3HVf/QOjou9ObIkRt7N4LmZrRlrYjJ3SMrThAgamDVUdtVsfR2r9CrDYtjWh2VLGFwKBgH87BtQZ9wFbKS+pclxpoedB9vQt1tD3f9XRqi8ccTf7RZuAF5D8stQYiwzgnYAXIImjyIBerz7mI5bWJJJyfh+rp0p/qeM2lMKCpNESfDyTDQ4neqH7CQHLJntCHYMSxpqSnyqPONbhrtBXoXF+ZeYkW9nH5uQQMvsUmVAbpMcv"; // This should be in pem format with \n at the
		// end of each line.
		String applicationName = "Lapso-iOS-Prod";
		String deviceToken = "60672007a76121e1ba04e53de873bbcfa80d1ac05cf0579aabb0a6bc72e78962"; // This is 64 hex characters.
		snsClientWrapper.demoNotification(SampleMessageGenerator.Platform.APNS, certificate,
				privateKey, deviceToken, applicationName, attributesMap);
	}

	public void demoAppleSandboxAppNotification() {
		// TODO: Please fill in following values for your application. You can
		// also change the notification payload as per your preferences using
		// the method
		// com.amazonaws.sns.samples.tools.SampleMessageGenerator.getSampleAppleMessage()
		String certificate = "-----BEGIN CERTIFICATE-----\n" +
				"MIIGKjCCBRKgAwIBAgIITw0H584atbYwDQYJKoZIhvcNAQELBQAwgZYxCzAJBgNVBAYTAlVTMRMwEQYDVQQKDApBcHBsZSBJbmMuMSwwKgYDVQQLDCNBcHBsZSBXb3JsZHdpZGUgRGV2ZWxvcGVyIFJlbGF0aW9uczFEMEIGA1UEAww7QXBwbGUgV29ybGR3aWRlIERldmVsb3BlciBSZWxhdGlvbnMgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkwHhcNMTgwMjE2MDIzMzU1WhcNMTkwMzE4MDIzMzU1WjCBqDEgMB4GCgmSJomT8ixkAQEMEGNvbS5zdXJhYW0uTGFwc28xLjAsBgNVBAMMJUFwcGxlIFB1c2ggU2VydmljZXM6IGNvbS5zdXJhYW0uTGFwc28xEzARBgNVBAsMCjg2WTZOVE4yWjkxMjAwBgNVBAoMKUlOVkVSU0lPTkVTIElOVEVSTkFDSU9OQUxFUyBHUlVQT1NVUkEgUyBBMQswCQYDVQQGEwJVUzCCASIwDQYJKoZIhvcNAQEBBQADggEPADCCAQoCggEBAJk373J2ZMqL4Aj8VPn8oDRWpikDkPG0DXTTNZSHs49GvslSDsAUpOrRJ4kWKptkiBFhXUiNy5Pp/lxZXcj/qravl8Kdj4N/HjmBIQsrQw656y+sOTVDFlsf4lOmAgWBJ3eCyverIUlqcvr4+VRr6WSbmPz7qiiZTMVVhUXEmXiX4O/+Ox+qz34YQW5W54lqkOruGoPRqgGKyjgncWZgd3RHcP54OYuMSe/+UbWXjMBfip+6NcShCaF77L+qvBm08DJtmSMeszKYCr81Xq1tOOUiDRHGsvZnPhyAcFnh0myCVjYrc+Imbm7O+oPT5ftzmI5Qw0JrqEIDi0/cb9MEKPMCAwEAAaOCAmYwggJiMB0GA1UdDgQWBBQz9UYHdVb+JaCjLmcGVSQ3ayuqxDAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFIgnFwmpthhgi+zruvZHWcVSVKO3MIIBHAYDVR0gBIIBEzCCAQ8wggELBgkqhkiG92NkBQEwgf0wgcMGCCsGAQUFBwICMIG2DIGzUmVsaWFuY2Ugb24gdGhpcyBjZXJ0aWZpY2F0ZSBieSBhbnkgcGFydHkgYXNzdW1lcyBhY2NlcHRhbmNlIG9mIHRoZSB0aGVuIGFwcGxpY2FibGUgc3RhbmRhcmQgdGVybXMgYW5kIGNvbmRpdGlvbnMgb2YgdXNlLCBjZXJ0aWZpY2F0ZSBwb2xpY3kgYW5kIGNlcnRpZmljYXRpb24gcHJhY3RpY2Ugc3RhdGVtZW50cy4wNQYIKwYBBQUHAgEWKWh0dHA6Ly93d3cuYXBwbGUuY29tL2NlcnRpZmljYXRlYXV0aG9yaXR5MDAGA1UdHwQpMCcwJaAjoCGGH2h0dHA6Ly9jcmwuYXBwbGUuY29tL3d3ZHJjYS5jcmwwDgYDVR0PAQH/BAQDAgeAMBMGA1UdJQQMMAoGCCsGAQUFBwMCMBAGCiqGSIb3Y2QGAwEEAgUAMBAGCiqGSIb3Y2QGAwIEAgUAMHcGCiqGSIb3Y2QGAwYEaTBnDBBjb20uc3VyYWFtLkxhcHNvMAUMA2FwcAwVY29tLnN1cmFhbS5MYXBzby52b2lwMAYMBHZvaXAMHWNvbS5zdXJhYW0uTGFwc28uY29tcGxpY2F0aW9uMA4MDGNvbXBsaWNhdGlvbjANBgkqhkiG9w0BAQsFAAOCAQEAJeF8YUyEQvAOO4su7Hc/dM+FTVHZ/Kk15EySM5bxZjoSq/LhyjP2ic/pJxcFNFTaGyuehjoQqaOH/z2encOHnZLV8LhIvDejZ1HmgFHLV5adPtQ8nZNLg+GYZYmdDA3zHe1HJci/B0UC+YUrNABxE7QYxA81bBce8aVkIr2CYTJqwZgZsui9tANtA5lGzKnAsZL5g1QC3ijDo65ZgvkRUhrhBGKJe3WQB3DQ825k1z9OFs30AcqdHOSUtzT5Az8etswa79ezkxIDj15cEKIGkUYcbXQkR3tMR3R6daqbzx9/2T4/gKhA+nOkPz0PEbSGpK8jbPkqF4pAnVWJduHxrg==\n" +
				"-----END CERTIFICATE-----\n"; // This should be in pem format with \n at the
									// end of each line.
		String privateKey = "-----BEGIN PRIVATE KEY-----\n" +
				"MIIEvgIBADANBgkqhkiG9w0BAQEFAASCBKgwggSkAgEAAoIBAQCZN+9ydmTKi+AI/FT5/KA0VqYpA5DxtA100zWUh7OPRr7JUg7AFKTq0SeJFiqbZIgRYV1IjcuT6f5cWV3I/6q2r5fCnY+Dfx45gSELK0MOuesvrDk1QxZbH+JTpgIFgSd3gsr3qyFJanL6+PlUa+lkm5j8+6oomUzFVYVFxJl4l+Dv/jsfqs9+GEFuVueJapDq7hqD0aoBiso4J3FmYHd0R3D+eDmLjEnv/lG1l4zAX4qfujXEoQmhe+y/qrwZtPAybZkjHrMymAq/NV6tbTjlIg0RxrL2Zz4cgHBZ4dJsglY2K3PiJm5uzvqD0+X7c5iOUMNCa6hCA4tP3G/TBCjzAgMBAAECggEBAII34t6iNslaFd8tLb+E8FdrKS8EY+S4hRM8Cnag2n+zvaR2hCjFouAbQIGnADEvQOGcoV5vfIzKuy6bmTSh7eHz3IQ/yuAUYoi28XF/pC9Y3cSXl8tnqBFsSLuVBHTPLs7Ir13M6K5vtScOAUG5sROblmu0LArzNycjpvADxfeRtIickFNdn+9Yj9ImmBwgnp3LKtMeCGi/ZJPHsmHFXsdRMOHYI22UtgZNUiZu4mWSzDiO3yg3F4iD73mi533Ul2laiJRz7tN2MeWPK22yrfia5MpbS7H9haGeCA9Da9GzcDjW6sMy78cDl4A9cIcNecjTLduezRWOYVWzQmEJxzkCgYEAy7DdEGOiLojAACP8R498Hjdh13vXsKI1XZf72o3TiO31Ooya/1o4qwCU4QJgg5HPUAIuL/VchMN043o3CWnYMTY4PARo7azp8tF51cu7FvUaBry8+095rLOP+G/dRd5XNwniC/PqcvZNs2vMBQQuy8VVHZQBYb3D0qQOCOCXP40CgYEAwJDk8bAIjvSZD0uHWULwChwf7G30w6df155FU6vYVoSqqz8DMKu4lNCAVNzHFFbVdPUD+n0sy4ChxQjTxAngjhwzz4EE2x9IX/OAGib2sdF452iC6Z6yAP1WEm9Mfxxwx/b2NpTWGGh/4ybYKayo4tYBBrAufrK1hn170+Doqn8CgYEAqhcIopTwazdaTUO/eOpCVOEeXNhXZnItafhuITTpfaBndPrQu4bVZ0ZXDAX2Hif2G/OWKgoTtfGZfBMkPkIafA2wq71q+X245kEyIeu8URFfKF0W2iyliCdxg522ApAF+DnSfvSxxEoU3EyZ016IzTP9PXPIK5xRF2ZTKeqRokkCgYBisaB1cqhgRGKyIR7Ek/cc519BOPXK1Vzc0MRtZtThOuuSCyCicFCRDO/JDsKF3R9X6z/XwRIVVt8Scjy+6+UIUNIJFvIbMERS0SUlwjSL3HVf/QOjou9ObIkRt7N4LmZrRlrYjJ3SMrThAgamDVUdtVsfR2r9CrDYtjWh2VLGFwKBgH87BtQZ9wFbKS+pclxpoedB9vQt1tD3f9XRqi8ccTf7RZuAF5D8stQYiwzgnYAXIImjyIBerz7mI5bWJJJyfh+rp0p/qeM2lMKCpNESfDyTDQ4neqH7CQHLJntCHYMSxpqSnyqPONbhrtBXoXF+ZeYkW9nH5uQQMvsUmVAbpMcv\n" +
				"-----END PRIVATE KEY-----\n"; // This should be in pem format with \n at the
								// end of each line.
		String applicationName = "Lapso-iOS-Dev";
		String deviceToken = "60672007A76121E1BA04E53DE873BBCFA80D1AC05CF0579AABB0A6BC72E78962"; // This is 64 hex characters.
		snsClientWrapper.demoNotification(SampleMessageGenerator.Platform.APNS_SANDBOX, certificate,
				privateKey, deviceToken, applicationName, attributesMap);
	}

	public void demoBaiduAppNotification() {
		/*
		 * TODO: Please fill in the following values for your application. If
		 * you wish to change the properties of your Baidu notification, you can
		 * do so by modifying the attribute values in the method
		 * addBaiduNotificationAttributes() . You can also change the
		 * notification payload as per your preferences using the method
		 * com.amazonaws
		 * .sns.samples.tools.SampleMessageGenerator.getSampleBaiduMessage()
		 */
		String userId = "";
		String channelId = "";
		String apiKey = "";
		String secretKey = "";
		String applicationName = "";
		snsClientWrapper.demoNotification(SampleMessageGenerator.Platform.BAIDU, apiKey, secretKey,
				channelId + "|" + userId, applicationName, attributesMap);
	}

	public void demoWNSAppNotification() {
		/*
		 * TODO: Please fill in the following values for your application. If
		 * you wish to change the properties of your WNS notification, you can
		 * do so by modifying the attribute values in the method
		 * addWNSNotificationAttributes() . You can also change the notification
		 * payload as per your preferences using the method
		 * com.amazonaws.sns.samples
		 * .tools.SampleMessageGenerator.getSampleWNSMessage()
		 */
		String notificationChannelURI = "";
		String packageSecurityIdentifier = "";
		String secretKey = "";
		String applicationName = "";
		snsClientWrapper.demoNotification(SampleMessageGenerator.Platform.WNS,
				packageSecurityIdentifier, secretKey, notificationChannelURI,
				applicationName, attributesMap);
	}

	public void demoMPNSAppNotification() {
		/*
		 * TODO: Please fill in the following values for your application. If
		 * you wish to change the properties of your MPNS notification, you can
		 * do so by modifying the attribute values in the method
		 * addMPNSNotificationAttributes() . You can also change the
		 * notification payload as per your preferences using the method
		 * com.amazonaws
		 * .sns.samples.tools.SampleMessageGenerator.getSampleMPNSMessage ()
		 */
		String notificationChannelURI = "";
		String applicationName = "";
		snsClientWrapper.demoNotification(SampleMessageGenerator.Platform.MPNS, "", "",
				notificationChannelURI, applicationName, attributesMap);
	}

	private static Map<String, MessageAttributeValue> addBaiduNotificationAttributes() {
		Map<String, MessageAttributeValue> notificationAttributes = new HashMap<String, MessageAttributeValue>();
		notificationAttributes.put("AWS.SNS.MOBILE.BAIDU.DeployStatus",
				new MessageAttributeValue().withDataType("String")
						.withStringValue("1"));
		notificationAttributes.put("AWS.SNS.MOBILE.BAIDU.MessageKey",
				new MessageAttributeValue().withDataType("String")
						.withStringValue("default-channel-msg-key"));
		notificationAttributes.put("AWS.SNS.MOBILE.BAIDU.MessageType",
				new MessageAttributeValue().withDataType("String")
						.withStringValue("0"));
		return notificationAttributes;
	}

	private static Map<String, MessageAttributeValue> addWNSNotificationAttributes() {
		Map<String, MessageAttributeValue> notificationAttributes = new HashMap<String, MessageAttributeValue>();
		notificationAttributes.put("AWS.SNS.MOBILE.WNS.CachePolicy",
				new MessageAttributeValue().withDataType("String")
						.withStringValue("cache"));
		notificationAttributes.put("AWS.SNS.MOBILE.WNS.Type",
				new MessageAttributeValue().withDataType("String")
						.withStringValue("wns/badge"));
		return notificationAttributes;
	}

	private static Map<String, MessageAttributeValue> addMPNSNotificationAttributes() {
		Map<String, MessageAttributeValue> notificationAttributes = new HashMap<String, MessageAttributeValue>();
		notificationAttributes.put("AWS.SNS.MOBILE.MPNS.Type",
				new MessageAttributeValue().withDataType("String")
						.withStringValue("token")); // This attribute is required.
		notificationAttributes.put("AWS.SNS.MOBILE.MPNS.NotificationClass",
				new MessageAttributeValue().withDataType("String")
						.withStringValue("realtime")); // This attribute is required.
														
		return notificationAttributes;
	}
}
